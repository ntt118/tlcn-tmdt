import * as express from "express";
import * as PromotionController from "../controller/PromotionController"

const router = express.Router();

router.post('/get-list', PromotionController.getList);
router.post('/create',PromotionController.create);
router.post('/get-id',PromotionController.getById);
router.post('/update',PromotionController.update);
router.post('/delete',PromotionController.deletePromotion);

export default router;