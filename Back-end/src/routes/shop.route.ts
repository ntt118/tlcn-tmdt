import * as express from "express";
import * as ShopController from "../controller/Shop.Controller"

const router = express.Router();

router.post('/get-shop', ShopController.getShop);
router.post('/update',ShopController.update);

export default router;