import * as express from "express";
import * as productController from '../controller/ProductController';
const router = express.Router();


// router.post('/get-list', productController.all);
router.post('/delete', productController.remove);
router.post('/get-id',productController.byId)
router.post('/create', productController.insert);
router.post('/update', productController.update);

export default router;