import * as express from "express";
import * as CartController from "../controller/CartController"

const router = express.Router();

router.post('/get-list', CartController.getList);
router.post('/create',CartController.create);
router.post('/get-id',CartController.getById);
router.post('/update',CartController.update);
router.post('/delete',CartController.deleteCart);
router.post('/delete/product', CartController.removeProductsFromCart);

export default router;