import * as express from "express";
import * as orderController from '../controller/OrderController';
const router = express.Router();


router.post('/get-list', orderController.all);
router.post('/delete', orderController.remove);
router.get('/paid/:id', orderController.paid);
router.post('/paymentQRCode', orderController.paymentQRCode);
router.post('/get-id',orderController.byId);
router.post('/user/get-id',orderController.getOrderByUserId);
router.post('/create', orderController.create);
router.post('/cancel', orderController.cancelOrder);
router.post('/update', orderController.update);
router.post('/getOrderToShipper', orderController.getOrderToShipper);
router.post('/dashboard', orderController.getForDashboard);
export default router;