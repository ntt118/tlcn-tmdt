import * as express from "express";
import * as BrandController from "../controller/BrandController"

const router = express.Router();

// router.post('/get-list', BrandController.getList);
router.post('/create',BrandController.create);
router.post('/get-id',BrandController.getById);
router.post('/update',BrandController.update);
router.post('/delete',BrandController.deleteCategory);

export default router;