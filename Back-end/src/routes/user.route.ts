import * as express from "express";
import * as UserController from "../controller/UserController"

var jwt = require('jsonwebtoken')

const router = express.Router();

function authenToken(req, res, next) {
    const authorizationHeader = req.headers['authorization'];
    //Bear token
    const token = authorizationHeader.split(' ')[1];
    if(!token) res.sendStatus(401);

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        if(err) res.sendStatus(403)
        next()
    })
}

router.post('/get-list', UserController.getList);
router.get('/confirm-register/:username', UserController.acceptRegister);
router.post('/reset-request', UserController.sendResetEmail)
router.post('/reset-pass', UserController.resetPassword)
router.post('/create',UserController.create)
router.post('/createStaff',UserController.createStaff)
router.post('/get-id', UserController.getByUsername)
router.post('/update', UserController.update)
router.post('/delete', UserController.deleteUser)
router.post('/auth', UserController.authAccount)

export default router;