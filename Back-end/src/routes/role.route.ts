import * as express from "express";
import * as RoleController from "../controller/RoleController"

const router = express.Router();

router.post('/get-list', RoleController.getList);
router.post('/create',RoleController.create);
router.post('/get-id',RoleController.getById);
router.post('/update',RoleController.update);
router.post('/delete',RoleController.deleteRole);

export default router;