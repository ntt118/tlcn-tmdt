import * as express from "express";
import * as CategoryController from "../controller/CategoryController"

const router = express.Router();

// router.post('/get-list', CategoryController.getList);
router.post('/create',CategoryController.create);
router.post('/get-id',CategoryController.getById);
router.post('/update',CategoryController.update);
router.post('/delete',CategoryController.deleteCategory);

export default router;