import * as express from "express";
import * as orderDetailController from '../controller/OrderDetailController';
const router = express.Router();


router.post('/', orderDetailController.all);
router.post('/get-id',orderDetailController.byOrderId)
// router.post('/create', orderDetailController.create);

export default router;