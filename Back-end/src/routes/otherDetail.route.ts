import * as express from "express";
import * as otherDetailController from '../controller/OtherDetailController';
const router = express.Router();


router.post('/get-list', otherDetailController.getList);
router.post('/create', otherDetailController.create);
router.post('/delete', otherDetailController.deleteOtherdetail);
router.post('/update', otherDetailController.update);
router.post('/list-type', otherDetailController.listTypeOtherDetail)

export default router;