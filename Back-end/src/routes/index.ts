import * as express from 'express';
import productRoute from './product.route';
import orderDetailRoute from './orderDetail.route';
import userRoute from './user.route';
import orderRoute from './order.route'
import shopRoute from './shop.route'
import roleRoute from './role.route'
import categoryRoute from './category.route'
import brandRoute from './brand.route'
import otherDetailRoute from './otherDetail.route'
// import permissionRoute from './permission.route'
import promotionRoute from './promotion.route'
import cartRoute from './cart.route'
import * as productController from '../controller/ProductController'
import * as CategoryController from '../controller/CategoryController'
import * as BrandController from '../controller/BrandController'


var jwt = require('jsonwebtoken')

const router = express.Router();

function authenToken(req, res, next) {
    try {
        const authorizationHeader = req.headers['authorization'];
        //Bear token
        const token = authorizationHeader.split(' ')[1];
        if(!token) res.sendStatus(401);

        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
            if(err) res.sendStatus(403)
            next()
        })
        console.log('req', req)
    } catch {
        res.sendStatus(400);
    }
}
router.post('/api/product/get-list', productController.all);
router.post('/api/product/get-id', productController.byId);
router.post('/api/category/get-list', CategoryController.getList);
router.post('/api/brand/get-list', BrandController.getList);

router.use('/api/product', productRoute)
router.use('/api/order',orderRoute)
router.use('/api/orderDetail',orderDetailRoute)
router.use('/api/user', userRoute)
router.use('/api/shop', shopRoute)
router.use('/api/role', roleRoute)
router.use('/api/category', categoryRoute)
router.use('/api/brand', brandRoute)
// router.use('/api/permission', permissionRoute)
router.use('/api/promotion',promotionRoute)
router.use('/api/cart',cartRoute)
router.use('/api/otherdetail',otherDetailRoute)
export default router;