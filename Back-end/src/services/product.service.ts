import { Imageproduct } from './../entities/Imageproduct';
import {getRepository, Repository} from "typeorm";
import { Product } from "../entities/Product";

export class ProductService {
    public static _ins: ProductService;
    public static get Ins(): ProductService {
        if(!ProductService._ins){
            ProductService._ins = new ProductService();
        }
        return ProductService._ins;
    }

    repository: Repository<Product>;
    repositoryImg: Repository<Imageproduct>;

    
    constructor(){
        this.repository = getRepository(Product);
        this.repositoryImg = getRepository(Imageproduct);

        
    }

    public async getAll(){
        //return this.repository.query("SELECT * FROM PRODUCT");
        return await this.repository.find({ relations: ["imageproducts","category","brand","productotherdetails"] });
    }
    public async getById(id: number){
        return await this.repository.findOne(id, { relations: ["imageproducts", "category","brand", "productotherdetails"] });
    }
    public async getByIdNoneImage(id: number){
        return await this.repository.findOne(id);
    }

    public async getByName(name: string){
        return await this.repository.findOne({name: name});
    }
    public async insert(product: Product){
        return await this.repository.save(product);
    }
    public async update(product: Product){
        return this.repository.save(product);
    }
    public async delete(id: number){
        let _product = await this.repository.findOne(id);
        return this.repository.delete(_product.id);
    }
    public async insertImageProduct(img: Imageproduct){
        return await this.repositoryImg.save(img);
    }
    public async deleteImageProduct(id: number){
        let _image = await this.repositoryImg.findOne(id);
        return this.repositoryImg.delete(_image.id);
    }
    public async getImageByProductId(productId: number){
        return await this.repositoryImg.find({productId})
    }
}