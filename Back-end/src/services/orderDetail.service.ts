import {getRepository, Repository} from "typeorm";
import { Orderdetail } from "../entities/Orderdetail";
import { OrderService } from "./order.service";

export class OrderDetailService {
    public static _ins: OrderDetailService;
    public static get Ins(): OrderDetailService {
        if(!OrderDetailService._ins){
            OrderDetailService._ins = new OrderDetailService();
        }
        return OrderDetailService._ins;
    }

    repository: Repository<Orderdetail>;
    
    constructor(){
        this.repository = getRepository(Orderdetail);
    }

    public getAll(){
        return this.repository.find();
    }
    public async getById(id: any){
        return await this.repository.find({ where: {orderId: id}});
    }
    public create(importStock: Orderdetail){    
        return this.repository.save(importStock);
    }

    public async createOrderDetail(orderId, data: Orderdetail) {
    
        var order = await OrderService.Ins.getOrder(orderId);
    
        if(!order){
            return;
        }
    
        let _orderDetail = new Orderdetail();
        _orderDetail.orderId = orderId
        _orderDetail.productotherdetailId = data.productotherdetailId
        _orderDetail.quantity = data.quantity
        await this.create(_orderDetail);
        return _orderDetail;
    }
}