import {getRepository, Repository} from "typeorm";
import { Cart } from "../entities/Cart";
export class CartService {

    public static _ins: CartService;

    public static get Ins(): CartService {
        if(!CartService._ins){
            CartService._ins = new CartService();
        }
        return CartService._ins;
    }

    repository: Repository<Cart>;
    
    constructor(){
        this.repository = getRepository(Cart);
    }
    public async getList(){
        return await this.repository.find();
    }
    public async getByUserId(userId: number){
        return await this.repository.find({ where: {userId}, relations:['productotherdetail']});
    }
    public async create(cart : Cart){
        return await this.repository.insert(cart);
    }
    public async update(cart : Cart[]){
        return await this.repository.save(cart);
    }
    public async delete(cart : Cart){
        return await this.repository.delete(cart);
    }
    public async deleteByUserId(userId: number) {
        return await this.repository.delete({userId})
    }
}