import {getRepository, Repository} from "typeorm";
import { User } from "../entities/User";
export class UserService {

    public static _ins: UserService;

    public static get Ins(): UserService {
        if(!UserService._ins){
            UserService._ins = new UserService();
        }
        return UserService._ins;
    }

    repository: Repository<User>;
    
    constructor(){
        this.repository = getRepository(User);
    }
    public async getList(){
        return await this.repository.find({where: {roleId: 2,isActive: 1}});
    }
    public async getByUsername(username: string){
        return await this.repository.findOne({username : username}, { relations: ["carts"] });
    }
    public async getByEmail(email: string){
        return await this.repository.findOne({email : email}, { relations: ["carts"] });
    }
    public async getById(id: number){
        return await this.repository.findOne(id);
    }
    public async create(user : User){
        return await this.repository.insert(user);
    }

    public async update(user : User){
        return await this.repository.save(user);
    }
    public async delete(user : User){
        return await this.repository.delete(user);
    }

    
}