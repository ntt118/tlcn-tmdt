import {getRepository, Repository} from "typeorm";
import { Color } from "../entities/Color";
import { Otherdetail } from "../entities/Otherdetail";
import { Size } from "../entities/Size";
export class OtherdetailService {

    public static _ins: OtherdetailService;

    public static get Ins(): OtherdetailService {
        if(!OtherdetailService._ins){
            OtherdetailService._ins = new OtherdetailService();
        }
        return OtherdetailService._ins;
    }

    repository: Repository<Otherdetail>;
    repositorySize: Repository<Size>;
    repositoryColor: Repository<Color>;
    
    constructor(){
        this.repository = getRepository(Otherdetail);
        this.repositorySize = getRepository(Size);
        this.repositoryColor = getRepository(Color);
    }
    public async getList(){
        return await this.repository.find();
    }
    public async getListSize(){
        return await this.repositorySize.find();
    }
    public async getListColor(){
        return await this.repositoryColor.find();
    }

    public async getById(id: number){
        return await this.repository.findOne({id :id}, {relations: ['color', 'size']});
    }
    public async getBySizeIdAndColorId(sizeId, colorId) {
        return await this.repository.findOne({sizeId, colorId});
    }
    
    public async getSizeById(id: number){
        return await this.repositorySize.findOne({id : id});
    }
    public async getColorById(id: number){
        return await this.repositoryColor.findOne({id : id});
    }

    // public async create(category : Otherdetail){
    //     return await this.repository.insert(category);
    // }
    public async createSize(size: Size){
        return await this.repositorySize.insert(size);
    }
    public async createColor(color: Color){
        return await this.repositoryColor.insert(color);
    }

    // public async getByName(name: string) : Promise<Otherdetail>{
    //     return await this.repository.findOne({})
    // }
    public async getByNameSize(name: string) : Promise<Size>{
        return await this.repositorySize.findOne({name: name.trim()});
    }
    public async getByNameColor(name: string) : Promise<Color>{
        return await this.repositoryColor.findOne({name: name.trim()});
    }

    // public async update(category : Otherdetail){
    //     return await this.repository.save(category);
    // }
    public async updateSize(size: Size){
        return await this.repositorySize.save(size);
    }
    public async updateColor(color: Color){
        return await this.repositoryColor.save(color);
    }

    public async listTypeOtherDetail(){
        return await this.repository.query("SELECT type FROM otherdetail GROUP BY type")
    }

    public async getOtherDetail(sizeId: number, colorId: number){
        return await this.repository.findOne({ sizeId, colorId });
    }
    public async createOtherdetail(data: any){
        return await this.repository.save(data);
    }
}