import {getRepository, Repository} from "typeorm";
import { Role } from "../entities/Role";
export class RoleService {

    public static _ins: RoleService;

    public static get Ins(): RoleService {
        if(!RoleService._ins){
            RoleService._ins = new RoleService();
        }
        return RoleService._ins;
    }

    repository: Repository<Role>;
    
    constructor(){
        this.repository = getRepository(Role);
    }
    public async getList(){
        return await this.repository.find();
    }
    public async getById(id: number){
        return await this.repository.findOne(id);
    }
    public async getByName(name: string) : Promise<any>{
       return await this.repository.findOne({name : name.trim()})
    }
    public async create(role : any){
        return await this.repository.save(role);
    }

    public async update(role : Role){
        return await this.repository.save(role);
    }
    public async delete(id: number){
        return await this.repository.delete(id);
    }

    
}