import {getRepository, Repository} from "typeorm";
import { Color } from "../entities/Color";
import { Otherdetail } from "../entities/Otherdetail";
import { Productotherdetail } from "../entities/Productotherdetail";
import { Size } from "../entities/Size";
export class ProductOtherdetailService {

    public static _ins: ProductOtherdetailService;

    public static get Ins(): ProductOtherdetailService {
        if(!ProductOtherdetailService._ins){
            ProductOtherdetailService._ins = new ProductOtherdetailService();
        }
        return ProductOtherdetailService._ins;
    }

    repository: Repository<Productotherdetail>;
    
    constructor(){
        this.repository = getRepository(Productotherdetail);
    }
    
    public async createProductOtherdetail(data){
        return await this.repository.save(data);
    }

    public async getByProductId(productId: number){
        return await this.repository.findOne({ relations: ["otherdetail"], where: { productId } });
    }

    public async getById(id: number) {
        return await this.repository.findOne(id, { relations: ["otherdetail"] })
    }

    public async getProducOtherdetail(productId: number, otherdetailId: number) {
        return await this.repository.findOne({productId, otherdetailId});
    }

    public async updateProducOtherdetail(productotherdetail: Productotherdetail) {
        return await this.repository.save(productotherdetail);
    }
}