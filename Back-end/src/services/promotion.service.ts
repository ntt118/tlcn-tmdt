import { getRepository, Repository } from "typeorm";
import { Promotion } from "../entities/Promotion";
export class PromotionService {

    public static _ins: PromotionService;

    public static get Ins(): PromotionService {
        if(!PromotionService._ins){
            PromotionService._ins = new PromotionService();
        }
        return PromotionService._ins;
    }


    repository: Repository<Promotion>;
    
    constructor(){
        this.repository = getRepository(Promotion);
    }
    public async getList(){
        return await this.repository.find();
    }
    public async getById(id: number){
        return await this.repository.findOne({id : id});
    }
    public async getByName(name: string) : Promise<any>{
       return await this.repository.findOne({name : name.trim()})
    }
    public async create(promo : Promotion){
        return await this.repository.insert(promo);
    }

    public async update(promo : Promotion){
        return await this.repository.save(promo);
    }
    public async delete(promo : Promotion){
        return await this.repository.delete(promo);
    }

    
}