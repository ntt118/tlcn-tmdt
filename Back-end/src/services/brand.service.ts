import {getRepository, Repository} from "typeorm";
import { Brand } from "../entities/Brand";
export class BrandService {

    public static _ins: BrandService;

    public static get Ins(): BrandService {
        if(!BrandService._ins){
            BrandService._ins = new BrandService();
        }
        return BrandService._ins;
    }

    repository: Repository<Brand>;
    
    constructor(){
        this.repository = getRepository(Brand);
    }
    public async getList(){
        return await this.repository.find();
    }
    public async getById(id: number){
        return await this.repository.findOne({id : id},{ relations: ["products"] });
    }
    public async getByName(name: string) : Promise<Brand>{
       return await this.repository.findOne({name : name.trim()})
    }
    public async create(category : Brand){
        return await this.repository.insert(category);
    }

    public async update(category : Brand){
        return await this.repository.save(category);
    }
    public async delete(category : Brand){
        return await this.repository.delete(category);
    }
}