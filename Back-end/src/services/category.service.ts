import {getRepository, Repository} from "typeorm";
import { Category } from "../entities/Category";
export class CategoryService {

    public static _ins: CategoryService;

    public static get Ins(): CategoryService {
        if(!CategoryService._ins){
            CategoryService._ins = new CategoryService();
        }
        return CategoryService._ins;
    }

    repository: Repository<Category>;
    
    constructor(){
        this.repository = getRepository(Category);
    }
    public async getList(){
        return await this.repository.find();
    }
    public async getById(id: number){
        return await this.repository.findOne({id : id},{ relations: ["products"] });
    }
    public async getByName(name: string) : Promise<Category>{
       return await this.repository.findOne({name : name.trim()})
    }
    public async create(category : Category){
        return await this.repository.insert(category);
    }

    public async update(category : Category){
        return await this.repository.save(category);
    }
    public async delete(category : Category){
        return await this.repository.delete(category);
    }

    
}