import * as crypto from 'crypto';
import { PersistedPassword } from '../models/persistedPassword.model'

const PASSWORD_LENGTH = 256;
const SALT_LENGTH = 64;
const ITERATIONS = 10000;
const DIGEST = 'sha256';
const BYTE_TO_STRING_ENCODING = 'hex';

export class AccountService{

    public static _ins: AccountService;

    public static get Ins(): AccountService {
        if(!AccountService._ins){
            AccountService._ins = new AccountService();
        }
        return AccountService._ins;
    }

    public async generateHashPassword(password: string): Promise<PersistedPassword> {
        return new Promise<PersistedPassword>((accept, reject) => {
            const salt = crypto.randomBytes(SALT_LENGTH).toString(BYTE_TO_STRING_ENCODING);
            crypto.pbkdf2(password, salt, ITERATIONS, PASSWORD_LENGTH, DIGEST, (error, hash) => {
                if (error) {
                    reject(error);
                } else {
                    accept({
                        salt,
                        hash: hash.toString(BYTE_TO_STRING_ENCODING),
                        iterations: ITERATIONS,
                    });
                }
            });
        });
    }

    public async verifyPassword(persistedPassword: PersistedPassword, passwordAttempt: string): Promise<boolean> {
        return new Promise<boolean>((accept, reject) => {
            crypto.pbkdf2(passwordAttempt, persistedPassword.salt, persistedPassword.iterations, PASSWORD_LENGTH, DIGEST, (error, hash) => {
                if (error) {
                    reject(error);
                } else {
                    accept(persistedPassword.hash === hash.toString(BYTE_TO_STRING_ENCODING));
                }
            });
        });
    }
}