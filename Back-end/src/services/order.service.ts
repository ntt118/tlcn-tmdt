import { endOfMonth, startOfMonth } from "date-fns";
import {Between, getRepository, In, LessThan, Repository} from "typeorm";
import { Order } from "../entities/Order";
export class OrderService {
    public static _ins: OrderService;
    public static get Ins(): OrderService {
        if(!OrderService._ins){
            OrderService._ins = new OrderService();
        }
        return OrderService._ins;
    }

    repository: Repository<Order>;
    
    constructor(){
        this.repository = getRepository(Order);
    }

    public async getAll(){
        return await this.repository.find({ where: {}, relations:['orderdetails']});
    }
    public async getOrderByDate(month, year) {
        const dateStart = startOfMonth(new Date(year, month))
        const dateEnd = endOfMonth(new Date(year, month))
        return await this.repository.find({
            where: {
                dateCreate: Between(dateStart, dateEnd)
            }
        })
    }
    public async getCountStatus(status: number) {
        return await this.repository.count({ where: { status } });
    }
    public async getOrderToShipper(){
        return await this.repository.find({where: [{status : 1}, {status : 2}, {status : 3}]});
    }
    public getOrder(id: any): any{
        //return this.repository.query(`SELECT * FROM PRODUCT WHERE id = ${id}`);
        return this.repository.findOne(id, { relations: ['orderdetails']});
    }
    public getAllOrderByUserId(id:any){
        return this.repository.find(id);
    }
    public create(order: Order){
        return this.repository.save(order);
    }
    public async update(order: Order){
        return this.repository.save(order);
    }
    public async delete(id: number){
        //return this.repository.query(`DELETE FROM PRODUCT WHERE id = ${id}`);
        let prd = await this.repository.findOne(id);
        return this.repository.delete(prd);
    }

    
}