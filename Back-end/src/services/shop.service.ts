import {getRepository, Repository} from "typeorm";
import { Shop } from "../entities/Shop";
export class ShopService {

    public static _ins: ShopService;

    public static get Ins(): ShopService {
        if(!ShopService._ins){
            ShopService._ins = new ShopService();
        }
        return ShopService._ins;
    }

    repository: Repository<Shop>;
    
    constructor(){
        this.repository = getRepository(Shop);
    }

    public async getShop(){
        return await this.repository.findOne({id : 1});
    }

    public async update(shop : Shop){
        return await this.repository.save(shop);
    } 
}