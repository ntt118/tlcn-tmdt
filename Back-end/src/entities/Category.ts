import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./Product";

@Entity("category", { schema: "ecommerce" })
export class Category {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("varchar", { name: "name", length: 45 })
  name: string;

  @Column("tinyint", { name: "status" })
  status: number;

  @Column("tinyint", { name: "isActive", nullable: true })
  isActive: number | null;

  @OneToMany(() => Product, (product) => product.category)
  products: Product[];
}
