import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Cart } from "./Cart";
import { Order } from "./Order";
import { Role } from "./Role";

@Index("email_UNIQUE", ["email"], { unique: true })
@Index("username_UNIQUE", ["username"], { unique: true })
@Index("fk_user_role1_idx", ["roleId"], {})
@Entity("user", { schema: "ecommerce" })
export class User {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("varchar", { name: "username", unique: true, length: 50 })
  username: string;

  @Column("varchar", { name: "name", length: 50 })
  name: string;

  @Column("varchar", { name: "email", unique: true, length: 30 })
  email: string;

  @Column("varchar", { name: "phoneNumber", nullable: true, length: 20 })
  phoneNumber: string | null;

  @Column("varchar", { name: "address", nullable: true, length: 100 })
  address: string | null;

  @Column("tinyint", { name: "gender", nullable: true })
  gender: number | null;

  @Column("datetime", { name: "dayOfBirth", nullable: true })
  dayOfBirth: Date | null;

  @Column("int", { name: "role_id" })
  roleId: number;

  @Column("blob", { name: "salt" })
  salt: Buffer;

  @Column("blob", { name: "hash" })
  hash: Buffer;

  @Column("int", { name: "iterations" })
  iterations: number;

  @Column("int", { name: "status", nullable: true })
  status: number | null;

  @Column("tinyint", { name: "isActive", nullable: true })
  isActive: number | null;

  @Column("varchar", { name: "resetCode", nullable: true, length: 45 })
  resetCode: string | null;

  @OneToMany(() => Cart, (cart) => cart.user)
  carts: Cart[];

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @ManyToOne(() => Role, (role) => role.users, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "role_id", referencedColumnName: "id" }])
  role: Role;
}
