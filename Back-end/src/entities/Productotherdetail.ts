import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Cart } from "./Cart";
import { Orderdetail } from "./Orderdetail";
import { Otherdetail } from "./Otherdetail";
import { Product } from "./Product";

@Index("fk_product_has_otherdetail_product1_idx", ["productId"], {})
@Index("fk_productotherdetail_size_has_color1_idx", ["otherdetailId"], {})
@Entity("productotherdetail", { schema: "ecommerce" })
export class Productotherdetail {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("int", { name: "product_id" })
  productId: number;

  @Column("int", { name: "otherdetail_id" })
  otherdetailId: number;

  @Column("float", { name: "price", nullable: true, precision: 12 })
  price: number | null;

  @Column("int", { name: "quantity", nullable: true })
  quantity: number | null;

  @OneToMany(() => Cart, (cart) => cart.productotherdetail)
  carts: Cart[];

  @OneToMany(() => Orderdetail, (orderdetail) => orderdetail.productotherdetail)
  orderdetails: Orderdetail[];

  @ManyToOne(
    () => Otherdetail,
    (otherdetail) => otherdetail.productotherdetails,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "otherdetail_id", referencedColumnName: "id" }])
  otherdetail: Otherdetail;

  @ManyToOne(() => Product, (product) => product.productotherdetails, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "product_id", referencedColumnName: "id" }])
  product: Product;
}
