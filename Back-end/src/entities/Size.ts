import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Otherdetail } from "./Otherdetail";

@Entity("size", { schema: "ecommerce" })
export class Size {
  @PrimaryGeneratedColumn({ type: "int", name: "_id", unsigned: true })
  id: number;

  @Column("varchar", { name: "name", length: 45 })
  name: string;

  @Column("tinyint", { name: "status", nullable: true })
  status: number | null;

  @OneToMany(() => Otherdetail, (otherdetail) => otherdetail.size)
  otherdetails: Otherdetail[];
}
