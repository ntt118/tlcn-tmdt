import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Imageproduct } from "./Imageproduct";
import { Category } from "./Category";
import { Brand } from "./Brand";
import { Productotherdetail } from "./Productotherdetail";
import { Promotionproduct } from "./Promotionproduct";

@Index("fk_product_brand1_idx", ["brandId"], {})
@Index("fk_product_category_idx", ["categoryId"], {})
@Entity("product", { schema: "ecommerce" })
export class Product {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("varchar", { name: "name", nullable: true, length: 45 })
  name: string | null;

  @Column("varchar", { name: "description", nullable: true, length: 100 })
  description: string | null;

  @Column("float", { name: "price", nullable: true, precision: 12 })
  price: number | null;

  @Column("int", { name: "quantity", nullable: true })
  quantity: number | null;

  @Column("int", { name: "category_id" })
  categoryId: number;

  @Column("float", { name: "rateAvg", nullable: true, precision: 12 })
  rateAvg: number | null;

  @Column("tinyint", { name: "status", nullable: true })
  status: number | null;

  @Column("tinyint", { name: "isActive", nullable: true })
  isActive: number | null;

  @Column("int", { name: "brand_id" })
  brandId: number;

  @OneToMany(() => Imageproduct, (imageproduct) => imageproduct.product)
  imageproducts: Imageproduct[];

  @ManyToOne(() => Category, (category) => category.products, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "category_id", referencedColumnName: "id" }])
  category: Category;

  @ManyToOne(() => Brand, (brand) => brand.products, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "brand_id", referencedColumnName: "id" }])
  brand: Brand;

  @OneToMany(
    () => Productotherdetail,
    (productotherdetail) => productotherdetail.product
  )
  productotherdetails: Productotherdetail[];

  @OneToMany(
    () => Promotionproduct,
    (promotionproduct) => promotionproduct.idProduct2
  )
  promotionproducts: Promotionproduct[];
}
