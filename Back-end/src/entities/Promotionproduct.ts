import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { Product } from "./Product";
import { Promotion } from "./Promotion";

@Index("fk_promotion_has_product_product1_idx", ["idProduct"], {})
@Index("fk_promotion_has_product_promotion_idx", ["idPromotion"], {})
@Entity("promotionproduct", { schema: "ecommerce" })
export class Promotionproduct {
  @Column("int", { primary: true, name: "_idPromotion" })
  idPromotion: number;

  @Column("int", { primary: true, name: "_idProduct" })
  idProduct: number;

  @Column("float", { name: "discount", nullable: true, precision: 12 })
  discount: number | null;

  @ManyToOne(() => Product, (product) => product.promotionproducts, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "_idProduct", referencedColumnName: "id" }])
  idProduct2: Product;

  @ManyToOne(() => Promotion, (promotion) => promotion.promotionproducts, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "_idPromotion", referencedColumnName: "id" }])
  idPromotion2: Promotion;
}
