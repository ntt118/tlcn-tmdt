import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { User } from "./User";
import { Orderdetail } from "./Orderdetail";

@Index("fk_order_user1_idx", ["userId"], {})
@Entity("order", { schema: "ecommerce" })
export class Order {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("datetime", { name: "dateCreate", nullable: true })
  dateCreate: Date | null;

  @Column("float", { name: "total", nullable: true, precision: 12 })
  total: number | null;

  @Column("int", { name: "status", nullable: true })
  status: number | null;

  @Column("int", { name: "user_id" })
  userId: number;

  @Column("tinyint", { name: "isPayment", nullable: true })
  isPayment: number | null;

  @Column("tinyint", { name: "paymentType", nullable: true })
  paymentType: number | null;

  @Column("varchar", { name: "address", nullable: true, length: 500 })
  address: string | null;

  @ManyToOne(() => User, (user) => user.orders, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "id" }])
  user: User;

  @OneToMany(() => Orderdetail, (orderdetail) => orderdetail.order)
  orderdetails: Orderdetail[];
}
