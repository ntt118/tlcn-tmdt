import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Productotherdetail } from "./Productotherdetail";
import { User } from "./User";

@Index("_id_UNIQUE", ["id"], { unique: true })
@Index("fk_cart_productotherdetail1_idx", ["productotherdetailId"], {})
@Index("fk_cart_user1_idx", ["userId"], {})
@Entity("cart", { schema: "ecommerce" })
export class Cart {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("int", { name: "quantity" })
  quantity: number;

  @Column("int", { name: "user_id" })
  userId: number;

  @Column("int", { name: "productotherdetail__id" })
  productotherdetailId: number;

  @ManyToOne(
    () => Productotherdetail,
    (productotherdetail) => productotherdetail.carts,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "productotherdetail__id", referencedColumnName: "id" }])
  productotherdetail: Productotherdetail;

  @ManyToOne(() => User, (user) => user.carts, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "id" }])
  user: User;
}
