import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("shop", { schema: "ecommerce" })
export class Shop {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("varchar", { name: "name", length: 100 })
  name: string;

  @Column("varchar", { name: "description", nullable: true, length: 500 })
  description: string | null;

  @Column("varchar", { name: "address", nullable: true, length: 500 })
  address: string | null;

  @Column("varchar", { name: "phoneNumber", nullable: true, length: 20 })
  phoneNumber: string | null;

  @Column("varchar", { name: "email", nullable: true, length: 45 })
  email: string | null;

  @Column("varchar", { name: "facebook", nullable: true, length: 50 })
  facebook: string | null;

  @Column("longblob", { name: "image", nullable: true })
  image: Buffer | null;
}
