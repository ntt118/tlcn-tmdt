import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";

@Entity("role", { schema: "ecommerce" })
export class Role {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("varchar", { name: "code", length: 10 })
  code: string;

  @Column("varchar", { name: "name", length: 45 })
  name: string;

  @Column("varchar", { name: "description", nullable: true, length: 500 })
  description: string | null;

  @OneToMany(() => User, (user) => user.role)
  users: User[];
}
