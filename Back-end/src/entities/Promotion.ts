import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Promotionproduct } from "./Promotionproduct";

@Entity("promotion", { schema: "ecommerce" })
export class Promotion {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("varchar", { name: "name", nullable: true, length: 80 })
  name: string | null;

  @Column("varchar", { name: "description", nullable: true, length: 500 })
  description: string | null;

  @Column("varchar", { name: "code", nullable: true, length: 45 })
  code: string | null;

  @Column("datetime", { name: "dateFrom", nullable: true })
  dateFrom: Date | null;

  @Column("datetime", { name: "dateTo", nullable: true })
  dateTo: Date | null;

  @Column("tinyint", { name: "status", nullable: true })
  status: number | null;

  @OneToMany(
    () => Promotionproduct,
    (promotionproduct) => promotionproduct.idPromotion2
  )
  promotionproducts: Promotionproduct[];
}
