import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Productotherdetail } from "./Productotherdetail";
import { Order } from "./Order";

@Index("fk_orderdetail_productotherdetail1_idx", ["productotherdetailId"], {})
@Index("fk_order_has_product_order1_idx", ["orderId"], {})
@Entity("orderdetail", { schema: "ecommerce" })
export class Orderdetail {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("int", { name: "order_id" })
  orderId: number;

  @Column("int", { name: "productotherdetail_id" })
  productotherdetailId: number;

  @Column("int", { name: "quantity", nullable: true })
  quantity: number | null;

  @Column("varchar", { name: "comment", nullable: true, length: 100 })
  comment: string | null;

  @Column("int", { name: "rate", nullable: true })
  rate: number | null;

  @ManyToOne(
    () => Productotherdetail,
    (productotherdetail) => productotherdetail.orderdetails,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "productotherdetail_id", referencedColumnName: "id" }])
  productotherdetail: Productotherdetail;

  @ManyToOne(() => Order, (order) => order.orderdetails, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "order_id", referencedColumnName: "id" }])
  order: Order;
}
