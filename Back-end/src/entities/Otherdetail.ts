import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Size } from "./Size";
import { Color } from "./Color";
import { Productotherdetail } from "./Productotherdetail";

@Index("_id_UNIQUE", ["id"], { unique: true })
@Index("fk_otherdetail_color1_idx", ["colorId"], {})
@Index("fk_otherdetail_size1_idx", ["sizeId"], {})
@Entity("otherdetail", { schema: "ecommerce" })
export class Otherdetail {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("int", { name: "size_id", nullable: true, unsigned: true })
  sizeId: number | null;

  @Column("int", { name: "color_id", nullable: true, unsigned: true })
  colorId: number | null;

  @Column("tinyint", { name: "status" })
  status: number;

  @ManyToOne(() => Size, (size) => size.otherdetails, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "size_id", referencedColumnName: "id" }])
  size: Size;

  @ManyToOne(() => Color, (color) => color.otherdetails, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "color_id", referencedColumnName: "id" }])
  color: Color;

  @OneToMany(
    () => Productotherdetail,
    (productotherdetail) => productotherdetail.otherdetail
  )
  productotherdetails: Productotherdetail[];
}
