import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Product } from "./Product";

@Index("fk_imageProduct_product1_idx", ["productId"], {})
@Entity("imageproduct", { schema: "ecommerce" })
export class Imageproduct {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("int", { name: "product_id", nullable: true })
  productId: number | null;

  @Column("mediumtext", { name: "image", nullable: true })
  image: string | null;

  @ManyToOne(() => Product, (product) => product.imageproducts, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "product_id", referencedColumnName: "id" }])
  product: Product;
}
