import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./Product";

@Entity("brand", { schema: "ecommerce" })
export class Brand {
  @PrimaryGeneratedColumn({ type: "int", name: "_id" })
  id: number;

  @Column("varchar", { name: "name", nullable: true, length: 45 })
  name: string | null;

  @Column("tinyint", { name: "status", nullable: true })
  status: number | null;

  @Column("tinyint", { name: "isActive", nullable: true })
  isActive: number | null;

  @OneToMany(() => Product, (product) => product.brand)
  products: Product[];
}
