import { Request, Response } from "express";
import { RoleService } from '../services/Role.service';
import { Role } from "../entities/Role";
import { HttpResponseModel } from '../models/http-response.model'
import { format } from "url";

export async function getList(req: Request, res: Response) {
    var rs = await RoleService.Ins.getList();
    res.json(rs);
}

export async function create(req: Request, res: Response) {
    var response: HttpResponseModel<any> = {
        status: "Success"
    }

    var role = await RoleService.Ins.getByName(req.body.name)
    if (role) {
        response.errMessage = "Name existed";
        response.status = "Fail"
        return res.json(response)

    }
    const abc = {
        name: req.body.name,
        permissions: req.body.permissions
    };
    await RoleService.Ins.create(abc);
    response.Value = abc;
    return res.json(response);
}

export async function getById(req: Request, res: Response) {
    var respronse: HttpResponseModel<Role> = {
        status: "Success"
    }
    var Role = await RoleService.Ins.getById(req.body.id);
    if (Role) {
        respronse.Value = Role;
        return res.json(respronse)
    }
    respronse.Value = null
    respronse.status = "Fail"
    respronse.errMessage = "Role not exist"
    return res.json(respronse)
}

export async function update(req: Request, res: Response) {
    var respronse: HttpResponseModel<any> = {
        status: "Success"
    }

    var role = await RoleService.Ins.getById(req.body.id);

    if (!role) {
        respronse.errMessage = "Id role not exist";
        respronse.status = "Fail"
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    var checkNameRole = await RoleService.Ins.getByName(req.body.Value.name);
    if (checkNameRole && checkNameRole.id != req.body.id) {
        respronse.errMessage = "name existed";
        respronse.status = "Fail";
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    role.name = req.body.Value.name;
    // role.permissions = req.body.Value.permissions;
    await RoleService.Ins.update(role)

    respronse.Value = role;
    return res.json(respronse);
}


export async function deleteRole(req: Request, res: Response) {
    var respronse: HttpResponseModel<Role> = {
        status: "Success"
    }

    var Role = await RoleService.Ins.getById(req.body.id);

    if (!Role) {
        respronse.errMessage = "Id role not exist, can't delete";
        respronse.status = "Fail"
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    await RoleService.Ins.delete(Role.id)
    respronse.Value = Role;
    return res.json(respronse);
}