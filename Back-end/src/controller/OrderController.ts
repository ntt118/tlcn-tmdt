import { CartService } from './../services/cart.service';
import { Request, Response } from "express";
import { OrderService } from '../services/order.service';
import { Order } from "../entities/Order";
import { HttpResponseModel } from "../models/http-response.model";
import { format } from "url";
import { isBuffer } from 'util';
import { UserService } from '../services/user.service'
import { ShopService } from '../services/shop.service'
import { OrderDetailService } from "../services/orderDetail.service";
import { ProductService } from "../services/product.service";
import { User } from "../entities/User";

var crypto = require('crypto');

import { v1 as uuidv1 } from 'uuid';
import { ProductOtherdetailService } from '../services/product-otherdetail.service';

const https = require('https');
//parameters send to MoMo get get payUrl
//pass empty value if your merchant does not have stores else merchantName=[storeName]; merchantId=[storeId] to identify a transaction map with a physical store





export async function all(req: Request, res: Response) {    
    var rs = await OrderService.Ins.getAll();
    for(var r of rs){
        r.user = await UserService.Ins.getById(r.userId)
        delete r.user.hash
        delete r.user.salt
    }
    res.json(rs);
}


export async function paymentQRCode(req: Request, res: Response) {    
    var order = await OrderService.Ins.getOrder(req.body.id);
    if(order){
        var shop = await ShopService.Ins.getShop();
        if(shop){
            var user = await UserService.Ins.getById(order.userId);

            var endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor"
            var hostname = "https://test-payment.momo.vn"
            var path = "/gw_payment/transactionProcessor"
            var partnerCode = "MOMOSJUX20201219"
            var accessKey = "xxg6MwSza0DyspbR"
            var serectkey = "TZNujGLmKfSA7hmSo1TVBfM1sYHVkgGt"
            var orderInfo = "Tên khách hàng: " + user.name
            var notifyurl= "http://localhost:4200/order-detail"
            var returnUrl = "http://localhost:3000/api/order/paid/" + order.id;
            var amount = order.total.toString();
            var orderId = uuidv1()
            var requestId = uuidv1()
            var requestType = "captureMoMoWallet"
            var extraData = "merchantName=tmdt;merchantId=tmdt" 

            var rawSignature =     "partnerCode="+partnerCode+
            "&accessKey="+accessKey+
            "&requestId="+requestId+
            "&amount="+amount+
            "&orderId="+orderId+
            "&orderInfo="+orderInfo+
            "&returnUrl="+returnUrl+
            "&notifyUrl="+notifyurl+
            "&extraData="+extraData
            
            try{

                var signature = crypto.createHmac('sha256', serectkey)
                .update(rawSignature)
                .digest('hex');


                var body = JSON.stringify({
                    partnerCode : partnerCode,
                    accessKey : accessKey,
                    requestId : requestId,
                    amount : amount,
                    orderId : orderId,
                    orderInfo : orderInfo,
                    returnUrl : returnUrl,
                    notifyUrl : notifyurl,
                    extraData : extraData,
                    requestType : requestType,
                    signature : signature,
                })


                var options = {
                    hostname: 'test-payment.momo.vn',
                    port: 443,
                    path: '/gw_payment/transactionProcessor',
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                      'Content-Length': Buffer.byteLength(body)
                   }
                  };


                var req1 = https.request(options, (res1) => {
                    console.log(`Status: ${res1.statusCode}`);
                    console.log(`Headers: ${JSON.stringify(res1.headers)}`);
                    res1.setEncoding('utf8');
                    res1.on('data', (body) => {
                        res.json({url : JSON.parse(body)})
                        return
                    });
                    res1.on('end', () => {
                        res1.on('end', () => {
                            console.log('No more data in response.');
                          });
                    });
                });

                req1.on('error', (e) => {
                    console.log(`problem with request: ${e.message}`);
                });
                req1.write(body);
                req1.end();
                
            }
            catch (err){
                console.log('err', err)
                res.json({name: false})
            }
            return;
        }
    }
    res.json( { name: false })
}

export async function getOrderToShipper(req: Request, res: Response) {    //0: cho xac nhan(admin), 1: cho xac nhan(shipper), 2: Dang giao, 3: Da giao, -1: khong nhan
    var rs = await OrderService.Ins.getOrderToShipper();
    for(var r of rs){
        r.user = await UserService.Ins.getById(r.userId)
    }
    res.json(rs);
}
export async function getOrderByUserId(req: Request, res: Response){
    var response : HttpResponseModel<Order[]> = {
        status : "Success"
    }
    var rs = await OrderService.Ins.getAll();
    for(var r of rs){
        // r.orderdetails = await OrderDetailService.Ins.getById(r.id)
        r.user = await UserService.Ins.getById(r.userId)
    }
    rs = rs.filter(r=>r.userId==req.body.id)
    if (rs) {
        response.Value = rs;
        return res.json(response);
    }

    response.Value=null;
    response.status = "Fail"
    response.errMessage = "Order not exists"
    return res.json(response)
}

export async function byId(req: Request, res: Response){
    var response : HttpResponseModel<Order> = {
        status : "Success"
    }
    
    var rs = await OrderService.Ins.getOrder(req.body.id);
    if(rs){
        // rs.orderdetails = await OrderDetailService.Ins.getById(rs.id)

        rs.user = await UserService.Ins.getById(rs.userId)
        delete rs.user.hash
        delete rs.user.salt

        const orderdetails = []
        await Promise.all(
            rs.orderdetails.map(async (item) => {
                const productotherdetail = await ProductOtherdetailService.Ins.getById(item.productotherdetailId)
                const product = await ProductService.Ins.getById(productotherdetail.productId)
                orderdetails.push({ ...item, productotherdetail, product })
                return { ...item, productotherdetail }
            })
        )
        rs.orderdetails = orderdetails;
            
        response.Value = rs;
        return res.json(response);
    }
    response.Value=null;
    response.status = "Fail"
    response.errMessage = "Order not exists"
    return res.json(response)
}

export async function create(req: Request, res: Response) {
    var response : HttpResponseModel<Order> = {
        status : "Success"
    }

    // Thêm trạng thái
    // 0 là chờ xác nhận
    // 1 là xác nhận
    // 2 là đang vận chuyển
    // 3 là đã nhận được hàng
    // 4 là huỷ
    let newOrder = new Order();
    newOrder = req.body
    newOrder.userId = req.body.userId;
    newOrder.dateCreate = new Date();
    newOrder.isPayment = 0;
    newOrder.status = 0;
    newOrder.paymentType = req.body.paymentType;
    const order = await OrderService.Ins.create(newOrder);
    if (order) {
        await CartService.Ins.deleteByUserId(req.body.userId);
    }
    var orderdetails = req.body.orderdetails;
    await Promise.all(
        orderdetails.map(async orderDetail => {
            return await OrderDetailService.Ins.createOrderDetail(order.id, orderDetail);
        })
    )
    newOrder.orderdetails = orderdetails;
    response.Value=newOrder;
    return res.json(response);
}

export async function cancelOrder(req: Request, res: Response) {
    var response : HttpResponseModel<Order> = {
        status : "Success"
    }

    const exist = await OrderService.Ins.getOrder(req.body.id);
    if(exist) {
        exist.status = 4;
        await OrderService.Ins.update(exist);
        response.Value = exist;
        return res.json(response);
    }
    response.Value = null;
    response.status = "Fail"
    response.errMessage = "Order not exists"
    return res.json(response);    
}

export async function paid(req: Request, res: Response) {
    var response : HttpResponseModel<Order> = {
        status : "Success"
    }
    var rs = await OrderService.Ins.getOrder(req.params.id);
    if(rs){
        rs.isPayment = 1;
        await OrderService.Ins.update(rs);
        response.Value = rs;
        res.writeHead(301,
            {Location: "http://localhost:4200/order-detail"}
          );
        res.end();
    }
    else
    {
        response.Value=req.body
        response.status = "Fail"
        response.errMessage = "Order not exists"
        return res.json(response)
    }
}

export async function update(req: Request, res: Response) {
    var response : HttpResponseModel<Order> = {
        status : "Success"
    }
    var rs = await OrderService.Ins.getOrder(req.body.id);
    if(rs){
        let _Order = new Order();
        _Order = req.body.Value
        _Order.id = req.body.id

        await reduceProduct(req)

        var rss = await OrderService.Ins.update(_Order);
        response.Value = _Order;
        return res.json(response);
    }
    else
    {
        response.Value=req.body
        response.status = "Fail"
        response.errMessage = "Order not exists"
        return res.json(response)
    }
}

export async function remove(req: Request, res: Response) {    
    var response : HttpResponseModel<Order> = {
        status : "Success"
    }  

    var rs = await OrderService.Ins.getOrder(req.body);

    if(rs){
        await OrderService.Ins.delete(req.body.id);
        response.Value = rs;
        return res.json(response);
    }
    else
    {
        response.errMessage = "Order not exists, can't delete";
        response.status = "Fail";
        response.Value= req.body
        return res.json(response);
    }
}
export async function reduceProduct(req) {
    if(req.body.Value.status == 1){
        var listOrderDatail = await OrderDetailService.Ins.getById(req.body.id)
        listOrderDatail.forEach(async rs => {
            var pro = null
            // var pro = await ProductService.Ins.getById(rs.productId)
            pro.quantity -= rs.quantity
            await ProductService.Ins.update(pro)
        })
    }
    else if(req.body.Value.status == -1){
        var listOrderDatail = await OrderDetailService.Ins.getById(req.body.id)
        listOrderDatail.forEach(async rs => {
            var pro = null
            // var pro = await ProductService.Ins.getById(rs.productId)
            pro.quantity += rs.quantity
            await ProductService.Ins.update(pro)
        })
    }
}

export async function getForDashboard(req, res) {
    const data = await OrderService.Ins.getAll();
    const totalOrder = data.length;
    const totalUser = await (await UserService.Ins.getList()).length;
    const rs = [];

    for (let i = 0; i < 12; i++) {
        const order = await OrderService.Ins.getOrderByDate(i, req.body.year);
        rs.push(order.reduce((pre, cur) => {
            return pre + (+cur.total || 0);
        }, 0))
    }
    
    const chart2 = []
    for (let i = 0; i < 5; i++) {
        const count = await OrderService.Ins.getCountStatus(i);
        chart2.push(count);
    }


    return res.json({
        chart1: rs,
        chart2,
        totalUser,
        totalOrder,
    });
}