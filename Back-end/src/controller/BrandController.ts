import { Request, Response } from "express";
import { Category } from "../entities/Category";
import { HttpResponseModel } from '../models/http-response.model'
import { BrandService } from "../services/brand.service";
import { ProductService } from '../services/product.service';

export async function getList(req: Request, res: Response){
    var rs = await BrandService.Ins.getList();
    res.json(rs);
}

export async function create(req: Request, res: Response){
    var response : HttpResponseModel<Category> = {
        status : "Success"
    }

    var brand = await BrandService.Ins.getByName(req.body.name)
    if(brand && brand.isActive == 1){
        response.errMessage = "Name existed";
        response.status = "Fail"
        return res.json(response)
    }
    var abc = new Category;
    abc.name = req.body.name;
    abc.status = 0
    abc.isActive = 1;
    await BrandService.Ins.create(abc);
    response.Value = abc;
    return res.json(response);
}

export async function getById(req: Request, res: Response){
    var respronse : HttpResponseModel<Category> = {
        status : "Success"
    }
    var brand = await BrandService.Ins.getById(req.body.id);
    if(brand){
        respronse.Value = brand;
        return res.json(respronse)
    }
    respronse.Value = null
    respronse.status = "Fail"
    respronse.errMessage = "Category not exist"
    return res.json(respronse)
}

export async function update(req: Request, res: Response){
    var respronse : HttpResponseModel<Category> = {
        status : "Success"
    }

    var brand = await BrandService.Ins.getById(req.body.id);

    if(!brand){
        respronse.errMessage = "Id Category not exist";
        respronse.status = "Fail"
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    var checkNameRole = await BrandService.Ins.getByName(req.body.Value.name);
    if(checkNameRole){
        if(checkNameRole.id != req.body.id){
            respronse.errMessage = "name existed";
            respronse.status = "Fail";
            respronse.Value = req.body.Value
            return res.json(respronse)
        }
    }
    brand.name = req.body.Value.name;
    brand.status = req.body.Value.status;
   
    await BrandService.Ins.update(brand)

    respronse.Value = brand;
    return res.json(respronse);
}


export async function deleteCategory(req: Request, res: Response){
    var respronse : HttpResponseModel<Category> = {
        status : "Success"
    }

    var brand = await BrandService.Ins.getById(req.body.id);

    if(!brand){
        respronse.errMessage = "Id brand not exist, can't delete";
        respronse.status = "Fail"
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    brand.isActive = 0;
    for(let p of brand.products){
        p.isActive = 0;
        ProductService.Ins.update(p)
    }
    await BrandService.Ins.update(brand)
    respronse.Value = brand;
    return res.json(respronse);
}