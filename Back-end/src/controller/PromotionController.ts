import { Request, Response } from "express";
import { PromotionService } from '../services/promotion.service';
import { Promotion } from "../entities/Promotion";
import { HttpResponseModel } from '../models/http-response.model'
import { format } from "url";

export async function getList(req: Request, res: Response){
    var rs = await PromotionService.Ins.getList();
    res.json(rs);
}

export async function create(req: Request, res: Response){
    var response : HttpResponseModel<Promotion> = {
        status : "Success"
    }

    var promotion = await PromotionService.Ins.getByName(req.body.name)
    if(promotion){
        response.errMessage = "Name existed";
        response.status = "Fail"
        return res.json(response)
    }
    var promo = new Promotion;
    promo.name = req.body.name;
    promo.code = req.body.code;
    promo.description = req.body.description;
    promo.dateFrom = req.body.dateFrom;
    promo.dateTo = req.body.dateTo;
    promo.status = 1;
    await PromotionService.Ins.create(promo);
    response.Value = promo;
    return res.json(response);
}

export async function getById(req: Request, res: Response){
    var respronse : HttpResponseModel<Promotion> = {
        status : "Success"
    }
    var promotion = await PromotionService.Ins.getById(req.body.id);
    if(promotion){
        respronse.Value = promotion;
        return res.json(respronse)
    }
    respronse.Value = null
    respronse.status = "Fail"
    respronse.errMessage = "Promotion not exist"
    return res.json(respronse)
}

export async function update(req: Request, res: Response){
    var respronse : HttpResponseModel<Promotion> = {
        status : "Success"
    }

    var promotion = await PromotionService.Ins.getById(req.body.id);

    if(!promotion){
        respronse.errMessage = "Promotion not exist";
        respronse.status = "Fail"
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    promotion.name = req.body.Value.name;
    promotion.code = req.body.code;
    promotion.description = req.body.description;
    promotion.dateFrom = req.body.Value.dateFrom;
    promotion.dateTo = req.body.Value.dateTo;
   
    await PromotionService.Ins.update(promotion)

    respronse.Value = promotion;
    return res.json(respronse);
}


export async function deletePromotion(req: Request, res: Response){
    var respronse : HttpResponseModel<Promotion> = {
        status : "Success"
    }

    var promotion = await PromotionService.Ins.getById(req.body.id);

    if(!promotion){
        respronse.errMessage = "Promotion not exist, can't delete";
        respronse.status = "Fail"
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    promotion.status = 0;
    await PromotionService.Ins.update(promotion);
    // await PromotionService.Ins.delete(promotion)
    respronse.Value = promotion;
    return res.json(respronse);
}