import { Request, Response } from "express";
import { ProductService } from "../services/product.service";
import { Product } from "../entities/Product";
import { HttpResponseModel } from "../models/http-response.model";
import { ProductModel } from "../models/product.model";
import { OtherdetailService } from "../services/otherDetail.service";
import { Otherdetail } from "../entities/Otherdetail";
import { ProductOtherdetailService } from "../services/product-otherdetail.service";
import { Productotherdetail } from "../entities/Productotherdetail";
import { Imageproduct } from "../entities/Imageproduct";
import { throws } from "assert";

export async function all(req: Request, res: Response) {
  var rs = await ProductService.Ins.getAll();
  var result: ProductModel[] = rs;
  for (const product of result) {
    const productotherdetails = [];
    for (const item of product.productotherdetails) {
      const otherdetail = await OtherdetailService.Ins.getById(
        item.otherdetailId
      );
      const productotherdetail = {
        otherdetailId: otherdetail.id,
        sizeId: otherdetail.sizeId,
        colorId: otherdetail.colorId,
        sizeName: otherdetail.size.name,
        colorName: otherdetail.color.name,
        quantity: item.quantity,
        price: item.price,
      };
      productotherdetails.push(productotherdetail);
    }
    product.productotherdetails = productotherdetails;
  }
  res.json(result);
}
export async function byId(req: Request, res: Response) {
  var response: HttpResponseModel<ProductModel> = {
    status: "Success",
  };

  var rs = await ProductService.Ins.getById(req.body.id);
  if (rs) {
    var result: ProductModel = rs;
    const productotherdetails = [];
    for (const item of rs.productotherdetails) {
      const otherdetail = await OtherdetailService.Ins.getById(
        item.otherdetailId
      );
      const productotherdetail = {
        otherdetailId: otherdetail.id,
        sizeId: otherdetail.sizeId,
        colorId: otherdetail.colorId,
        sizeName: otherdetail.size.name,
        colorName: otherdetail.color.name,
        quantity: item.quantity,
        price: item.price,
      };
      productotherdetails.push(productotherdetail);
    }
    result.productotherdetails = productotherdetails;
    // for(let i in rs.imageproducts){
    //     result.imageproducts[i].image = Buffer.from(rs.imageproducts[i].image).toString('ascii')
    // }
    response.Value = result;
    return res.json(response);
  }
  response.Value = req.body;
  response.status = "Fail";
  response.errMessage = "Product not exists";
  return res.json(response);
}
export async function insert(req: Request, res: Response) {
  var response: HttpResponseModel<any> = {
    status: "Success",
  };

  var rs = await ProductService.Ins.getByName(req.body.name);

  if (rs) {
    if (rs.status == 0) {
      response.errMessage = "Product existed but haven't confirm!";
    } else if (rs.status == 1) {
      response.errMessage = "Product existed!";
    } else if (rs.status == 2) {
      response.errMessage = "Product existed but it don't use!";
    }
    response.status = "Fail";
    response.Value = req.body;
    return res.json(response);
  }

  let _Product = new Product();
  _Product = req.body;
  _Product.isActive = 1;

  const listImg = [];
  for (let i in _Product.imageproducts) {
    const img = _Product.imageproducts[i];
    await ProductService.Ins.insertImageProduct(img);
    listImg.push(img);
  }
  _Product.imageproducts = listImg;
  const otherdetails = req.body.otherdetails;
  _Product.quantity = otherdetails.reduce((preValue, curValue) => {
    return preValue + curValue.quantity;
  }, 0);
  const newProduct = await ProductService.Ins.insert(_Product);
  await Promise.all(
    otherdetails.map(async (rs) => {
      let otherdetail = await OtherdetailService.Ins.getOtherDetail(
        rs.sizeId,
        rs.colorId
      );
      if (!otherdetail) {
        const newOtherdetail = {
          sizeId: rs.sizeId,
          colorId: rs.colorId,
          status: 1,
        };
        await OtherdetailService.Ins.createOtherdetail(newOtherdetail);
        otherdetail = await OtherdetailService.Ins.getOtherDetail(
          rs.sizeId,
          rs.colorId
        );
      }
      const data = {
        otherdetailId: otherdetail.id,
        price: rs.price,
        quantity: rs.quantity,
        productId: newProduct.id,
      };
      await ProductOtherdetailService.Ins.createProductOtherdetail(data);
    })
  );

  response.Value = _Product;
  return res.json(response);
}
export async function update(req: Request, res: Response) {
  var response: HttpResponseModel<Product> = {
    status: "Success",
  };
  var rs = await ProductService.Ins.getById(req.body.id);
  if (rs) {
    rs = Object.assign(rs, req.body.Value);

    let imageOld = await ProductService.Ins.getImageByProductId(req.body.id);
    imageOld.forEach(async (r) => {
      await ProductService.Ins.deleteImageProduct(r.id);
    });

    const otherdetails = req.body.Value.otherdetails;
    rs.quantity = otherdetails.reduce((preValue, curValue) => {
      return preValue + curValue.quantity;
    }, 0);

    await Promise.all(
      otherdetails.map(async (rss) => {
        const otherdetail = await OtherdetailService.Ins.getBySizeIdAndColorId(
          rss.sizeId,
          rss.colorId
        );
        if (otherdetail) {
          const productOtherdetail = await ProductOtherdetailService.Ins.getProducOtherdetail(rs.id, otherdetail.id);
          productOtherdetail.quantity = rss.quantity;
          productOtherdetail.price = rss.price;
          await ProductOtherdetailService.Ins.updateProducOtherdetail(productOtherdetail);
        } else {
          let otherdetail = await OtherdetailService.Ins.getOtherDetail(
            rss.sizeId,
            rss.colorId
          );
          if (!otherdetail) {
            const newOtherdetail = {
              sizeId: rss.sizeId,
              colorId: rss.colorId,
              status: 1,
            };
            await OtherdetailService.Ins.createOtherdetail(newOtherdetail);
            otherdetail = await OtherdetailService.Ins.getOtherDetail(
              rss.sizeId,
              rss.colorId
            );
          }
          const data = {
            otherdetailId: otherdetail.id,
            price: rss.price,
            quantity: rss.quantity,
            productId: rs.id,
          };
          await ProductOtherdetailService.Ins.createProductOtherdetail(data);
        }
      })
    );
    const imageproducts = req.body.Value.imageproducts;
    await Promise.all(
      imageproducts.map(async (i) => {
        const item: Imageproduct = {
          productId: rs.id,
          image: i.image,
          id: null,
          product: rs,
        };
        await ProductService.Ins.insertImageProduct(item);
      })
    );
    delete rs.imageproducts;
    delete rs.productotherdetails;
    delete rs.promotionproducts;
    await ProductService.Ins.update(rs);
    response.Value = rs;
    return res.json(response);
  } else {
    response.Value = req.body;
    response.status = "Fail";
    response.errMessage = "Product not exists";
    return res.json(response);
  }
}

export async function remove(req: Request, res: Response) {
  var response: HttpResponseModel<Product> = {
    status: "Success",
  };

  var rs = await ProductService.Ins.getById(req.body.id);

  if (rs) {
    rs.isActive = 0;
    await ProductService.Ins.update(rs);
    response.Value = rs;
    return res.json(response);
  } else {
    response.errMessage = "Product not exists, can't delete";
    response.status = "Fail";
    response.Value = req.body;
    return res.json(response);
  }
}
