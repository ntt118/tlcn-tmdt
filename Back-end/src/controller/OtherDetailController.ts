import { Request, Response } from "express";
import { OtherdetailService } from '../services/otherDetail.service';
import { Otherdetail } from "../entities/Otherdetail";
import { HttpResponseModel } from '../models/http-response.model'
import { Size } from "../entities/Size";
import { Color } from "../entities/Color";

export async function getList(req: Request, res: Response){
    var rs = [];
    if (req.body.type === "SIZE") {
        rs = await OtherdetailService.Ins.getListSize();
    } else if (req.body.type === "COLOR") {
        rs = await OtherdetailService.Ins.getListColor();
    } else if (req.body.type === "ALL"){
        rs = await OtherdetailService.Ins.getList();
    }
    res.json(rs);
}

export async function create(req: Request, res: Response){
    var response : HttpResponseModel<Color | Size> = {
        status : "Success"
    }

    if(req.body.type === "SIZE"){
        var category = await OtherdetailService.Ins.getByNameSize(req.body.name)
        if(category){
            response.errMessage = "Name existed";
            response.status = "Fail"
            return res.json(response)
        }
        var abc = new Size;
        abc.status = 1
        abc.name = req.body.name;
        await OtherdetailService.Ins.createSize(abc);
        response.Value = abc;
        return res.json(response);
    } else if(req.body.type === "COLOR") {
        var category = await OtherdetailService.Ins.getByNameColor(req.body.name)
        if(category){
            response.errMessage = "Name existed";
            response.status = "Fail"
            return res.json(response)
        }
        var abc = new Color;
        abc.status = 1
        abc.name = req.body.name;
        await OtherdetailService.Ins.createColor(abc);
        response.Value = abc;
        return res.json(response);
    } else {
        response.errMessage = "Xảy ra lỗi vui lòng thử lại sau."
        return res.json(response)
    }
}

// export async function getById(req: Request, res: Response){
//     var respronse : HttpResponseModel<Otherdetail> = {
//         status : "Success"
//     }
//     var category = await OtherdetailService.Ins.getById(req.body.id);
//     if(category){
//         respronse.Value = category;
//         return res.json(respronse)
//     }
//     respronse.Value = null
//     respronse.status = "Fail"
//     respronse.errMessage = "Otherdetail not exist"
//     return res.json(respronse)
// }

export async function update(req: Request, res: Response){
    var response : HttpResponseModel<any> = {
        status : "Success"
    }

    if(req.body.Value.type === "SIZE"){
        var category = await OtherdetailService.Ins.getSizeById(req.body.id)
        if(!category){
            response.errMessage = "Name does not exist";
            response.status = "Fail"
            response.Value = req.body.Value
            return res.json(response)
        }
        category.name = req.body.Value.name;
        const xyz = await OtherdetailService.Ins.updateSize(category);
        response.Value = xyz;
        return res.json(response);
    } else if(req.body.Value.type === "COLOR") {
        var category = await OtherdetailService.Ins.getColorById(req.body.id)
        if(!category){
            response.errMessage = "Name does not exist";
            response.status = "Fail"
            response.Value = req.body.Value
            return res.json(response)
        }
        category.name = req.body.Value.name;
        const xyz = await OtherdetailService.Ins.updateColor(category);
        response.Value = xyz;
        return res.json(response);
    } else {
        response.errMessage = "Xảy ra lỗi vui lòng thử lại sau."
        return res.json(response)
    }

    // var category = await OtherdetailService.Ins.getById(req.body.id);

    // if(!category){
    //     respronse.errMessage = "Id Otherdetail not exist";
    //     respronse.status = "Fail"
    //     respronse.Value = req.body.Value
    //     return res.json(respronse)
    // }
    // var checkNameRole = await OtherdetailService.Ins.getByName(req.body.Value.name);
    // if(checkNameRole){
    //     if(checkNameRole.id != req.body.id){
    //         respronse.errMessage = "name existed";
    //         respronse.status = "Fail";
    //         respronse.Value = req.body.Value
    //         return res.json(respronse)
    //     }
    // }
    // category.status = req.body.Value.status;
   
    // await OtherdetailService.Ins.update(category)

    // respronse.Value = category;
    // return res.json(response);
}


export async function deleteOtherdetail(req: Request, res: Response){
    var response : HttpResponseModel<any> = {
        status : "Success"
    }

    if(req.body.type === "SIZE"){
        var category = await OtherdetailService.Ins.getSizeById(req.body.id)
        if(!category){
            response.errMessage = "Name does not exist";
            response.status = "Fail"
            response.Value = req.body.Value
            return res.json(response)
        }
        category.status = 0;
        const xyz = await OtherdetailService.Ins.updateSize(category);
        response.Value = xyz;
        return res.json(response);
    } else if(req.body.type === "COLOR") {
        var category = await OtherdetailService.Ins.getColorById(req.body.id)
        if(!category){
            response.errMessage = "Name does not exist";
            response.status = "Fail"
            response.Value = req.body.Value
            return res.json(response)
        }
        category.status = 0;
        const xyz = await OtherdetailService.Ins.updateColor(category);
        response.Value = xyz;
        return res.json(response);
    } else {
        response.errMessage = "Xảy ra lỗi vui lòng thử lại sau."
        return res.json(response)
    }

    // var category = await OtherdetailService.Ins.getById(req.body.id);

    // if(!category){
    //     respronse.errMessage = "Id category not exist, can't delete";
    //     respronse.status = "Fail"
    //     respronse.Value = req.body.Value
    //     return res.json(respronse)
    // }
    // category.status = 0;
    // await OtherdetailService.Ins.update(category);
    // respronse.Value = category;
    // return res.json(respronse);
}

export async function listTypeOtherDetail(req: Request, res: Response) {
    var respronse : HttpResponseModel<Otherdetail> = {
        status : "Success"
    }
    var list = await OtherdetailService.Ins.listTypeOtherDetail()
    list = list.map(r => r.type)
    respronse.Value = list
    return res.json(respronse)
}