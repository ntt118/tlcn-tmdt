import { OrderDetailService } from './../services/orderDetail.service';
import { Request, Response } from "express";
import { Orderdetail } from "../entities/Orderdetail";
import { HttpResponseModel } from '../models/http-response.model';
import { OrderService } from '../services/order.service';

export async function all(req: Request, res: Response) {    
    var rs = await OrderDetailService.Ins.getAll();
    res.json(rs);
}
export async function byOrderId(req: Request, res: Response){
    var id: any ={
        "orderId": req.body.order_id
    }
    var response : HttpResponseModel<Orderdetail[]> = {
        status : "Success"
    }
    
    var rs = await OrderDetailService.Ins.getById(req.body.order_id);

    if(rs){
        response.Value = rs;
        return res.json(response);
    }
    response.Value=req.body;
    response.status = "Fail"
    response.errMessage = "Order Detail not exists"
    return res.json(response)
}



