import { Request, Response } from "express";
import { CategoryService } from '../services/category.service';
import { Category } from "../entities/Category";
import { HttpResponseModel } from '../models/http-response.model'
import { format } from "url";
import { ProductService } from '../services/product.service';

export async function getList(req: Request, res: Response){
    var rs = await CategoryService.Ins.getList();
    res.json(rs);
}

export async function create(req: Request, res: Response){
    var response : HttpResponseModel<Category> = {
        status : "Success"
    }

    var category = await CategoryService.Ins.getByName(req.body.name)
    if(category && category.isActive == 1){
        response.errMessage = "Name existed";
        response.status = "Fail"
        return res.json(response)
    }
    var abc = new Category;
    abc.name = req.body.name;
    abc.status = 0
    abc.isActive = 1;
    await CategoryService.Ins.create(abc);
    response.Value = abc;
    return res.json(response);
}

export async function getById(req: Request, res: Response){
    var respronse : HttpResponseModel<Category> = {
        status : "Success"
    }
    var category = await CategoryService.Ins.getById(req.body.id);
    if(category){
        respronse.Value = category;
        return res.json(respronse)
    }
    respronse.Value = null
    respronse.status = "Fail"
    respronse.errMessage = "Category not exist"
    return res.json(respronse)
}

export async function update(req: Request, res: Response){
    var respronse : HttpResponseModel<Category> = {
        status : "Success"
    }

    var category = await CategoryService.Ins.getById(req.body.id);

    if(!category){
        respronse.errMessage = "Id Category not exist";
        respronse.status = "Fail"
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    var checkNameRole = await CategoryService.Ins.getByName(req.body.Value.name);
    if(checkNameRole){
        if(checkNameRole.id != req.body.id){
            respronse.errMessage = "name existed";
            respronse.status = "Fail";
            respronse.Value = req.body.Value
            return res.json(respronse)
        }
    }
    category.name = req.body.Value.name;
    category.status = req.body.Value.status;
   
    await CategoryService.Ins.update(category)

    respronse.Value = category;
    return res.json(respronse);
}


export async function deleteCategory(req: Request, res: Response){
    var respronse : HttpResponseModel<Category> = {
        status : "Success"
    }

    var category = await CategoryService.Ins.getById(req.body.id);

    if(!category){
        respronse.errMessage = "Id category not exist, can't delete";
        respronse.status = "Fail"
        respronse.Value = req.body.Value
        return res.json(respronse)
    }
    category.isActive = 0;
    for(let p of category.products){
        p.isActive = 0;
        ProductService.Ins.update(p)
    }
    await CategoryService.Ins.update(category)
    respronse.Value = category;
    return res.json(respronse);
}