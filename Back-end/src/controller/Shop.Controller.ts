import { Shop } from './../entities/Shop';
import { Request, Response } from "express";
import { ShopService } from '../services/shop.service';
import { HttpResponseModel } from '../models/http-response.model'
import { ShopModel } from '../models/shop.model'

export async function getShop(req: Request, res: Response){
    var respronse : HttpResponseModel<ShopModel> = {
        status : "Success"
    }
    var shop = await ShopService.Ins.getShop();
    if(shop){
        var returnShop : ShopModel = {
            id: shop.id,
            address: shop.address,
            description: shop.description,
            email: shop.email,
            facebook : shop.facebook,
            name: shop.name,
            image: shop.image?Buffer.from(shop.image).toString('ascii'):null,
            phoneNumber: shop.phoneNumber
        }
        respronse.Value = returnShop;
        return res.json(respronse)
    }
    respronse.Value = null
    respronse.status = "Fail"
    respronse.errMessage = "Shop not found"
    return res.json(respronse)
}

export async function update(req: Request, res: Response){
    var respronse : HttpResponseModel<Shop> = {
        status : "Success"
    }

    var shop = await ShopService.Ins.getShop();

    // if(!shop){
    //     // respronse.errMessage = "Shop not found";
    //     // respronse.status = "Fail"
    //     // respronse.Value = req.body.Value
    //     var newShop: Shop = {
    //         id : null,
    //         name : req.body.Value.name?req.body.Value.name:"",
    //         email : req.body.Value.email?req.body.Value.email:"",
    //         phoneNumber : req.body.Value.phoneNumber?req.body.Value.phoneNumber:"",
    //         address : req.body.Value.address?req.body.Value.address:"",
    //         description : req.body.Value.description?req.body.Value.description:"",
    //         facebook : req.body.Value.facebook?req.body.Value.facebook:"",
    //         image : req.body.Value.image?req.body.Value.image:"",
    //         accessKey : ""
    //     }
       
    //     await ShopService.Ins.update(newShop)
    //     respronse.Value = shop;
    //     return res.json(respronse)
    // }

    shop.name = req.body.Value.name?req.body.Value.name:shop.name
    shop.email = req.body.Value.email?req.body.Value.email:shop.email
    shop.phoneNumber = req.body.Value.phoneNumber?req.body.Value.phoneNumber:shop.phoneNumber
    shop.address = req.body.Value.address?req.body.Value.address:shop.address
    shop.description = req.body.Value.description?req.body.Value.description:shop.description
    shop.facebook = req.body.Value.facebook?req.body.Value.facebook:shop.facebook
    shop.image = req.body.Value.image?req.body.Value.image:shop.image
    await ShopService.Ins.update(shop)
    respronse.Value = shop;
    return res.json(respronse);
}





// export async function getList(req: Request, res: Response){
//     var rs = await UserService.Ins.getList();
//     res.json(rs);
// }

// export async function create(req: Request, res: Response){
//     var respronse : HttpResponseModel<shop> = {
//         status : "Success"
//     }

//     var shop = await UserService.Ins.getByUsername(req.body.username);

//     if(shop){
//         respronse.errMessage = "Username is exists";
//         respronse.status = "Fail"
//         return res.json(respronse)
        
//     }
//     var shop = new shop;
//     shop.username = req.body.username
//     shop.password = req.body.password
//     shop.name = req.body.name
//     shop.email = req.body.email
//     shop.phoneNumber = req.body.phoneNumber
//     shop.address = req.body.address
//     shop.gender = req.body.gender
//     shop.dayOfBirth = req.body.dayOfBirth
//     shop.roleId = 2

//     await UserService.Ins.create(shop);
//     respronse.Value = shop;
//     return res.json(respronse);
// }

// export async function getByUsername(req: Request, res: Response){
//     var respronse : HttpResponseModel<shop> = {
//         status : "Success"
//     }
//     var shop = await UserService.Ins.getByUsername(req.body.username);
//     if(shop){
//         respronse.Value = shop;
//         return res.json(respronse)
//     }
//     respronse.Value = null
//     respronse.status = "Fail"
//     respronse.errMessage = "shop not exists"
//     return res.json(respronse)
// }

// export async function update(req: Request, res: Response){
//     var respronse : HttpResponseModel<shop> = {
//         status : "Success"
//     }

//     var shop = await UserService.Ins.getByUsername(req.body.username);

//     if(!shop){
//         respronse.errMessage = "Username not exists";
//         respronse.status = "Fail"
//         respronse.Value = req.body.Value
//         return res.json(respronse)
//     }

//     shop.password = req.body.Value.password?req.body.Value.password:shop.password
//     shop.name = req.body.Value.name?req.body.Value.name:shop.name
//     shop.email = req.body.Value.email?req.body.Value.email:shop.email
//     shop.phoneNumber = req.body.Value.phoneNumber?req.body.Value.phoneNumber:shop.phoneNumber
//     shop.address = req.body.Value.address?req.body.Value.address:shop.address
//     shop.gender = req.body.Value.gender?req.body.Value.gender:shop.gender
//     shop.dayOfBirth = req.body.Value.dayOfBirth?req.body.Value.dayOfBirth:shop.dayOfBirth

//     await UserService.Ins.update(shop)

//     respronse.Value = shop;
//     return res.json(respronse);
// }


// export async function deleteUser(req: Request, res: Response){
//     var respronse : HttpResponseModel<shop> = {
//         status : "Success"
//     }

//     var shop = await UserService.Ins.getByUsername(req.body.username);

//     if(!shop){
//         respronse.errMessage = "Username not exists, can't delete";
//         respronse.status = "Fail"
//         respronse.Value = req.body.Value
//         return res.json(respronse)
//     }
//     await UserService.Ins.delete(shop)
//     respronse.Value = shop;
//     return res.json(respronse);
// }