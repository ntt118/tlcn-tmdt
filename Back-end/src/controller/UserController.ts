import { Request, Response } from "express";
import { UserService } from '../services/user.service';
import { User } from "../entities/User";
import { HttpResponseModel } from '../models/http-response.model'
import { format } from "url";
import { AccountService } from '../services/account.service';
import { UserModel } from "../models/user.model";
import { RoleService } from "../services/Role.service";
var jwt = require('jsonwebtoken');

var nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'quangduy49jp@gmail.com',
      pass: 'Quangduy5868'
    }
  });

export async function getList(req: Request, res: Response){
    var rs = await UserService.Ins.getList();
    rs.forEach(r => {
        delete r.hash
        delete r.iterations
        delete r.salt
    })
    res.json(rs);
}

export async function create(req: Request, res: Response){
    var response : HttpResponseModel<User> = {
        status : "Success"
    }

    var user = await UserService.Ins.getByUsername(req.body.username);

    if(user){
        response.errMessage = "Username is exists";
        response.status = "Fail"
        return res.json(response)
    }

    AccountService.Ins.generateHashPassword(req.body.password).then(rs => {
        var user = new User;
        user.username = req.body.username
        user.name = req.body.name
        user.email = req.body.email
        user.phoneNumber = req.body.phoneNumber
        user.address = req.body.address || null
        user.gender = req.body.gender || null
        user.dayOfBirth = req.body.dayOfBirth || null
        user.roleId = 2
        user.status = 1
        user.salt = Buffer.from(rs.salt, 'utf8');
        user.hash = Buffer.from(rs.hash, 'utf8');
        user.iterations = rs.iterations;
        user.isActive = 1;
        UserService.Ins.create(user).then(result => {
            delete user.hash;
            delete user.salt;
            delete user.iterations;
            response.Value = user;
            // var mailOptions = {
            //     from: 'sondabac00020@gmail.com',
            //     to: user.email,
            //     subject: 'Sending Email using Node.js',
            //     html: `<h1>Welcome</h1><p>Xác nhận đăng kí <a href="http://localhost:3000/api/user/confirm-register/${user.username}">tại đây</a>!</p>`
            //   };
            // transporter.sendMail(mailOptions, function (error, info) {
            //     if (error) {
            //         console.log(error);
            //     } else {
            //         console.log('Email sent: ' + info.response);
            //     }
            // });
            return res.json(response);
        }, fail => {
                response.status = "Fail1"
            return res.json(response)
        });  
    }, fail => {
        response.status = "Fail2"
        return res.json(response)
    })
}

export async function sendResetEmail(req: Request, res: Response) {
    var response : HttpResponseModel<User> = {
        status : "Success",
        errMessage: null
    }
    const resetCode = Math.floor(100000 + Math.random() * 900000);

    const user = await UserService.Ins.getByEmail(req.body.email);

    if(!user){
        response.errMessage = "Tài khoản không tồn tại";
        response.status = "Fail";
        return res.json(response);
    }

    user.resetCode = resetCode.toString();

    UserService.Ins.update(user).then(rs => {
        var mailOptions = {
            from: 'no-reply@email.com',
            to: user.email,
            subject: 'Khôi phục mật khẩu tài khoản',
            html: `<h1>Khôi phục tài khoản</h1>
            <p>Mã xác nhận của bạn là ${resetCode}.<p>`
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                response.errMessage = "Gửi email không thành công";
                response.status = "Fail";
                return res.json(response);
            } else {
                console.log('Email sent: ' + info.response); 
            }
        });

        return res.json(response)
    }, err => {
        response.errMessage = "Khôi phục mật khẩu thất bại";
        response.status = "Fail"
        return res.json(response)
    })    
}

export async function resetPassword(req: Request, res: Response) {
    var response : HttpResponseModel<User> = {
        status : "Success"
    }

    const user = await UserService.Ins.getByEmail(req.body.email);

    if(!user){
        response.errMessage = "Tài khoản không tồn tại";
        response.status = "Fail";
        return res.json(response);
    }

    if(req.body.resetCode !== user.resetCode && user.resetCode === null) {
        response.errMessage = "Mã khôi phục không chính xác";
        response.status = "Fail";
        return res.json(response);
    }

    AccountService.Ins.generateHashPassword(req.body.newPassword).then(rs => {
        user.salt = Buffer.from(rs.salt, 'utf8');
        user.hash = Buffer.from(rs.hash, 'utf8');
        user.iterations = rs.iterations;
        user.resetCode = null;
        UserService.Ins.update(user).then(r => {
            delete user.hash;
            delete user.salt;
            delete user.iterations;
            response.Value = user;
            return res.json(response);
        }, err => {
            response.errMessage = "Khôi phục mật khẩu thất bại";
            response.status = "Fail"
            return res.json(response)
        })
    })
}

export async function createStaff(req: Request, res: Response){
    var response : HttpResponseModel<User> = {
        status : "Success"
    }

    var user = await UserService.Ins.getByUsername(req.body.username);

    if(user){
        response.errMessage = "Username is exists";
        response.status = "Fail"
        return res.json(response)
        
    }

    AccountService.Ins.generateHashPassword(req.body.password).then(rs => {
        var user = new User;
        user.username = req.body.username
        user.name = req.body.name
        user.email = req.body.email
        user.phoneNumber = req.body.phoneNumber
        user.address = req.body.address
        user.gender = req.body.gender
        user.dayOfBirth = req.body.dayOfBirth
        user.roleId = 2
        user.status = 1
        user.salt = Buffer.from(rs.salt, 'utf8');
        user.hash = Buffer.from(rs.hash, 'utf8');
        user.iterations = rs.iterations;
        user.isActive = 1;
        UserService.Ins.create(user).then(result => {
            delete user.hash;
            delete user.salt;
            delete user.iterations;
            response.Value = user;
            return res.json(response);
        }, fail => {
                response.status = "Fail1"
            return res.json(response)
        });  
    }, fail => {
        response.status = "Fail2"
        return res.json(response)
    })  
}


export async function acceptRegister(req: Request, res: Response){
    var response : HttpResponseModel<User> = {
        status : "Success"
    }
    
    var user = await UserService.Ins.getByUsername(req.params.username);

    if(user){
       user.status = 1;
       await UserService.Ins.update(user);
       res.writeHead(302, {
        'Location': 'http://localhost:4200/login'
        //add other headers here...
      });
      res.end();
      return;
    }
    return;
}

export async function getByUsername(req: Request, res: Response){
    var response : HttpResponseModel<User> = {
        status : "Success"
    }
    var user = await UserService.Ins.getByUsername(req.body.username);
    if(user){
        response.Value = user;
        return res.json(response)
    }
    response.Value = null
    response.status = "Fail"
    response.errMessage = "User not exists"
    return res.json(response)
}

let refreshTokens = []

export async function refreshToken (req: Request, res: Response){
    var response : HttpResponseModel<any> = {
        status : "Success"
    }
    const refreshToken = req.body.token;
    if(!refreshToken) res.sendStatus(401);
    if(!refreshTokens.includes(refreshToken)) res.sendStatus(403);
    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, data) => {
        if(err) res.sendStatus(403)
        const accessToken = jwt.sign({username: data.username}, process.env.ACCESS_TOKEN_SECRET)
        response.accessToken = accessToken
        return res.json(response)
    })
    response.status = "Fail"
    response.accessToken = null
    response.errMessage = "Refresh token invalid"
    return res.json(response)   
}

export async function logout(req: Request, res: Response){
    const refreshToken = req.body.token;
    refreshTokens = refreshTokens.filter(refToken => refToken !== refreshToken)
    res.sendStatus(200);
}

export async function authAccount(req: Request, res: Response){
    var response : HttpResponseModel<any> = {
        status : "Success"
    }

    var user = await UserService.Ins.getByUsername(req.body.username)

    if (user && user.isActive == 1 && user.status == 1)
    {
        await AccountService.Ins.verifyPassword({hash : user.hash.toString(), salt : user.salt.toString(), iterations : user.iterations}, req.body.password)
        .then(async rs => {
            response.Value = rs;
            var role = await RoleService.Ins.getById(user.roleId);
            if (rs) {
                var currentUser : UserModel = {
                    id: user.id,
                    username : user.username,
                    address : user.address,
                    dayOfBirth : user.dayOfBirth,
                    email : user.email,
                    phoneNumber : user.phoneNumber,
                    name : user.name,
                    gender : user.gender,
                    role: role.code
                };
                
                response.Value = currentUser;
                const accessToken = jwt.sign(currentUser, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '7d' })
                const refreshToken = jwt.sign(currentUser, process.env.REFRESH_TOKEN_SECRET)
                response.accessToken = accessToken
                response.refreshToken = refreshToken
                refreshTokens.push(refreshToken)
                return res.json(response)      
            }
            response.Value = null
            response.status = "Fail"
            response.errMessage = "Email or Password not match"
            return res.json(response)      
        })
        // AccountService.Ins.generateHashPassword
    }
    else {
        response.Value = null
        response.status = "Fail"
        response.errMessage = "User not exist"
        return res.json(response)
    }
}

export async function update(req: Request, res: Response){
    var response : HttpResponseModel<User> = {
        status : "Success"
    }

    var user = await UserService.Ins.getByUsername(req.body.username);

    if(!user){
        response.errMessage = "Username not exists";
        response.status = "Fail"
        response.Value = req.body
        return res.json(response)
    }

    await AccountService.Ins.verifyPassword({hash : user.hash.toString(), salt : user.salt.toString(), iterations : user.iterations}, req.body.oldPassword)
    .then(async rs => {
        if (rs) {
            user.name = req.body.name ? req.body.name : user.name
            user.email = req.body.email ? req.body.email : user.email
            user.phoneNumber = req.body.phoneNumber ? req.body.phoneNumber : user.phoneNumber
            user.address = req.body.address ? req.body.address : user.address
            
            if(req.body.password) {
                const rs = await AccountService.Ins.generateHashPassword(req.body.password)
                if(rs) {
                    user.salt = Buffer.from(rs.salt, 'utf8');
                    user.hash = Buffer.from(rs.hash, 'utf8');
                    user.iterations = rs.iterations;
                    user.resetCode = null;
                }
            }

            await UserService.Ins.update(user).then(r => {
                delete user.hash;
                delete user.salt;
                delete user.iterations;
                response.Value = user;
                return res.json(response);
            }, err => {
                response.errMessage = "Khôi phục mật khẩu thất bại";
                response.status = "Fail"
                return res.json(response)
            })
        }

        response.Value = null;
        response.status = "Fail"
        response.errMessage = "Email hoặc mật khẩu không đúng"
        return res.json(response)      
    })
}


export async function deleteUser(req: Request, res: Response){
    var response : HttpResponseModel<User> = {
        status : "Success"
    }

    var user = await UserService.Ins.getByUsername(req.body.username);

    if(!user){
        response.errMessage = "Username not exists, can't delete";
        response.status = "Fail"
        response.Value = req.body.Value
        return res.json(response)
    }
    user.isActive = 0;
    await UserService.Ins.update(user)
    response.Value = user;
    return res.json(response);
}

export function authorization(req: Request, res: Response){
    const data = req.body
    const accessToken = jwt.sign(data, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '30s' })
    res.json({accessToken})
}