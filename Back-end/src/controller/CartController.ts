import { Request, Response } from "express";
import { CartService } from '../services/cart.service';
import { UserService } from '../services/user.service';
import { Cart } from "../entities/Cart";
import { HttpResponseModel } from '../models/http-response.model'
import { format } from "url";
import { GridFSBucketReadStream } from "typeorm";


export async function getList(req: Request, res: Response){
    var rs = await CartService.Ins.getList();
    res.json(rs);
}

export async function create(req: Request, res: Response){
    var response : HttpResponseModel<Cart> = {
        status : "Success"
    }
    var abc = new Cart;

    var cart2 = await CartService.Ins.getByUserId(req.body.userId)
    if(cart2.length>0){
        var cart = cart2.filter(r=>r.productotherdetailId == req.body.productotherdetailId)
        if(cart.length>0){
            cart[0].quantity += req.body.quantity;
            await CartService.Ins.update(cart)
            response.Value = cart[0];
            return res.json(response);
        }
        else{
            abc.userId = req.body.userId;
            abc.productotherdetailId = req.body.productotherdetailId;
            abc.quantity = req.body.quantity;
            await CartService.Ins.create(abc);
        }
    }
    else{
        abc.userId = req.body.userId;
        abc.productotherdetailId = req.body.productotherdetailId;
        abc.quantity = req.body.quantity;
        await CartService.Ins.create(abc);
    }
    response.Value = abc;
    return res.json(response);
}

export async function getById(req: Request, res: Response){
    var response : HttpResponseModel<Cart[]> = {
        status : "Success"
    }
    var cart = await CartService.Ins.getByUserId(req.body.userId);
    if(cart){
        response.Value = cart;
        return res.json(response)
    }
    response.Value = null
    response.status = "Fail"
    response.errMessage = "Cart not exist"
    return res.json(response)
}


export async function update(req: Request, res: Response){
    var response : HttpResponseModel<Cart[]> = {
        status : "Success"
    }

    var cart = await CartService.Ins.getByUserId(req.body.userId);

    if(!cart){
        response.errMessage = "Cart not exist";
        response.status = "Fail"
        response.Value = req.body.Value
        return res.json(response)
    }
    cart = req.body.Value;
    CartService.Ins.update(cart)
    
    response.Value = cart;
    return res.json(response);
}


export async function deleteCart(req: Request, res: Response){
    var response : HttpResponseModel<Cart[]> = {
        status : "Success"
    }

    var cart = await CartService.Ins.getByUserId(req.body.userId);
    if(!cart){
        response.errMessage = "Id cart not exist, can't delete";
        response.status = "Fail"
        response.Value = null
        return res.json(response)
    }
    
    cart.forEach(r => {
        CartService.Ins.delete(r)
    })
    response.Value = cart;
    return res.json(response);
}

export async function removeProductsFromCart(req: Request, res: Response) {
    var response : HttpResponseModel<Cart[]> = {
        status : "Success"
    }
    var cart = await CartService.Ins.getByUserId(req.body.userId);
    if(!cart) {
        response.errMessage = "Cart not exist, can't delete";
        response.status = "Fail";
        response.Value = null;
        return res.json(response);
    }

    var product = cart.filter(c => c.productotherdetailId == req.body.productotherdetailId)[0];
    if(product) {
        CartService.Ins.delete(product)
    }
    return res.json(response);
}