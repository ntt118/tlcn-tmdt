export interface HttpResponseModel<T> {
    Value? : T;
    status: string;
    errMessage? : string;
    accessToken?: string;
    refreshToken?: string;
}

export interface HttpResponseDeleteModel {
    _id : string;
    status: string;
    errMessage? : string;
}