export interface PersistedPassword {
    salt: string ;
    hash: string ;
    iterations: number;
}