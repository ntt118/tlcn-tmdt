import { Category } from "../entities/Category";
import { Productotherdetail } from "../entities/Productotherdetail";

export interface ProductModel{
  id?: number;
name?: string | null;
description?: string | null;
price?: number | null;
quantity?: number | null;
categoryId?: number;
category: Category;
rateAvg?: number | null;
status?: number | null;
imageproducts?: ImageproductModel[];
productotherdetails?: Productotherdetail[]

//   orderdetails?: Orderdetail[];
}

export interface ImageproductModel{
  id?: number;

  image?: Buffer | null | any;

  productId?: number;

}