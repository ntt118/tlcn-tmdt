export interface UserModel{
  id?: number;
  username: string;
  name: string;
  email: string;
  phoneNumber: string | null;
  address: string | null;
  gender: number | null;
  dayOfBirth: Date | null;
  role: string | null
}