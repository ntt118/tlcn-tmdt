export interface ShopModel{
    id?: number
    name?: string
    description?: string
    address?: string
    phoneNumber?: string
    email?: string
    facebook?: string
    image?: any | null;
}
export interface UpdateShop{
    id?: number
    Value?: {
        name?: string
        description?: string
        address?: string
        phoneNumber?: string
        email?: string
        facebook?: string
        image?: any | null;
    }
}