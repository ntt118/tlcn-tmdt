import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
    // badge: {
    //   color: 'info',
    //   text: 'NEW',
    // }
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Quản lý']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Khách hàng',
    to: '/customer/list',
    icon: 'cil-people',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Đơn đặt hàng',
    to: '/order/list',
    icon: 'cil-cart',
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Sản phẩm',
    route: '/product',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Thêm sản phẩm',
        to: '/product/create',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Danh sách sản phẩm',
        to: '/product/list',
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Phân loại',
    route: '/category',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Thêm phân loại',
        to: '/category/create',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Danh sách phân loại',
        to: '/category/list',
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Thương hiệu',
    route: '/brand',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Thêm thương hiệu',
        to: '/brand/create',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Danh sách thương hiệu',
        to: '/brand/list',
      },
    ],
  },
  // {
  //   _tag: 'CSidebarNavDropdown',
  //   name: 'Khuyến mãi',
  //   route: '/promotion',
  //   icon: 'cil-puzzle',
  //   _children: [
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Thêm khuyến mãi',
  //       to: '/promotion/create',
  //     },
  //     {
  //       _tag: 'CSidebarNavItem',
  //       name: 'Danh sách khuyến mãi',
  //       to: '/promotion/list',
  //     },
  //   ],
  // },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Chi tiết khác',
    route: '/otherdetail',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Thêm chi tiết khác',
        to: '/otherdetail/create',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Danh sách chi tiết khác',
        to: '/otherdetail/list',
      },
    ],
  }
]

export default _nav
