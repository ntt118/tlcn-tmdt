import React from 'react';
const Dashboard = React.lazy(() => import('../views/dashboard/Dashboard'));
//Category
const ListCategory = React.lazy(() => import('../page/admin/category/list-category'));
const CreateCategory = React.lazy(() => import('../page/admin/category/create-category'));
//Brand
const ListBrand = React.lazy(() => import('../page/admin/brand/list-brand'));
const CreateBrand = React.lazy(() => import('../page/admin/brand/create-brand'));
//Other Detail
const ListOtherDetail = React.lazy(() => import('../page/admin/otherdetail/list'));
const CreateOtherDetail = React.lazy(() => import('../page/admin/otherdetail/create'));
//Product
const ListProduct = React.lazy(() => import('../page/admin/product/list-product'));
const CreateProduct = React.lazy(() => import('../page/admin/product/create-product'));
//Promotion
const ListPromotion = React.lazy(() => import('../page/admin/promotion/list-promotion'));
const CreatePromotion = React.lazy(() => import('../page/admin/promotion/create-promotion'));
//Customer
const ListCustomer = React.lazy(() => import('../page/admin/customer/list-customer'));
//Order
const ListOrder = React.lazy(() => import('../page/admin/order/list-order'))
const OrderDetail = React.lazy(() => import('../page/admin/order/order-detail'))

const routes = [
    { path: '/', exact: true, name: 'Home' },
    { path: '/dashboard', name: 'Dashboard', component: Dashboard },
    
    { path: '/category', name: 'Category', component: ListCategory, exact: true },
    { path: '/category/list', name: 'List Category', component: ListCategory },
    { path: '/category/create', name: 'Create Category', component: CreateCategory },

    { path: '/brand', name: 'Brand', component: ListBrand, exact: true },
    { path: '/brand/list', name: 'List Brand', component: ListBrand },
    { path: '/brand/create', name: 'Create Brand', component: CreateBrand },

    { path: '/product', name: 'Product', component: ListProduct, exact: true },
    { path: '/product/list', name: 'List Product', component: ListProduct },
    { path: '/product/create', name: 'Create Product', component: CreateProduct },

    { path: '/otherDetail', name: 'Other Detail', component: ListOtherDetail, exact: true },
    { path: '/otherDetail/list', name: 'List Other Detail', component: ListOtherDetail },
    { path: '/otherDetail/create', name: 'Create Other Detail', component: CreateOtherDetail },

    { path: '/promotion', name: 'Promotion', component: ListPromotion, exact: true },
    { path: '/promotion/list', name: 'List Promotion', component: ListPromotion },
    { path: '/promotion/create', name: 'Create Promotion', component: CreatePromotion },

    { path: '/customer', name: 'Customer', component: ListCustomer, exact: true },
    { path: '/customer/list', name: 'List Customer', component: ListCustomer },

    { path: '/order', name: 'Order', component: ListOrder, exact: true },
    { path: '/order/list', name: 'List Order', component: ListOrder },
    { path: '/order/detail', name: 'Order Detail', component: OrderDetail },
]
export default routes;