import React from 'react'
import CIcon from '@coreui/icons-react'

// import Apis
import CategoryApi from "src/api/category"
import BrandApi from 'src/api/brand'

const _nav_Client =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Home',
    to: '/home',
    icon: <CIcon name="cil-home" customClasses="c-sidebar-nav-icon"/>,
    // badge: {
    //   color: 'info',
    //   text: 'NEW',
    // }
  },
  {
    _tag: 'CSidebarNavDropdown',
    id: 'categories',
    name: 'Phân loại',
    route: '/product',
    icon: 'cil-apps',
    _children: [
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Loại 1',
      //   to: '/home',
      // },
      // {
      //   _tag: 'CSidebarNavItem',
      //   name: 'Loại 2',
      //   to: '/home',
      // },
    ],
  },
  {    
    _tag: 'CSidebarNavDropdown',
    id: 'brands',
    name: 'Thương hiệu',
    route: '/brands',
    icon: 'cil-tags',
    _children: [

    ]
  }
]


const updateCategoriesNav = () => {
  CategoryApi.getAll().then(rs => {
    _nav_Client.map(r => {
      if (r.id === 'categories') {
        rs.forEach(c => {
          r._children.push({
            _tag: 'CSidebarNavItem',
            name: c.name,
            to: `/category/${c.id}`
          })
        })
      }
      return r
    })
  })
}

const updateBrandsNav = () => {
  BrandApi.getAll().then(rs => {
    _nav_Client.map(r => { 
      if (r.id === 'brands') {
        rs.forEach(c => {
          r._children.push({
            _tag: 'CSidebarNavItem',
            name: c.name,
            to: `/brand/${c.id}`
          })
        })
      }
      return r
    })
  })
}

updateCategoriesNav()
updateBrandsNav()

export default _nav_Client
