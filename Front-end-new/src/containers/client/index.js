import TheFooter from "./TheFooter";
// import TheSidebar from "./TheSidebar";
import TheHeader from "./TheHeader";
import TheContent from "./TheContent";

export {
    TheHeader,
    TheFooter,
    // TheSidebar,
    TheContent
}