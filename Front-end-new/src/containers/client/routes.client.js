import React from 'react';
import Order from 'src/page/client/order/order';
import OrderList from 'src/page/client/order/order-list';
const Cart = React.lazy(() => import('src/page/client/cart/cart'));
const ProductsList = React.lazy(() => import('src/page/client/products-list/products-list'));
const ProductDetail = React.lazy(() => import('src/page/client/product-detail/product-detail'));
const routes = [
    { path: '/', exact: true, name: 'Trang chủ' },
    { path: '/home', exact: true, name: 'Danh sách sản phẩm', component: ProductsList },
    { path: '/category/:id', exact: true, name: 'Sản phẩm theo phân loại', component: ProductsList },
    { path: '/brand/:id', exact: true, name: 'Sản phẩm theo thương hiệu', component: ProductsList },
    { path: '/product/:id', name: 'Chi tiết', component: ProductDetail },
    { path: '/cart', name: 'Giỏ hàng', component: Cart},
    { path: '/order', name: 'Đơn hàng', exact: true, component: OrderList},
    { path: '/order/checkout', exact: true, name: 'Đặt hàng', component: Order}

]
export default routes;