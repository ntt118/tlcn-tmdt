import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CHeader,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CSubheader,
  CBreadcrumbRouter,
  CLink,
  CInputGroup,
  CDropdown,
  CDropdownToggle,
  CImg,
  CDropdownMenu,
  CDropdownItem,
  CBadge
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useHistory } from "react-router"
import { getLocalUserData, logout } from 'src/stores/actions/user.action'
import { getCart, resetCart } from 'src/stores/actions/cart.action'

// routes config
import routes from './routes.client'

import CartApi from 'src/api/cart'
import { getListCategory } from 'src/stores/actions/category.action'
import { getListBrand } from 'src/stores/actions/brand.action'
import { formatPrice } from 'src/page/utils'

const TheHeader = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const user = useSelector(state => state.user.user)
  const cart = useSelector(state => state.cart.cart)
  const [searchText, setSearchText] = useState('')
  const productList = useSelector(state => state.product.products)
  const [listFiltered, setListFiltered] = useState(null)
  const [totalProducts, setTotalProducts] = useState(0)
  const productsList = useSelector(state => state.product.products)
  const [showCate, setShowCate] = useState(false)
  const categories = useSelector(state => state.category.categories)
  const brands = useSelector(state => state.brand.brands)

  const Logout = () => {
    localStorage.removeItem('accessToken')
    localStorage.removeItem('role')
    dispatch(logout())
    history.push('/home')
  }

  useEffect(() => {
    if(searchText.length > 0){
      setListFiltered(productList.filter(r => r.name.toLowerCase().includes(searchText.toLowerCase())))
    }
  }, [searchText])
  
  useEffect(() => {
    if (user) {
      dispatch(getCart(user.id))
    }
    else {
      dispatch(resetCart())
    }
  }, [user])

  useEffect(() => {
    setTotalProducts(cart?.length)
  }, [cart])

  useEffect(() => {
    if(!categories.length)
      dispatch(getListCategory())
    if(!brands.length)
      dispatch(getListBrand())
    if(!user){
      dispatch(getLocalUserData())
    }
  }, [])

  const DeleteFromCart = (id) => {
    if (user) {
      CartApi.removeProductFromCart({userId: user.id, productotherdetailId: id}).then(r => {
        dispatch(getCart(user.id))
      })
    }
  }

  const UserDropdown = () => {
    return <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar" style={{border: 'solid 1px'}}>
          <CIcon name="cil-user" alt="SignIn"/>
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>Tài khoản</strong>
        </CDropdownItem>
        <CDropdownItem onClick={() => {history.push('/profile')}}>
          <CIcon name="cil-user" className="mfe-2" />
          {user.name}
        </CDropdownItem>
        <CDropdownItem>
          @ {user.email}
        </CDropdownItem>
        <CDropdownItem divider />
        <CDropdownItem onClick={Logout}>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          Đăng xuất
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  }

  const CartDropdown = () => {
    return <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar" style={{width:'fit-content'}}>
          <CIcon name="cil-cart" alt="Cart" />
          <CBadge shape="pill" color="danger">{totalProducts}</CBadge>
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>Giỏ hàng</strong>
          { cart?.length > 0 && cart.map((product, index) => {
            return <CDropdownItem style={{paddingLeft: '0px'}}>
              <div style={{display:'flex', justifyContent:'center'}}>
                <img  style={{maxHeight:'40px', maxWidth:'40px'}} src={productsList.filter(p => p.id === product.productotherdetail.productId)[0]?.imageproducts[0]?.image} alt="slide 1"/>
                <div className="pl-2">
                  <CLink to={`/product/${product.productotherdetail.productId}`} style={{fontSize: '14px', fontWeight: 'bold'}}>{productsList.filter(p => p.id === product.productotherdetail.productId)[0]?.name}</CLink>
                  <div style={{textAlign:'left'}}><strong>SL:</strong> {product.quantity}</div>
                </div>         
                <div style={{position:'absolute', right:'10px'}} onClick={() => DeleteFromCart(product.productotherdetailId)}>
                  <CIcon name="cil-x"/>  
                </div>    
              </div>            
            </CDropdownItem>
          })
          }
          { cart?.length > 0 && <strong><CLink to='/cart'> Xem  giỏ hàng </CLink></strong>}
        </CDropdownItem>
        { cart?.length === 0 && <CDropdownItem><strong> Không có sản phẩm trong giỏ hàng </strong></CDropdownItem>}
      </CDropdownMenu>
    </CDropdown>
  }

  const categorySelect = (id, type) => {
    const url = type === 'category' ? '/category/' : '/brand/'
    setShowCate(!showCate)
    history.push(url+id)
  }

  const CategoriesMenu = () => {
    return <div className="categories-container" >
      <div className="categories-menu dropdown-menu show">
      <h2 style={{color:'#321fdb', cursor: 'pointer', textAlign: 'center'}} onClick={() => history.push('/home')}>Tất cả sản phẩm</h2>
      <div style={{display: 'flex', width: '100%', justifyContent:'space-evenly'}}>
        <div>
          <h2 style={{color:'#321fdb'}}>Phân loại</h2>
          { categories?.length && categories.map((category, index) => {
            return <div key={index} className="category-item" onClick={() => categorySelect(category.id, 'category')}>
              {category.name}
            </div>
          }) 
        }
        </div>
        <div>
        <h2 style={{color:'#321fdb'}}>Thương hiệu</h2>
        { brands?.length && brands.map((brand, index) => {
            return <div key={index} className="category-item" onClick={() => categorySelect(brand.id, 'brand')}>
              {brand.name}
            </div>
          }) 
        }
        </div>
      </div>      
    </div>
    </div> 
    
  }

  return (
    <CHeader withSubheader onClick={(e) => {
      if(e.target.id === 'category') {
        setShowCate(!showCate)
      }
      else (
        setShowCate(false)
      )
    }}>
      { showCate && 
        <CategoriesMenu />
      }
      <CHeaderNav className="mr-auto w-75 p-3">
        <CHeaderNavItem className="px-3" >
          <CHeaderNavLink to="/home">Trang chủ</CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink id="category">Danh mục
      </CHeaderNavLink>
          
        </CHeaderNavItem>
        <CHeaderNavItem className="d-sm-down-none px-3 col-sm-6">
          <CInputGroup>
            <CDropdown inNav>
              <CDropdownMenu className="header-search-list" show={searchText.length>0} style={{minWidth:'350px'}}>
                {listFiltered ? listFiltered.length>0 ? listFiltered.map((product, index) => {
                  return <CDropdownItem key={index} className="header-search-item" href={`#/product/${product.id}`}>
                      <div style={{padding: '5px 10px'}}>
                        <img style={{height:'50px', width:'50px'}} src={product.imageproducts[0]?.image} alt={product.name} /> 
                      </div>
                      <div>
                        <h5>{product.name}</h5>
                        <span>{formatPrice(product.productotherdetails[0].price) }&#8363;</span>
                      </div>
                  </CDropdownItem>
                  }) : <CDropdownItem><CIcon name="cil-x-circle" size="xl"/>&nbsp;&nbsp;No matched product</CDropdownItem> : <></>                  
                }
              </CDropdownMenu>
            </CDropdown>
            <div className="w-75">
              <input id="input-search" onChange={(e) => setSearchText(e.target.value)} name="input-search" placeholder="Tìm theo tên sản phẩm" />
              <CIcon name="cil-magnifying-glass" />
            </div>
          </CInputGroup>
        </CHeaderNavItem>        
      </CHeaderNav>

      <CHeaderNav className="px-3">
          { !user && <CLink className="c-subheader-nav-link mr-2" href="#/login"> 
            <CIcon name="cil-user" alt="SignIn"/>&nbsp;Đăng nhập
          </CLink> }
          <CartDropdown />
          { user && <div style={{display:'flex', alignItems:'center'}}>
              <CLink  href="#/order" className="mx-2"><CIcon name="cil-clipboard" alt="Cart" />Đơn hàng</CLink>
              <UserDropdown/>
            </div>  
          }
      </CHeaderNav>

      <CSubheader className="px-3 justify-content-between" style={{background:'#ebedef'}}>
        <CBreadcrumbRouter 
          className="border-0 c-subheader-nav m-0 px-0 px-md-3" 
          routes={routes} 
        />
      </CSubheader>
    </CHeader>
  )
}

export default TheHeader
