import HttpAuthClient from "./util";

const OtherDetailApi = {
    getAll: (data) => {
        const url = `/otherdetail/get-list`
        return HttpAuthClient.post(url, data);
    },
    create: (data) => {
        const url = `/otherdetail/create`
        return HttpAuthClient.post(url, data)
    },
    update: (data) => {
        const url = `/otherdetail/update`
        return HttpAuthClient.post(url, data)
    },
    delete: (data) => {
        const url = `/otherdetail/delete`
        return HttpAuthClient.post(url, data)
    },
    getListType: (data) => {
        const url = `/otherdetail/list-type`
        return HttpAuthClient.post(url)
    }
}

export default OtherDetailApi