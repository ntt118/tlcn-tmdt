import HttpAuthClient from "./util";

const CartApi = {
    getAll: () => {
        const url = `/cart/get-list`
        return HttpAuthClient.post(url);
    },
    getByUserId: (id) => {
        const url = '/cart/get-id'
        return HttpAuthClient.post(url, {userId: id})
    },
    create: (data) => {
        const url = `/cart/create`
        return HttpAuthClient.post(url, data)
    },
    update: (data) => {
        const url = `/cart/update`
        return HttpAuthClient.post(url, data)
    },
    delete: (data) => {
        const url = `/cart/delete`
        return HttpAuthClient.post(url, data)
    },
    removeProductFromCart: (data) => {
        const url = '/cart/delete/product'
        return HttpAuthClient.post(url, data)
    }
}

export default CartApi