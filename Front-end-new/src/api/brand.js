import HttpAuthClient from "./util";

const BrandApi = {
    getAll: () => {
        const url = `/brand/get-list`
        return HttpAuthClient.post(url);
    },
    create: (data) => {
        const url = `/brand/create`
        return HttpAuthClient.post(url, data)
    },
    update: (data) => {
        const url = `/brand/update`
        return HttpAuthClient.post(url, data)
    },
    delete: (data) => {
        const url = `/brand/delete`
        return HttpAuthClient.post(url, data)
    }
}

export default BrandApi