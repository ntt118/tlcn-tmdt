import axiosClient from "./axiosClient"
import HttpAuthClient from "./util";

const UserApi = {
    getAll: () => {
        const url = `/user/get-list`
        return HttpAuthClient.post(url);
    },
    login: (data) => {
        const url = `/user/auth`
        return axiosClient.post(url, data)
    },
    signUp: (data) => {
        const url = `/user/create`
        return axiosClient.post(url, data)
    },
    update: (data) => {
        const url = '/user/update'
        return axiosClient.post(url, data)
    },
    delete: (data) => {
        const url = `/user/delete`
        return HttpAuthClient.post(url, data)
    },
    resetPassRequest: (data) => {
        const url = `/user/reset-request`
        return HttpAuthClient.post(url, data)
    },
    resetPassword: (data) => {
        const url = `/user/reset-pass`
        return HttpAuthClient.post(url, data)
    }
}

export default UserApi