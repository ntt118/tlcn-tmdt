import axiosClient from "./axiosClient";
import HttpAuthClient from "./util";

const CategoryApi = {
    getAll: () => {
        const url = `/category/get-list`
        return axiosClient.post(url);
    },
    create: (data) => {
        const url = `/category/create`
        return HttpAuthClient.post(url, data)
    },
    update: (data) => {
        const url = `/category/update`
        return HttpAuthClient.post(url, data)
    },
    delete: (data) => {
        const url = `/category/delete`
        return HttpAuthClient.post(url, data)
    }
}

export default CategoryApi