import HttpAuthClient from "./util";

const ProductApi = {
    getAll: () => {
        const url = `/product/get-list`
        return HttpAuthClient.post(url);
    },
    getById: (data) => {
        const url = `/product/get-id`
        return HttpAuthClient.post(url, data)
    },
    create: (data) => {
        const url = `/product/create`
        return HttpAuthClient.post(url, data)
    },
    update: (data) => {
        const url = `/product/update`
        return HttpAuthClient.post(url, data)
    },
    delete: (data) => {
        const url = `/product/delete`
        return HttpAuthClient.post(url, data)
    }
}

export default ProductApi