import HttpAuthClient from "./util";

const PromotionApi = {
    getAll: () => {
        const url = `/promotion/get-list`
        return HttpAuthClient.post(url);
    },
    create: (data) => {
        const url = `/promotion/create`
        return HttpAuthClient.post(url, data)
    },
    update: (data) => {
        const url = `/promotion/update`
        return HttpAuthClient.post(url, data)
    },
    delete: (data) => {
        const url = `/promotion/delete`
        return HttpAuthClient.post(url, data)
    }
}

export default PromotionApi