import axiosClient from "./axiosClient";

const accessToken = JSON.parse(localStorage.getItem('accessToken'))
const config = {
    headers: {
        "Authorization" : `Bearer ${accessToken}`,
        'Access-Control-Allow-Origin': '*',
        "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS",
        "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
    }
}

export default class HttpAuthClient {
    static get(url) {
        axiosClient.get(url , { headers: {"Authorization" : `Bearer ${accessToken}`} })
        .then(res => {
            console.log(res);
        })
        return axiosClient.get(url, config);
      }
    static post(url, data) {
        return axiosClient.post(url, data, config);
    }
}