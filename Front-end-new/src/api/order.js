import HttpAuthClient from "./util";

const OrderApi = {
    getAll: () => {
        const url = `/order/get-list`
        return HttpAuthClient.post(url);
    },
    create: (data) => {
        const url = '/order/create'
        return HttpAuthClient.post(url, data)
    },
    update: (data) => {
        const url = '/order/update'
        return HttpAuthClient.post(url, data)
    },
    getById: (data) => {
        const url = `/order/get-id`
        return HttpAuthClient.post(url, data)
    },
    getByUserId: (data) => {
        const url = `/order/user/get-id`
        return HttpAuthClient.post(url, data)
    },
    cancelOrder: (data) => {
        const url = '/order/cancel'
        return HttpAuthClient.post(url, data)
    },
    dashboard: (data) => {
        const url = '/order/dashboard'
        return HttpAuthClient.post(url, data)
    },
    momo: (data) => {
        const url = '/order/paymentQRCode'
        return HttpAuthClient.post(url, data)
    }
}

export default OrderApi