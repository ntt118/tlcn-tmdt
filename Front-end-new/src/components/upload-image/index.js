import CIcon from "@coreui/icons-react"
import { useRef, useState, useEffect, forwardRef, useImperativeHandle } from "react"
import { icons } from "src/assets/icons"
import "./style.scss"
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/react";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: green;
`;

const UploadImage = forwardRef((props, ref) => {
    useImperativeHandle(ref, () => ({
        getData(){
            return imageList
        }
    }))

    const [imageList, setImageList] = useState([])
    const [imageShow, setImageShow] = useState("")
    const [imageIndex, setImageIndex] = useState(0)
    const [loading, setLoading] = useState(false)
    const fileInput = useRef()

    useEffect(() => {
        if(imageList && imageList.length > 0)
            setImageShow(imageList[imageIndex]) 
    }, [imageList, imageIndex])

    useEffect(() => {
        if(props.imageList && props.imageList.length > 0){
            setImageList(props.imageList)
            setImageIndex(0)
            setImageShow(props.imageList[0])
        }
        else {
            setImageList([])
            setImageIndex(0)
            setImageShow("")
        }
    }, [props.imageList])

    const nextImage = () => {
        if (imageIndex === imageList.length - 1) {
            setImageIndex(0)
            return
        }
        setImageIndex(imageIndex+1)
    }
    const preImage = () => {
        if (imageIndex === 0) {
            setImageIndex(imageList.length - 1)
            return
        }
        setImageIndex(imageIndex-1)
    }
    const deleteImage = () => {
        setImageList(imageList.filter(r => r !== imageList[imageIndex]))
        if(imageIndex > 0){
            setImageIndex(imageIndex-1)
        }
    }
    const uploadSubmit = () => {
        fileInput.current.click()
    }
    const uploadImage = async (e) => {
        var checkType = false
        var checkSize = false
        Array.prototype.forEach.call(e.target.files, r => {
            if (!r.type.includes("image/")) {
                checkType = true
            }
            if(r.size > 5 * 1024 * 1024){
              checkSize = true
            }
        })
        if (checkType) {
            // notification.error("Xin vui lòng chỉ chọn hình ảnh")
            return
        }
        if(checkSize){
        //   notification.error("Xin vui lòng chọn file có kích thước nhỏ hơn 5MB")
          return
        }
        setLoading(true)
        const filePathsPromises = [];
        Array.prototype.forEach.call(e.target.files, file => {
            filePathsPromises.push(getBase64(file));
        });
        const filePaths = await Promise.all(filePathsPromises);
        setImageList(filePaths)
        setLoading(false)
    }

    const getBase64 = file => {
        return new Promise(resolve => {
          let baseURL = "";
          // Make new FileReader
          let reader = new FileReader();
    
          // Convert the file to base64 text
          reader.readAsDataURL(file);
    
          // on reader load somthing...
          reader.onload = () => {
            // Make a fileInfo Object
            baseURL = reader.result;
            resolve(baseURL);
          };
        });
    };

    return (
        <div className="upload-image">
            <input ref={fileInput} type="file" hidden accept="image/*" multiple={true} class="btn btn-primary btn-add-product" onChange={(e) => uploadImage(e)} />
            <div class="btn btn-primary btn-add-product" onClick={uploadSubmit}>Thêm ảnh</div>
            <div class="image-product">
                {imageList.length > 0 && <img src={imageShow} alt="" class="img-thumbnail img" />}
                {imageList.length > 0 && <>
                    {imageList.length > 1 && <CIcon content={icons.cilCaretLeft} className="arrow-left" onClick={() => preImage()} />}
                    {imageList.length > 1 && <CIcon content={icons.cilCaretRight} className="arrow-right" onClick={() => nextImage()} />}
                    <CIcon content={icons.cilX} className="btn-delete" onClick={() => deleteImage()} />
                </>}
                {imageList.length === 0 && <div class="no-image">
                    {!loading && <span>Chọn ảnh sản phẩm</span>}
                    <ClipLoader loading={loading} css={override} size={100} />
                </div>}
            </div>
        </div>
    )
})
export default UploadImage