import { CToast, CToastBody, CToaster, CToastHeader } from "@coreui/react";
import { forwardRef, useImperativeHandle, useState } from "react";

const Toaster  = forwardRef((props, ref) => {
    useImperativeHandle(ref, () => ({
        showToaster(){
            addToast()
        }
    }))

    const [toasts, setToasts] = useState([])

    const addToast = (position = "top-right", autohide = true, autohideValue = 3000, closeButton = true, fade = true) => {
        setToasts([
          ...toasts, 
          { position, autohide: autohide && autohideValue, closeButton, fade }
        ])
      }
    
    
      const toasters = (()=>{
        return toasts.reduce((toasters, toast) => {
          toasters[toast.position] = toasters[toast.position] || []
          toasters[toast.position].push(toast)
          return toasters
        }, {})
      })()

    return (
        // <CToaster
        //     position={'top-right'}
        //     key={'toaster top-right'}>
        //     <CToast
        //         key={'toast toaster top-right'}
        //         show={isShowToast}
        //         autohide={3000}
        //         fade={true}>
        //         <CToastHeader closeButton={true}>
        //             {title}
        //         </CToastHeader>
        //         <CToastBody>
        //             {content}
        //         </CToastBody>
        //     </CToast>
        // </CToaster>
        <div>
            {Object.keys(toasters).map((toasterKey) => (
                <CToaster
                  position={toasterKey}
                  key={'toaster' + toasterKey}
                >
                  {
                    toasters[toasterKey].map((toast, key)=>{
                    return(
                      <CToast
                        key={'toast' + key}
                        show={true}
                        autohide={toast.autohide}
                        fade={toast.fade}
                      >
                        <CToastHeader closeButton={toast.closeButton}>
                          {props.title}
                        </CToastHeader>
                        <CToastBody>
                          {props.content}
                        </CToastBody>
                      </CToast>
                    )
                  })
                  }
                </CToaster>
              ))}
        </div>
    )
})
export default Toaster;