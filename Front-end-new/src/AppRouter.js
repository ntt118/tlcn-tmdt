import { useState } from "react"
import { BrowserRouter as Route, Switch } from "react-router"
import { TheLayout } from "./containers"
import Login from "./page/client/login/login"
import SignUp from "./page/client/signUp/signUp"
import ForgetPassword from "./page/client/signUp/forgetPassword"
import Page404 from "./views/pages/page404/Page404"
import Page500 from "./views/pages/page500/Page500"

export default function AppRouter() {

    const [isLogin, setIsLogin] = useState(false)
    const [isAdmin, setIsAdmin] = useState(false)

    return (
        <Route>
            <Switch>            
              <Route exact path="/login" name="Trang đăng nhập" component={Login} />
              <Route exact path="/signUp" name="Trang đăng ký" component={SignUp} />
              <Route exact path="/forget-password" name="Trang quên mật khẩu" component={ForgetPassword} />
              <Route exact path="/404" name="Page 404" component={Page404} />
              <Route exact path="/500" name="Page 500" component={Page500} />
              <Route path="/" name="Admin Page" render={props => 
                    {return <TheLayout {...props}/> }
              } />
            </Switch>
        </Route>
    )
}