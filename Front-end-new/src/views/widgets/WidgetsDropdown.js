import React from 'react'
import {
  CWidgetDropdown,
  CRow,
  CCol} from '@coreui/react'
import ChartLineSimple from '../charts/ChartLineSimple'
import { useSelector } from 'react-redux';

const WidgetsDropdown = () => {

  const dashboard = useSelector(state => state.order.dashboard);
  // render
  return (
    <CRow style={{ justifyContent: 'center' }}>
      <CCol sm="6" lg="3">
        <CWidgetDropdown
          color="gradient-primary"
          header={dashboard?.totalUser ?? 0}
          text="Người dùng"
          footerSlot={
            <ChartLineSimple
              pointed
              className="c-chart-wrapper mt-3 mx-3"
              style={{height: '70px'}}
              dataPoints={[65, 59, 84, 84, 51, 55, 100]}
              pointHoverBackgroundColor="primary"
              label="Members"
              labels="months"
              options={{
                tooltips: {
                  enabled: false
                }
              }}
            />
          }
        >
        </CWidgetDropdown>
      </CCol>

      <CCol sm="6" lg="3">
        <CWidgetDropdown
          color="gradient-info"
          header={dashboard?.totalOrder ?? 0}
          text="Đơn hàng"
          footerSlot={
            <ChartLineSimple
              pointed
              className="mt-3 mx-3"
              style={{height: '70px'}}
              dataPoints={[1, 18, 9, 17, 34, 22, 11]}
              pointHoverBackgroundColor="info"
              options={{ elements: { line: { tension: 0.00001 }}}}
              label="Đơn hàng"
              labels="tháng"
              options={{
                tooltips: {
                  enabled: false
                }
              }}
            />
          }
        >
        </CWidgetDropdown>
      </CCol>
    </CRow>
  )
}

export default WidgetsDropdown
