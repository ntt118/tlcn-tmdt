import React, { lazy, useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCardGroup,
  CSelect
} from '@coreui/react'

import { CChartBar, CChartDoughnut } from '@coreui/react-chartjs'
import { useDispatch, useSelector } from 'react-redux'
import { getDashboard } from 'src/stores/actions/order.action.js'

const WidgetsDropdown = lazy(() => import('../widgets/WidgetsDropdown.js'))

const Dashboard = () => {

  const dispatch = useDispatch();
  const dashboard = useSelector(state => state.order.dashboard);
  const yearList = [2022, 2021, 2020, 2019, 2018]
  const [yearSelect, setYearSelect] = useState(2022);

  useEffect(() => {
    dispatch(getDashboard({ year: yearSelect }))
  }, [yearSelect])

  const onChangeField = (e) => {
    setYearSelect(e.target.value)
  }

  return (
    <>
      <WidgetsDropdown />
      <CCard>
        <CCardBody>
          <CCardGroup columns className="cols-2" >
            <CCard>
              <CCardHeader style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                Thống kê doanh thu
                <CSelect custom value={yearSelect} onChange={(e) => onChangeField(e)} name="year" id="year" style={{ width: '100px' }}>
                    {yearList && yearList.map((rs, index) => {
                        return <option key={rs} value={rs}>{rs}</option>
                    })}
                </CSelect>
              </CCardHeader>
              <CCardBody>
                <CChartBar
                  datasets={[
                    {
                      label: 'VND',
                      backgroundColor: '#f87979',
                      data: dashboard?.chart1 ?? []
                    }
                  ]}
                  labels="months"
                  options={{
                    tooltips: {
                      enabled: true
                    }
                  }}
                />
              </CCardBody>
            </CCard>

            <CCard>
              <CCardHeader>
                Thống kê đơn hàng
              </CCardHeader>
              <CCardBody>
                <CChartDoughnut
                  datasets={[
                    {
                      backgroundColor: [
                        '#41B883',
                        '#E46651',
                        '#00D8FF',
                        '#7bf600',
                        '#ffa54c'
                      ],
                      data: dashboard?.chart2 ?? [0,0,0,0,0]
                    }
                  ]}
                  labels={['Đang chờ', 'Đã xác nhận', 'Đang giao', 'Đã nhận', 'Đã hủy', ]}
                  options={{
                    tooltips: {
                      enabled: true
                    }
                  }}
                />
              </CCardBody>
            </CCard>
          </CCardGroup>
        </CCardBody>
        </CCard>
    </>
  )
}

export default Dashboard
