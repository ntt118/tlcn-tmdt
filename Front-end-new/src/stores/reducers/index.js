import { combineReducers } from "redux"
import brandReducer from "./brand.reducer"
import categoryReducer from "./category.reducer"
import productReducer from "./product.reducer"
import promotionReducer from "./promotion.reducer"
import sidebarReducer from "./sidebar.reducer"
import userReducer from "./user.reducer"
import cartReducer from "./cart.reducer"
import otherDetailReducer from "./otherdetail.reducer"
import orderReducer from "./order.reducer"

const rootReducer = combineReducers({
    user: userReducer,
    sidebar: sidebarReducer,
    category: categoryReducer,
    brand: brandReducer,
    product: productReducer,
    promotion: promotionReducer,
    cart: cartReducer,
    otherDetail: otherDetailReducer,
    order: orderReducer,
})

export default rootReducer