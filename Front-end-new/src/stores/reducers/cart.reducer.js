import { GET_CART, GET_CART_SUCCESS, RESET_CART } from "../actions/cart.action";

const initialState = {
    cart: [],
    isLoading: false,
    failMessage: null
}

const cartReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_CART: {
            return { ...state, isLoading: false }
        }
        case GET_CART_SUCCESS: {
            return { ...state, isLoading: true, cart: action.payload }
        }
        case RESET_CART: {
            return { ...state, isLoading: false, cart: [] }
        }

        default:
            return state
    }
}

export default cartReducer