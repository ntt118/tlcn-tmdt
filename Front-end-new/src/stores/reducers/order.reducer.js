import { GET_LIST_ORDER, GET_LIST_ORDER_SUCCESS, GET_LIST_ORDER_BY_USER_ID, GET_LIST_ORDER_BY_USER_ID_SUCCESS, GET_ORDER_BY_ID, GET_ORDER_BY_ID_SUCCESS, GET_DASH_BOARD_ID, GET_DASH_BOARD, GET_DASH_BOARD_SUCCESS } from "../actions/order.action"

const initialState = {
    orders: [],
    ordersAll: [],
    order: null,
    isLoading: false,
    dashboard: null
}

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_LIST_ORDER: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_ORDER_SUCCESS: {
            return { ...state, isLoading: true, ordersAll: action.payload }
        }
        case GET_LIST_ORDER_BY_USER_ID: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_ORDER_BY_USER_ID_SUCCESS: {
            return { ...state, isLoading: true, orders: action.payload.Value }
        }
        case GET_ORDER_BY_ID: {
            return { ...state, isLoading: false }
        }
        case GET_ORDER_BY_ID_SUCCESS: {
            return { ...state, isLoading: true, order: action.payload.Value }
        }
        case GET_DASH_BOARD: {
            return { ...state, isLoading: false }
        }
        case GET_DASH_BOARD_SUCCESS: {
            return { ...state, dashboard: action.payload }
        }

        default:
            return state
    }
}
export default orderReducer