import { GET_LIST_PRODUCT, GET_LIST_PRODUCT_SUCCESS } from "../actions/product.action"

const initialState = {
    products: [],
    isLoading: false
}

const productReducer = (state = initialState, action) => {
    switch (action.type){
        case GET_LIST_PRODUCT: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_PRODUCT_SUCCESS: {
            return { ...state, isLoading: true, products: action.payload }
        }

        default:
            return state
    }
}
export default productReducer