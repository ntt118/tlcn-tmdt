import { GET_LIST_BRAND, GET_LIST_BRAND_SUCCESS } from "../actions/brand.action"

const initialState = {
    brands: [],
    isLoading: false
}

const brandReducer = (state = initialState, action) => {
    switch (action.type){
        case GET_LIST_BRAND: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_BRAND_SUCCESS: {
            return { ...state, isLoading: true, brands: action.payload }
        }

        default:
            return state
    }
}
export default brandReducer