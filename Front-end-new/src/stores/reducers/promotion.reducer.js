import { GET_LIST_PROMOTION, GET_LIST_PROMOTION_SUCCESS } from "../actions/promotion.action"

const initialState = {
    promotions: [],
    isLoading: false
}

const promotionReducer = (state = initialState, action) => {
    switch (action.type){
        case GET_LIST_PROMOTION: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_PROMOTION_SUCCESS: {
            return { ...state, isLoading: true, promotions: action.payload }
        }

        default:
            return state
    }
}
export default promotionReducer