import { GET_LIST_CATEGORY, GET_LIST_CATEGORY_SUCCESS } from "../actions/category.action"

const initialState = {
    categories: [],
    isLoading: false
}

const categoryReducer = (state = initialState, action) => {
    switch (action.type){
        case GET_LIST_CATEGORY: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_CATEGORY_SUCCESS: {
            return { ...state, isLoading: true, categories: action.payload }
        }

        default:
            return state
    }
}
export default categoryReducer