import { GET_LIST_COLOR, GET_LIST_COLOR_SUCCESS, GET_LIST_OTHER_DETAIL, GET_LIST_OTHER_DETAIL_SUCCESS, GET_LIST_SIZE, GET_LIST_SIZE_SUCCESS, GET_LIST_TYPE, GET_LIST_TYPE_SUCCESS } from "../actions/otherdetail.action"

const initialState = {
    otherDetails: [],
    listType: [],
    isLoading: false,
    sizes: [],
    colors: [],
}

const otherDetailReducer = (state = initialState, action) => {
    switch (action.type){
        case GET_LIST_OTHER_DETAIL: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_OTHER_DETAIL_SUCCESS: {
            return { ...state, isLoading: true, otherDetails: action.payload }
        }
        case GET_LIST_TYPE: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_TYPE_SUCCESS: {
            return { ...state, isLoading: true, listType: action.payload }
        }
        case GET_LIST_SIZE: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_SIZE_SUCCESS: {
            return { ...state, isLoading: true, sizes: action.payload }
        }
        case GET_LIST_COLOR: {
            return { ...state, isLoading: false }
        }
        case GET_LIST_COLOR_SUCCESS: {
            return { ...state, isLoading: true, colors: action.payload }
        }

        default:
            return state
    }
}
export default otherDetailReducer