import {SET_SIDE_BAR_SHOW} from "../actions/sidebar.action"
const initialState = {
    sidebarShow: 'responsive'
}

const sidebarReducer = (state = initialState, action) => {
    switch (action.type){
        case SET_SIDE_BAR_SHOW: {
            return { ...state, sidebarShow: action.payload }
        }

        default:
            return state
    }
}
export default sidebarReducer