import { GET_LIST_USER, GET_LIST_USER_SUCCESS, LOGIN, LOGIN_SUCCESS, LOGOUT, LOGIN_ERROR, GET_LOCAL_USER_DATA } from "../actions/user.action";
var jwt = require('jsonwebtoken');

const initialState = {
    users: [],
    user: null,
    loading: false,
    error: false
}

const userReducer = (state = initialState, action) => {
    switch (action.type){
        case GET_LIST_USER: {
            return { ...state, loading: true, error: null }
        }
        case GET_LIST_USER_SUCCESS: {
            return { ...state, loading: true, error: null, users: action.payload }
        }
        case LOGIN: {
            return { ...state, loading: true, error: null }
        }
        case LOGIN_SUCCESS: {
            return { ...state, loading: true, error: null, user:action.payload }
        }
        case LOGIN_ERROR: {
            return { ...state, loading: true, error:action.payload }
        }
        case LOGOUT: {
            return { ...state, error: null, user: null}
        }
        case GET_LOCAL_USER_DATA: {
            const token = JSON.parse(localStorage.getItem('accessToken')?.toString() || null)
            if(token) {                
                const decodedInfo = jwt.decode(token) || null;
                return { ...state, user: decodedInfo, error: null }
            }
            return { ...state, user: null, error: null }
        }
        default:
            return state
    }
}

export default userReducer