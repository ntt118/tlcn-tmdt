import { takeLatest, call, put, select } from "redux-saga/effects";
import UserApi from "../../api/user.api";
import { getListUserSuccess, GET_LIST_USER, loginSuccess, LOGIN, loginError } from "../actions/user.action";

function* getUserListSaga({ payload }) {
    try {
        const response = yield call(UserApi.getAll);
        console.log('response' , response)
        yield put(getListUserSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

function* userLogin({payload}) {
    try {
        const response = yield call(UserApi.login, payload);
        if(response.Value) {
            yield put(loginSuccess(response.Value));
            localStorage.setItem('role', JSON.stringify(response.Value.role))
            localStorage.setItem('accessToken', JSON.stringify(response.accessToken))
        }
        else{
            yield put(loginError(response.errMessage));
        }
    }
    catch (error){
        console.log('saga error', error);
    }
}

export default function* userSaga() {
    yield takeLatest(GET_LIST_USER, getUserListSaga)
    yield takeLatest(LOGIN, userLogin)
}
