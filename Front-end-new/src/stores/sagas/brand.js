import { takeLatest, call, put } from "redux-saga/effects";
import BrandApi from "src/api/brand";
import { getListBrandSuccess, GET_LIST_BRAND } from "../actions/brand.action";

function* getBrandListSaga({ payload }) {
    try {
        const response = yield call(BrandApi.getAll);
        yield put(getListBrandSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

export default function* brandSaga() {
    yield takeLatest(GET_LIST_BRAND, getBrandListSaga)
}
