import { takeLatest, call, put } from "redux-saga/effects";
import CartApi from "src/api/cart";
import { GET_CART, getCartSuccess } from "../actions/cart.action";

function* getCart({ payload }) {
    try {
        const response = yield call(CartApi.getByUserId, payload)
        yield put(getCartSuccess(response.Value))
    }
    catch (err) {
        console.log(err)
    }
}

export default function* cartSaga() {
    yield takeLatest(GET_CART, getCart)
}