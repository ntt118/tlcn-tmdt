import { takeLatest, call, put } from "redux-saga/effects";
import CategoryApi from "src/api/category";
import { getListCategorySuccess, GET_LIST_CATEGORY } from "../actions/category.action";

function* getCategoryListSaga({ payload }) {
    try {
        const response = yield call(CategoryApi.getAll);
        yield put(getListCategorySuccess(response));
    } catch (error) {
        console.log(error);
    }
}

export default function* categorySaga() {
    yield takeLatest(GET_LIST_CATEGORY, getCategoryListSaga)
}
