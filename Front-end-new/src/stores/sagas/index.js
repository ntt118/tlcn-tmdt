import { all } from "redux-saga/effects";
import userSaga from '../sagas/user.saga'
import brandSaga from "./brand";
import categorySaga from "./category";
import productSaga from "./product";
import promotionSaga from "./promotion";
import cartSaga from "./cart";
import otherDetailSaga from "./otherdetail";
import orderSaga from "./order";

export default function* rootSaga() {
    yield all([
      userSaga(),
      categorySaga(),
      brandSaga(),
      productSaga(),
      promotionSaga(),
      cartSaga(),
      otherDetailSaga(),
      orderSaga()
    ]);
  }  