import { takeLatest, call, put } from "redux-saga/effects";
import OtherDetailApi from "src/api/otherDetail";
import { getListColorSuccess, getListOtherDetailSuccess, getListSizeSuccess, getListTypeSuccess, GET_LIST_COLOR, GET_LIST_OTHER_DETAIL, GET_LIST_SIZE, GET_LIST_TYPE } from "../actions/otherdetail.action";

function* getOtherDetailListSaga({ payload }) {
    try {
        console.log('payload', payload)
        var response;
        if (payload === "COLOR") {
            response = yield call(OtherDetailApi.getAll, { type: "COLOR" });
        } else if (payload === "SIZE") {
            response = yield call(OtherDetailApi.getAll, { type: "SIZE" });
        } else response = yield call(OtherDetailApi.getAll, { type: "ALL" });
        yield put(getListOtherDetailSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

function* getListSizeSaga() {
    try {
        const response = yield call(OtherDetailApi.getAll, { type: "SIZE" });
        yield put(getListSizeSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

function* getListColorSaga() {
    try {
        const response = yield call(OtherDetailApi.getAll, { type: "COLOR" });
        yield put(getListColorSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

function* getListTypeSaga() {
    try {
        const response = yield call(OtherDetailApi.getListType);
        yield put(getListTypeSuccess(response.Value));
    } catch (error) {
        console.log(error);
    }
}

export default function* otherDetailSaga() {
    yield takeLatest(GET_LIST_OTHER_DETAIL, getOtherDetailListSaga)
    yield takeLatest(GET_LIST_TYPE, getListTypeSaga)
    yield takeLatest(GET_LIST_SIZE, getListSizeSaga)
    yield takeLatest(GET_LIST_COLOR, getListColorSaga)
}
