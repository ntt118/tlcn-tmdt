import { takeLatest, call, put } from "redux-saga/effects";
import PromotionApi from "src/api/promotion";
import { getListPromotionSuccess, GET_LIST_PROMOTION } from "../actions/promotion.action";

function* getPromotionListSaga({ payload }) {
    try {
        const response = yield call(PromotionApi.getAll);
        yield put(getListPromotionSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

export default function* promotionSaga() {
    yield takeLatest(GET_LIST_PROMOTION, getPromotionListSaga)
}
