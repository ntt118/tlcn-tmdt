import { takeLatest, call, put } from "redux-saga/effects";
import OrderApi from "src/api/order";
import { GET_LIST_ORDER, getListOrderSuccess, GET_LIST_ORDER_BY_USER_ID, getListOrderByUserIdSuccess, GET_ORDER_BY_ID, getOrderByIdSuccess, GET_DASH_BOARD, getDashboardSuccess } from "../actions/order.action";

function* getOrderListSaga({ payload }) {
    try {
        const response = yield call(OrderApi.getAll);
        yield put(getListOrderSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

function* getOrderListByIdSaga({ payload }) {
    try {
        const response = yield call(OrderApi.getByUserId, payload);
        yield put(getListOrderByUserIdSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

function* getOrderByIdSaga({payload}) {
    try {
        const response = yield call(OrderApi.getById, payload);
        yield put(getOrderByIdSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

function* getDashboard({payload}) {
    try {
        const response = yield call(OrderApi.dashboard, payload);
        yield put(getDashboardSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

export default function* orderSaga() {
    yield takeLatest(GET_LIST_ORDER, getOrderListSaga)
    yield takeLatest(GET_LIST_ORDER_BY_USER_ID, getOrderListByIdSaga)
    yield takeLatest(GET_ORDER_BY_ID, getOrderByIdSaga)
    yield takeLatest(GET_DASH_BOARD, getDashboard)
}
