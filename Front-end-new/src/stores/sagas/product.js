import { takeLatest, call, put } from "redux-saga/effects";
import ProductApi from "src/api/product";
import { getListProductSuccess, GET_LIST_PRODUCT } from "../actions/product.action";

function* getProductListSaga({ payload }) {
    try {
        const response = yield call(ProductApi.getAll);
        yield put(getListProductSuccess(response));
    } catch (error) {
        console.log(error);
    }
}

export default function* productSaga() {
    yield takeLatest(GET_LIST_PRODUCT, getProductListSaga)
}
