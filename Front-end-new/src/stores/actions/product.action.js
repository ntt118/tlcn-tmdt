export const GET_LIST_PRODUCT = 'GET_LIST_PRODUCT'
export const GET_LIST_PRODUCT_SUCCESS = 'GET_LIST_PRODUCT_SUCCESS'

export const getListProduct = payload => ({
    type: GET_LIST_PRODUCT,
    payload
})

export const getListProductSuccess = payload => ({
    type: GET_LIST_PRODUCT_SUCCESS,
    payload
})