export const GET_LIST_BRAND = 'GET_LIST_BRAND'
export const GET_LIST_BRAND_SUCCESS = 'GET_LIST_BRAND_SUCCESS'

export const getListBrand = payload => ({
    type: GET_LIST_BRAND,
    payload
})

export const getListBrandSuccess = payload => ({
    type: GET_LIST_BRAND_SUCCESS,
    payload
})