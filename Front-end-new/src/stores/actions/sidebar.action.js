export const SET_SIDE_BAR_SHOW = `SET_SIDE_BAR_SHOW`;

export const setSideBarShow = payload => ({
	type: SET_SIDE_BAR_SHOW,
	payload,
});