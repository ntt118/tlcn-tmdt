export const GET_LIST_CATEGORY = 'GET_LIST_CATEGORY'
export const GET_LIST_CATEGORY_SUCCESS = 'GET_LIST_CATEGORY_SUCCESS'

export const getListCategory = payload => ({
    type: GET_LIST_CATEGORY,
    payload
})

export const getListCategorySuccess = payload => ({
    type: GET_LIST_CATEGORY_SUCCESS,
    payload
})