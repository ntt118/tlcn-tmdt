export const GET_CART = 'GET_CART'
export const GET_CART_SUCCESS = 'GET_CART_SUCCESS'
export const RESET_CART = 'RESET_CART'

export const getCart = payload => ({
    type: GET_CART,
    payload
})

export const getCartSuccess = payload => ({
    type: GET_CART_SUCCESS,
    payload
})

export const resetCart = payload => ({
    type: RESET_CART,
    payload
})