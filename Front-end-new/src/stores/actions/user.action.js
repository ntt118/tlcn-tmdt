export const GET_LIST_USER = `GET_LIST_USER`;
export const GET_LIST_USER_SUCCESS = `GET_LIST_USER_SUCCESS`;
export const LOGIN = `LOGIN`;
export const LOGIN_SUCCESS = `LOGIN_SUCCESS`;
export const LOGOUT = 'LOGOUT';
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const GET_LOCAL_USER_DATA = 'GET_LOCAL_USER_DATA';

export const getListUser = payload => ({
	type: GET_LIST_USER,
	payload,
});

export const getListUserSuccess = payload => ({
	type: GET_LIST_USER_SUCCESS,
	payload,
});

export const login = payload => ({
	type: LOGIN,
	payload
})

export const loginSuccess = payload => ({
	type: LOGIN_SUCCESS,
	payload
})

export const loginError = payload => ({
	type: LOGIN_ERROR,
	payload
})

export const logout = payload => ({
	type: LOGOUT,
	payload
})

export const getLocalUserData = payload => ({
	type: GET_LOCAL_USER_DATA,
	payload
})
