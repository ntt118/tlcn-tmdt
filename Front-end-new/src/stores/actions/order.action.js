export const GET_LIST_ORDER = 'GET_LIST_ORDER'
export const GET_LIST_ORDER_SUCCESS = 'GET_LIST_ORDER_SUCCESS'
export const GET_LIST_ORDER_BY_USER_ID = 'GET_LIST_ORDER_BY_USER_ID'
export const GET_LIST_ORDER_BY_USER_ID_SUCCESS = 'GET_LIST_ORDER_BY_USER_ID_SUCCESS'
export const GET_ORDER_BY_ID = 'GET_ORDER_BY_ID'
export const GET_ORDER_BY_ID_SUCCESS = 'GET_ORDER_BY_ID_SUCCESS'
export const GET_DASH_BOARD = 'GET_DASH_BOARD'
export const GET_DASH_BOARD_SUCCESS = 'GET_DASH_BOARD_SUCCESS'

export const getListOrder = payload => ({
    type: GET_LIST_ORDER,
    payload
})

export const getListOrderSuccess = payload => ({
    type: GET_LIST_ORDER_SUCCESS,
    payload
})

export const getListOrderByUserId = payload => ({
    type: GET_LIST_ORDER_BY_USER_ID,
    payload
})


export const getListOrderByUserIdSuccess = payload => ({
    type: GET_LIST_ORDER_BY_USER_ID_SUCCESS,
    payload
})

export const getOrderById = payload => ({
    type: GET_ORDER_BY_ID,
    payload
})

export const getOrderByIdSuccess = payload => ({
    type: GET_ORDER_BY_ID_SUCCESS,
    payload
})

export const getDashboard = payload => ({
    type: GET_DASH_BOARD,
    payload
})

export const getDashboardSuccess = payload => ({
    type: GET_DASH_BOARD_SUCCESS,
    payload
})