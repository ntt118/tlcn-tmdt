export const GET_LIST_OTHER_DETAIL = 'GET_LIST_OTHER_DETAIL'
export const GET_LIST_OTHER_DETAIL_SUCCESS = 'GET_LIST_OTHER_DETAIL_SUCCESS'
export const GET_LIST_TYPE = 'GET_LIST_TYPE'
export const GET_LIST_TYPE_SUCCESS = 'GET_LIST_TYPE_SUCCESS'
export const GET_LIST_SIZE = 'GET_LIST_SIZE'
export const GET_LIST_SIZE_SUCCESS = 'GET_LIST_SIZE_SUCCESS'
export const GET_LIST_COLOR = 'GET_LIST_COLOR'
export const GET_LIST_COLOR_SUCCESS = 'GET_LIST_COLOR_SUCCESS'

export const getListOtherDetail = payload => ({
    type: GET_LIST_OTHER_DETAIL,
    payload
})

export const getListOtherDetailSuccess = payload => ({
    type: GET_LIST_OTHER_DETAIL_SUCCESS,
    payload
})

export const getListType = payload => ({
    type: GET_LIST_TYPE,
    payload
})

export const getListTypeSuccess = payload => ({
    type: GET_LIST_TYPE_SUCCESS,
    payload
})

export const getListSize = payload => ({
    type: GET_LIST_SIZE,
    payload
})

export const getListSizeSuccess = payload => ({
    type: GET_LIST_SIZE_SUCCESS,
    payload
})

export const getListColor = payload => ({
    type: GET_LIST_COLOR,
    payload
})

export const getListColorSuccess = payload => ({
    type: GET_LIST_COLOR_SUCCESS,
    payload
})