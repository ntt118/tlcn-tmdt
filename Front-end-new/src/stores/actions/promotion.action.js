export const GET_LIST_PROMOTION = 'GET_LIST_PROMOTION'
export const GET_LIST_PROMOTION_SUCCESS = 'GET_LIST_PROMOTION_SUCCESS'

export const getListPromotion = payload => ({
    type: GET_LIST_PROMOTION,
    payload
})

export const getListPromotionSuccess = payload => ({
    type: GET_LIST_PROMOTION_SUCCESS,
    payload
})