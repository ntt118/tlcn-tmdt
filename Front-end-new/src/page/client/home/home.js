import {
    TheFooter,
    TheHeader,
    TheContent
  } from 'src/containers/client/index'

const Home = () => {

    return (
        <div id="home">
            <div className="c-wrapper">
                <TheHeader/>
                <div className="c-body" style={{background: "#fff", marginBottom: '10px'}}>
                    <TheContent />
                </div>
                <TheFooter/>
            </div>
        </div>
    )
}

export default Home