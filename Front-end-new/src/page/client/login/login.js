import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from "react-redux"
import { useHistory, useLocation } from "react-router"
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CLink
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import './login.scss'
import Toaster from 'src/components/toaster'
import { login } from 'src/stores/actions/user.action'

const Login = () => {

    const dispatch = useDispatch()
    const history = useHistory()
    const query = new URLSearchParams(useLocation().search)
    const user = useSelector(state => state.user.user)
    const errorMsg = useSelector(state => state.user.error)

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [returnUrl, setReturnUrl] = useState(null)
    const toastRef = useRef()

    const Login = () => {
        dispatch(login({username, password}))
    }

    useEffect(() => {
        const urlReturn = query.get('returnUrl')
        if (urlReturn) {
            setReturnUrl(urlReturn)
        }
    }, [])

    useEffect(() => {
        if(user) {
            if (returnUrl)
                history.push(returnUrl)
            else if (user.role === "ADMIN")
                history.push('/dashboard')
            else
                history.push('/home')
        }
    }, [user])

    useEffect(() => {
        if(errorMsg) {
            toastRef.current.showToaster()
        }
    }, [errorMsg])

    return (
        <div id="client-login">
            <CContainer>
                <CRow className="justify-content-center">
                    <CCol md="6">
                        <CCardGroup>
                            <CCard className="p-4">
                                <CCardBody>
                                    <CForm>
                                        <h1>Đăng nhập</h1>
                                        <p className="text-muted">Đăng nhập vào tài khoản của bạn</p>
                                        <CInputGroup className="mb-3">
                                        <CInputGroupPrepend>
                                            <CInputGroupText>
                                            <CIcon name="cil-user" />
                                            </CInputGroupText>
                                        </CInputGroupPrepend>
                                        <CInput type="text" value={username} onChange={(e) => setUsername(e.target.value)} placeholder="Tên tài khoản" autoComplete="username" />
                                        </CInputGroup>
                                        <CInputGroup className="mb-4">
                                        <CInputGroupPrepend>
                                            <CInputGroupText>
                                            <CIcon name="cil-lock-locked" />
                                            </CInputGroupText>
                                        </CInputGroupPrepend>
                                        <CInput type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Mật khẩu" autoComplete="current-password" />
                                        </CInputGroup>
                                        <CRow>
                                        <CCol xs="6">
                                            <CButton color="primary" onClick={() => Login()} className="px-4">Đăng nhập</CButton>
                                        </CCol>
                                        <CCol xs="6" className="text-right">
                                            <CLink href="#/signUp" className="px-1">Đăng ký</CLink>
                                            <CButton href='#/forget-password' color="link" className="px-1">Quên mật khẩu?</CButton>
                                        </CCol>
                                        </CRow>
                                    </CForm>
                                </CCardBody>
                            </CCard>
                        </CCardGroup>
                    </CCol>
                </CRow>
            </CContainer>
            <Toaster ref={toastRef} title="Lỗi đăng nhập" content={errorMsg || 'Không xác định'} />
        </div>
    )
}

export default Login