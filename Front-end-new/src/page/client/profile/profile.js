
import React, { useEffect, useRef, useState } from 'react'
import UserApi from 'src/api/user.api'
import { useHistory } from 'react-router'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CInvalidFeedback
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Toaster from 'src/components/toaster'
import { validateEmail, validatePassword, validateName, validatePhone, validateUsername } from 'src/utils'
import { useDispatch, useSelector } from 'react-redux'
import { login } from 'src/stores/actions/user.action'

const UserProfile = () => {
    
    const history = useHistory()
    const dispatch = useDispatch()
    const user = useSelector(state => state.user.user)

    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [password, setPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [rePassword, setRePassword] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')

    const [notifyContent, setNotifyContent] = useState('')
    const [notifyTitle, setNotifyTitle] = useState('')

    const toastRef = useRef()

    useEffect(() => {
        if(user) {
            setUsername(user.username)
            setEmail(user.email)
            setName(user.name)
            setAddress(user.address)
            setPhoneNumber(user.phoneNumber)
        }
        else {
            history.goBack()
        }
    }, [])
    
    const validationCheck = () => {
        if(newPassword?.length && (!validatePassword(newPassword) || newPassword!==rePassword)){
            return false
        }
        if(validateName(name) || !validateUsername(username) || !validatePhone(phoneNumber) || !validateEmail(email) || !validatePassword(password))
        {
            return false
        }
        return true
    }

    const updateProfile = () => {
        if (validationCheck()) {
            UserApi.update({username, name, phoneNumber, email, address, oldPassword: password, password: newPassword}).then(rs => {
                if(rs.Value) {
                    dispatch(login({username, password: newPassword.length ? newPassword : password}))
                    setNotifyContent('Thay đổi thông tin thành công')
                    setNotifyTitle('Thay đổi thông tin thành công')
                    toastRef.current.showToaster()
                    setTimeout(() => {
                        history.push('/home')
                    }, 3000)
                }
                else {
                    setNotifyContent(rs.errMessage)
                    setNotifyTitle('Thay đổi thông tin thất bại')
                    toastRef.current.showToaster()
                }
            })
        }
        else {
            setNotifyContent('Hãy nhập hết các trường')
            setNotifyTitle('Hãy nhập hết các trường')
            toastRef.current.showToaster()
        }
    }

    return (
        <div className="c-app c-default-layout flex-row align-items-center">
            <CContainer>
                <CRow className="justify-content-center">
                <CCol md="9" lg="7" xl="6">
                    <CCard className="mx-4">
                    <CCardBody className="p-4">
                        <CForm>
                        <h1>Thay đổi thông tin</h1>
                        <p className="text-muted">Thay đổi thông tin tài khoản của bạn.</p>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-user" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput disabled type="text" invalid={!validateUsername(username)} value={username} onChange={(e) => setUsername(e.target.value)} placeholder="Username" autoComplete="username" />
                            <CInvalidFeedback>Tên đăng nhập không phù hợp</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-user" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="text" invalid={validateName(name)} value={name} onChange={(e) => setName(e.target.value)} placeholder="Name" autoComplete="name" />
                            <CInvalidFeedback>Tên không phù hợp.</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-phone" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="number" invalid={phoneNumber !== '' && !validatePhone(phoneNumber)} value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} placeholder="Phone" autoComplete="phone" />
                            <CInvalidFeedback>Số điện thoại không đúng.</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>@</CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="text" invalid={email !== '' && !validateEmail(email)} value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email" autoComplete="email" />
                            <CInvalidFeedback>Email không đúng.</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-home" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="text" value={address} onChange={(e) => setAddress(e.target.value)} placeholder="Địa chỉ" autoComplete="email" />
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-lock-locked" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="password" invalid={password !=='' && !validatePassword(password)} value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Password" autoComplete="password" />
                            <CInvalidFeedback>Mật khẩu phải có ít nhất 8 ký tự và bao gồm số(0-9)</CInvalidFeedback>
                        </CInputGroup>
                        <div style={{textAlign:'center', marginBottom: '1rem'}}>
                            Nhập mật khẩu mới nếu bạn muốn đổi mật khẩu
                        </div>                    
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-lock-locked" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="password" invalid={newPassword !=='' && !validatePassword(newPassword)} value={newPassword} onChange={(e) => setNewPassword(e.target.value)} placeholder="New Password" autoComplete="new-password" />
                            <CInvalidFeedback>Mật khẩu phải có ít nhất 8 ký tự và bao gồm số(0-9)</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-4">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-lock-locked" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="password" invalid={newPassword !== rePassword} value={rePassword} onChange={(e) => {setRePassword(e.target.value)}} placeholder="Repeat password" autoComplete="password" />
                            <CInvalidFeedback>Mật khẩu không khớp</CInvalidFeedback>
                        </CInputGroup>
                        <CButton color="success" onClick={()=> updateProfile()} block>Cập nhật</CButton>
                        </CForm>
                    </CCardBody>
                    </CCard>
                </CCol>
                </CRow>
            </CContainer>
            <Toaster ref={toastRef} title={notifyTitle} content={notifyContent} />
        </div>
    )
}

export default UserProfile 