import { useDispatch, useSelector } from "react-redux"
import { useEffect, useRef, useState } from "react"
import { getListOrderByUserId } from "src/stores/actions/order.action"
import Toaster from "src/components/toaster"
import { CButton, CCard, CCardBody, CCardHeader, CContainer, CDataTable, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react"
import CIcon from "@coreui/icons-react"
import Moment from 'react-moment';
import { OrderStatus } from "src/page/admin/order/order-status-const"
import OrderApi from "src/api/order"
import { formatPrice } from "src/page/utils"
import { getListProduct } from "src/stores/actions/product.action"

const OrderList = () => {
    const dispatch = useDispatch()

    const user = useSelector(state => state.user.user)
    const products = useSelector(state => state.product.products)
    const orders = useSelector(state => state.order.orders)
    const successToastRef = useRef()
    const failToastRef = useRef()
    const fields = [
        { key: 'dateCreate', label: 'Ngày tạo', _style: { width: '150px'} }, 
        { key: 'paymentType', label: 'Phương thức thanh toán', _style: { width: '150px'} }, 
        { key: 'isPayment', label: 'Trạng thái thanh toán', _style: { width: '150px'} }, 
        { key: 'status', label: 'Trạng thái đơn hàng', _style: { width: '150px'} }, 
        { key: 'total', label: 'Thành tiền', _style: { width: '100px'} }, 
        'action'
    ]

    const detailFields = [
        { key: 'name', label: 'Tên sản phẩm', _style: { width: '150px'} }, 
        { key: 'image', label: 'Ảnh sản phẩm', _style: { width: '100px'} }, 
        { key: 'color', label: 'Màu', _style: { width: '10px'} }, 
        { key: 'size', label: 'Kích thước', _style: { width: '70px'} }, 
        { key: 'amount', label: 'Số lượng', _style: { width: '50px'} }, 
        { key: 'price', label: 'Giá', _style: { width: '100px'} }, 
    ]

    const [showDetailOrder, setShowDetailOrder] = useState(false)
    const [selectedOrder, setSelectedOrder] = useState(null)

    useEffect(() => {
        if(user?.id) {            
            dispatch(getListOrderByUserId({id: user.id}))
        }
    }, [user])

    useEffect(() => {
        if(!products?.length) {            
            dispatch(getListProduct())
        }
    }, [products])

    const [showModal, setShowModal] = useState(false)
    const [orderSelected, setOrderSelected] = useState(null)
    const showCancelOrder = (order) => {
        setOrderSelected(order)
        setShowModal(true)
    }
    const cancelOrder = () => {
        OrderApi.cancelOrder({id: orderSelected.id}).then(rs => {
            dispatch(getListOrderByUserId({id: user.id}))
            setShowModal(false)
        })
    }

    const getProductsDetail = (otherDetail) => {
        let result = null
        products.forEach(product => {
            const exist = product.productotherdetails.filter(detail => 
                detail.otherdetailId === otherDetail.productotherdetailId
            )[0]
            if(exist)
                result = {...product, detailId: otherDetail.productotherdetailId, amount:otherDetail.quantity }
        })
        return result
    }

    return (
        <div>
            <CContainer>
                <CCard className="category" style={{ border: 'none' }}>
                <CCardHeader className="text-center"  style={{ border: 'none' }}>
                    <h2 className="m-0">Danh sách đơn hàng của bạn</h2>
                </CCardHeader>
                <CCardBody>
                    <CDataTable
                        items={orders}
                        fields={fields}
                        striped
                        itemsPerPage={5}
                        pagination
                        scopedSlots={{
                            'dateCreate':
                                    (item, index) => (
                                        <td>
                                            <Moment format="LLL">
                                                {item.dateCreate}
                                            </Moment> 
                                        </td>
                                    ),
                            'paymentType': 
                                    (item, index) => (
                                        <td>{item.paymentType === 1 ? 'Ví Momo' : 'COD'}</td>
                                    ),
                            'isPayment':
                                    (item, index) => (
                                        <td>{item.isPayment === 1 ? 'Đã thanh toán' : 'Chưa thanh toán'}</td>
                                    ),
                            'status':
                                    (item, index) => (
                                        <td>{item.status === 0 ? 'Chờ xác nhận' 
                                            :item.status === 1 ? 'Đã Xác nhận' 
                                            :item.status === 2 ? 'Đang vận chuyển' 
                                            :item.status === 3 ? 'Đã nhận hàng' : 'Huỷ'}</td>
                                    ),
                            'total':
                            (item, index) => (
                                <td>{formatPrice(item.total)} &#x111;</td>
                            ),
                            'action': (item, index) => (
                                <td style={{ width: '100px', textAlign: 'center' }}>
                                     <CButton onClick={() => {
                                         setShowDetailOrder(true)
                                         setSelectedOrder(item)
                                        }} color="primary">Xem</CButton>
                                    {(item.status === OrderStatus.Waiting) && <CButton onClick={() => showCancelOrder(item)} style={{marginLeft: '5px'}} color="danger">Huỷ </CButton>}
                                </td>
                            )
                        }}
                    />
                </CCardBody>
            </CCard>
            </CContainer>
            <Toaster ref={successToastRef} title="Thành công" content="Đặt hàng thành công!"/>
            <Toaster ref={failToastRef} title="Thiếu thông tin." content="Hãy nhập đầy đủ thông tin!" />
            
            <CModal show={showModal}>
                <CModalHeader>
                    <CModalTitle>Huỷ đơn hàng</CModalTitle>
                </CModalHeader>
                <CModalBody>Bạn có chắc chắn huỷ đơn này ?</CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setShowModal(false)}>
                        Đóng
                    </CButton>
                    <CButton color="danger" onClick={() => cancelOrder()}>Huỷ</CButton>
                </CModalFooter>
            </CModal>

            <CModal show={showDetailOrder} size='lg'>
                <CModalHeader>
                    <CModalTitle>Chi tiết đơn hàng</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    { selectedOrder !== null 
                        && selectedOrder.orderdetails !== null 
                        && <CDataTable
                        items={selectedOrder.orderdetails.map(item => getProductsDetail(item))}
                        fields={detailFields}
                        striped
                        itemsPerPage={5}
                        pagination
                        scopedSlots={{
                            'image': 
                            (item, index) => (
                                <img style={{width: '100px', height:'100px'}} src={item.imageproducts[0]?.image} alt="slide 1"/>
                                ),
                                'color':
                            (item, index) => (
                                <td>
                                                {item.productotherdetails.filter(detail => detail.otherdetailId === item.detailId)[0]?.colorName}
                                            </td>
                                        ),
                                        'size': 
                                        (item, index) => (
                                            <td>
                                                {item.productotherdetails.filter(detail => detail.otherdetailId === item.detailId)[0]?.sizeName}
                                            </td>
                                        ),
                                        'price':
                                        (item, index) => (
                                            <td>
                                                {formatPrice(item.productotherdetails.filter(detail => detail.otherdetailId === item.detailId)[0]?.price)}&#x111;
                                            </td>
                                        ),
                                    }}
                                    />
                                }
                            </CModalBody>
                        <CModalFooter>
                    <CButton color="secondary" onClick={() => setShowDetailOrder(false)}>
                        Đóng
                    </CButton>
                </CModalFooter>
            </CModal>
            </div>
            )
}

export default OrderList