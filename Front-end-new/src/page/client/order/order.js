import { CButton, CCol, CContainer, CForm, CFormGroup, CInput, CInputGroup, CInputRadio, CLabel, CRow, CSelect } from "@coreui/react"
import { useEffect, useRef, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory, useLocation } from "react-router"
import { getCart } from "src/stores/actions/cart.action"
import { getListProduct } from "src/stores/actions/product.action"
import "./order.scss"
import addressApi  from "./addressApi"
import { formatPrice } from "src/page/utils"
import Toaster from "src/components/toaster"
import OrderApi from "src/api/order"
const Order = () => {    
    
    const dispatch = useDispatch()
    const history = useHistory()
    const location = useLocation()
    const user = useSelector(state => state.user.user)
    const products = useSelector(state => state.product.products)
    const cart =  useSelector(state => state.cart.cart)
    const [paymentType, setPaymentType] = useState(0)
    const successToastRef = useRef()
    const failToastRef = useRef()
    
    const orgData = {
        province: null,
        district: null,
        wards: null,
        address: null
    }

    const [data, setData] = useState(orgData)
    const [voucherCode, setVoucherCode] = useState(null)
    
    useEffect(() => {
        dispatch(getListProduct())
    }, [])

    useEffect(() => {        
    }, [data])

    useEffect(() => {
        if(!user?.id){
            history.push(`/login?returnUrl=${location.pathname}`)
        }
        else {
            setData({...data, address: user.address})
            dispatch(getCart(user.id))
        }
    }, [user])

    const onChangeField = (e, field) => {
        if(field === 'province'){
            setData({...data, province: e.target.value, district: null, wards: null})
        }
        else{
            setData({...data, [field]: e.target.value})
        }
    }

    const MapAddress = () => {
        return addressApi.filter(p => p.codename === data.province)[0]?.name + ", "+ 
        addressApi.filter(p => p.codename === data.province)[0]?.districts.filter(d => d.codename === data.district)[0]?.name + ", " + 
        addressApi.filter(p => p.codename === data.province)[0]?.districts.filter(d => d.codename === data.district)[0]?.wards.filter(w => w.codename === data.wards)[0]?.name + ", " + 
        data.address
    }

    const GetTotal = () => {
        let total = 0;
        cart.forEach(item => {
            const product = products.filter(p => p.id === item.productotherdetail.productId)[0]
            const detail = product.productotherdetails.filter(d => d.otherdetailId === item.productotherdetailId)[0];
            total += item.quantity*detail?.price
        })
        return total
    }

    const order = () => {
        if(!data.province || !data.address || !data.district || !data.wards) {
            failToastRef.current.showToaster()
            return
        }
        const address = MapAddress()
        const total = GetTotal();

        const orderdetails = cart.map(item => {
            return {
                productotherdetailId: item.productotherdetailId,
                quantity: item.quantity,
            }
        })

        OrderApi.create({
            address,
            total,
            userId: user.id,
            orderdetails,
            paymentType,
        }).then(rs => {
            if(rs){
                if(paymentType===1) {
                    OrderApi.momo({id: rs.Value.id}).then(rs =>{
                        if(rs.url) {
                            window.open(rs.url.payUrl, '_blank').focus();
                        }
                    })
                }
                successToastRef.current.showToaster()
                setTimeout(() => {
                    dispatch(getCart(user.id))
                    history.push('/order')
                }, 3000)
            }
        })
    }

    return (
        <div>
            <CContainer>
                <CRow>
                    <CCol xs="12" lg="6" md="12" className="mt-3">
                        <h3>Thông tin giao hàng</h3>
                        <CForm>
                            <p>Địa chỉ nhận hàng:</p>
                            <CInput type="text" value={data.address} onChange={(e) => onChangeField(e, 'address')} placeholder="Địa chỉ nhận hàng"></CInput>
                            <div className="mt-3 address-selector">
                                <CInputGroup className="mr-3">
                                <CSelect custom value={data.province} onChange={(e) => onChangeField(e, 'province')} className="mr-3">
                                    <option key="-1" value="-1">Chọn tỉnh/thành phố</option>
                                    {addressApi && addressApi.map((province, index) => {
                                        return <option key={index} value={province.codename}>{province.name}</option>
                                    })}
                                </CSelect>
                                </CInputGroup>
                                <CInputGroup custom value={data.district} onChange={(e) => onChangeField(e, 'district')} className="mr-3">
                                    <CSelect >
                                        <option key="-1" value="-1">Chọn quận/huyện</option>
                                        {data?.province && data?.province!=="-1" && addressApi.filter(province => province.codename === data.province)[0]?.districts.map((district, index) => {
                                            return <option key={index} value={district.codename}>{district.name}</option>
                                        })}
                                    </CSelect>
                                </CInputGroup>
                                <CInputGroup custom value={data.wards} onChange={(e) => onChangeField(e, 'wards')} className="mr-3">
                                    <CSelect >
                                        <option key="-1" value="-1">Chọn phường/xã</option>
                                        { data?.district && data?.district!=="-1" && addressApi.filter(province => province.codename === data.province)[0]?.districts?.filter(ward => ward.codename === data.district)[0]?.wards.map((ward, index) => {
                                            return <option key={index} value={ward.codename}>{ward.name}</option>
                                        })}
                                    </CSelect>
                                </CInputGroup>
                            </div>
                            { (data.address && data.district && data.province && data.wards) &&
                                <p className="my-3"><strong>Địa chỉ hiện tại:</strong> {`${MapAddress()}`}</p>
                            }
                            <div className="mt-3">
                                <h4>Phương thức vận chuyển:</h4> Giao hàng tận nơi                                
                            </div>
                            <div className="my-3">
                                <h4>Phương thức thanh toán:</h4>  
                                <div style={{display: 'flex', fontSize: '1.1rem'}}>
                                    <CFormGroup variant="checkbox" className="mr-3">
                                        <CInputRadio className="form-check-input" id="radio1" name="radios" value="shipPayment" checked={paymentType===0} onChange={() => setPaymentType(0)}/>
                                        <CLabel variant="checkbox" htmlFor="radio1">Thanh toán khi giao hàng</CLabel>
                                    </CFormGroup>
                                    <CFormGroup variant="checkbox">
                                        <CInputRadio className="form-check-input" id="radio2" name="radios" value="momoPayment" checked={paymentType===1} onChange={() => setPaymentType(1)} />
                                        <CLabel variant="checkbox" htmlFor="radio2">Thanh toán qua Momo</CLabel>
                                    </CFormGroup>
                                </div>                              
                            </div>
                            <div className="mt-3" style={{display: 'flex', justifyContent:'space-between'}}>
                                <CButton color="primary" onClick={() => history.push('/cart')}>Giỏ hảng</CButton>
                                <CButton color="primary" style={{fontSize:'16px', fontWeight: 'bold'}} onClick={() => order()}>Đặt hàng</CButton>
                            </div>
                        </CForm>
                    </CCol>
                    <CCol xs="12" lg="6" md="12" className="order-price-cont mt-3">
                    { cart?.length > 0 && cart.map((item, index) => {
                        const product = products.filter(p => p.id === item.productotherdetail.productId)[0]
                        const detail = product.productotherdetails.filter(d => d.otherdetailId === item.productotherdetailId)[0];
                        return <div className="item-detail" style={index<cart?.length-1 ? {borderBottom:'solid 1px #c4c9d0', marginBottom:'10px'}: {}}>
                            <img className="d-inline-block item-image" src={product?.imageproducts[0]?.image} alt="slide 1"/>
                            <div style={{display:'flex', width:'100%', justifyContent:'space-between', alignSelf:'center'}}>
                                <div className="item-info">
                                    <span to={`/product/${item.productId}`} className="mb-1" style={{fontSize: '16px', fontWeight: 'bold'}}>
                                        {product?.name} - Size: S
                                    </span>
                                    <div className="item-quantity">
                                        Số lượng: {item.quantity}
                                    </div>
                                    <div className="item-price">
                                        Đơn giá: {formatPrice(detail?.price)} &#x111;
                                    </div>
                                </div>                                
                                <div className="item-total">
                                    <div className="total-item-price">
                                        {formatPrice(item.quantity*detail?.price)} &#x111;
                                    </div>
                                </div>      
                            </div>                     
                        </div>
                    }) } 
                    {/* <div className="order-discount mt-3 pt-3">
                        <CInput type="text" className="voucher-text" value={voucherCode} onChange={(e) => setVoucherCode(e.target.value)} placeholder="Mã giảm giá" />
                        <CButton className="voucher-btn" color="secondary">Sử dụng</CButton>
                    </div> */}
                    {/* <div className="order-price-include mt-3 pt-3">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Tạm tính</td>
                                    <td>{formatPrice(GetTotal())} &#x111;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div> */}
                    <div className="order-total-price">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Tổng cộng</td>
                                    <td>{formatPrice(GetTotal())} &#x111;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </CCol>
                </CRow>
            </CContainer>
            <Toaster ref={successToastRef} title="Thành công" content="Đặt hàng thành công!"/>
            <Toaster ref={failToastRef} title="Thiếu thông tin." content="Hãy nhập đầy đủ thông tin!" />
        </div>
    )
}

export default Order