import React, { useState, useRef } from 'react'
import {
    CButton,
    CCard,
    CCardBody,
    CCardGroup,
    CCol,
    CContainer,
    CForm,
    CInput,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CRow,
    CLink,
    CInvalidFeedback
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Toaster from 'src/components/toaster'
import UserApi from 'src/api/user.api'
import { validatePassword } from 'src/utils'

const ForgetPassword = () => {

    const [email, setEmail] = useState(null)
    const [code, setCode] = useState(null)
    const [newPass, setNewPassword] = useState(null)
    const [isSent, setIsSent] = useState(false)
    const toastRef = useRef()
    const [message, setMessage] = useState(null)
    const [titleToaster, setTitleToaster] = useState(null)

    const sentRequest = () => {
        UserApi.resetPassRequest({email}).then(rs => {
            if (!rs.errMessage) {
                setMessage('Mã xác nhận đã được gửi.\n Hãy kiểm tra mail của bạn.')
                setTitleToaster('Thành công')
                setIsSent(true)
            }
            else {
                console.log('rs', rs)
                setMessage(rs.errMessage)
                setTitleToaster('Không thành công')
            }
            toastRef.current.showToaster()
        })
    }

    const resetPassword = () => {
        if (!isSent) {
            sentRequest()
        }
        else {            
            setIsSent(true)
            if(validatePassword(newPass)) {
                UserApi.resetPassword({newPassword: newPass, email, resetCode: code}).then(rs => {
                    if (!rs.errMessage) {
                        setMessage('Thay đổi mật khẩu thành công')
                        setTitleToaster('Thành công')
                    }
                    else {
                        setMessage(rs.errMessage)
                        setTitleToaster('Không thành công')
                    }
                    toastRef.current.showToaster()
                })
            }
            else {
                setMessage('Hãy nhập đầy đủ các trường.')
                setTitleToaster('Lỗi')
                toastRef.current.showToaster()
            }
        }
    }


    return (
        <div style={{display:'flex', minHeight: '100vh', flexDirection: 'row', alignItems: 'center'}}>
            <CContainer>
                <CRow className="justify-content-center">
                    <CCol md="6">
                        <CCardGroup>
                            <CCard className="p-4">
                                <CCardBody>
                                    <CForm>
                                        <h1>Quên tài khoản</h1>
                                        <p className="text-muted">Nhập email tài khoản của bạn</p>
                                        <CInputGroup className="mb-3">
                                        <CInputGroupPrepend>
                                            <CInputGroupText>
                                            <CIcon name="cil-user" />
                                            </CInputGroupText>
                                        </CInputGroupPrepend>
                                        <CInput type="text" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="E-mail" autoComplete="email" />
                                        </CInputGroup>
                                        {   isSent && <><CInputGroup className="mb-4 w-75">
                                                <CInputGroupPrepend>
                                                    <CInputGroupText>
                                                    <CIcon name="cil-lock-locked" />
                                                    </CInputGroupText>
                                                </CInputGroupPrepend>
                                                <CInput className="mr-3" type="number" value={code} onChange={(e) => setCode(e.target.value)} placeholder="Mã khôi phục" autoComplete="code" />
                                                <CButton color="primary" onClick={() => sentRequest()}>Gửi lại</CButton>
                                            </CInputGroup>
                                            <CInputGroup className="mb-4 w-75">
                                                <CInputGroupPrepend>
                                                    <CInputGroupText>
                                                    <CIcon name="cil-lock-locked" />
                                                    </CInputGroupText>
                                                </CInputGroupPrepend>
                                                <CInput className="mr-3" type="password" invalid={newPass && !validatePassword(newPass)} value={newPass} onChange={(e) => setNewPassword(e.target.value)} placeholder="Mật khẩu mới" autoComplete="code" />
                                                <CInvalidFeedback>Password must contains at least 8 characters and includes number(0-9)</CInvalidFeedback>
                                            </CInputGroup>
                                            </>
                                        }                                        
                                        <CRow>
                                        <CCol xs="6">
                                            <CButton color="primary" onClick={() => resetPassword()} className="px-4">{!isSent ? 'Xác nhận': 'Đổi mật khẩu'}</CButton>
                                        </CCol>
                                        <CCol xs="6" className="text-right">
                                            <CLink href="#/login" className="px-1">Đăng nhập</CLink>
                                        </CCol>
                                        </CRow>
                                    </CForm>
                                </CCardBody>
                            </CCard>
                        </CCardGroup>
                    </CCol>
                </CRow>
            </CContainer>
            <Toaster ref={toastRef} title={titleToaster} content={message || 'Không xác định'} />
        </div>
    )
}

export default ForgetPassword