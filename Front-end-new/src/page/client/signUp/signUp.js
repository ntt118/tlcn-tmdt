
import React, { useRef, useState } from 'react'
import UserApi from 'src/api/user.api'
import { useHistory } from 'react-router'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CInvalidFeedback
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Toaster from 'src/components/toaster'
import { validateEmail, validatePassword, validateName, validatePhone, validateUsername } from 'src/utils'

const SignUp = () => {
    
    const history = useHistory()

    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [name, setName] = useState('')
    const [address, setAddress] = useState('')
    const [password, setPassword] = useState('')
    const [rePassword, setRePassword] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')

    const [notifyContent, setNotifyContent] = useState('')
    const [notifyTitle, setNotifyTitle] = useState('')

    const toastRef = useRef()
    
    const validationCheck = () => {
        if(validateName(name) || !validateUsername(username) || !validatePhone(phoneNumber) || !validateEmail(email) || !validatePassword(password) || password!==rePassword)
        {
            return false
        }
        if(password !== rePassword) {
            return false
        }
        return true
    }

    const signUpClicked = () => {
        if (validationCheck()) {
            UserApi.signUp({username, name, phoneNumber, email, address, password}).then(rs => {
                if(rs.Value) {
                    setNotifyContent('Đăng ký thành công')
                    setNotifyTitle('Đăng ký thành công')
                    toastRef.current.showToaster()
                    setTimeout(() => {
                        history.push('/login')
                    }, 3000)
                }
            })
        }
        else {
            setNotifyContent('Hãy nhập hết các trường')
            setNotifyTitle('Hãy nhập hết các trường')
            toastRef.current.showToaster()
        }
    }

    return (
        <div className="c-app c-default-layout flex-row align-items-center">
            <CContainer>
                <CRow className="justify-content-center">
                <CCol md="9" lg="7" xl="6">
                    <CCard className="mx-4">
                    <CCardBody className="p-4">
                        <CForm>
                        <h1>Đăng ký</h1>
                        <p className="text-muted">Tạo tài khoản của riêng bạn.</p>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-user" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="text" invalid={!validateUsername(username)} value={username} onChange={(e) => setUsername(e.target.value)} placeholder="Username" autoComplete="username" />
                            <CInvalidFeedback>Tên đăng nhập không phù hợp</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-user" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="text" invalid={validateName(name)} value={name} onChange={(e) => setName(e.target.value)} placeholder="Name" autoComplete="name" />
                            <CInvalidFeedback>Tên không phù hợp.</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-phone" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="number" invalid={phoneNumber !== '' && !validatePhone(phoneNumber)} value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} placeholder="Phone" autoComplete="phone" />
                            <CInvalidFeedback>Số điện thoại không đúng.</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>@</CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="text" invalid={email !== '' && !validateEmail(email)} value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email" autoComplete="email" />
                            <CInvalidFeedback>Email không đúng.</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-home" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="text" value={address} onChange={(e) => setAddress(e.target.value)} placeholder="Địa chỉ" autoComplete="email" />
                        </CInputGroup>
                        <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-lock-locked" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="password" invalid={password !=='' && !validatePassword(password)} value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Password" autoComplete="new-password" />
                            <CInvalidFeedback>Mật khẩu phải có ít nhất 8 ký tự và bao gồm số(0-9)</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-4">
                            <CInputGroupPrepend>
                            <CInputGroupText>
                                <CIcon name="cil-lock-locked" />
                            </CInputGroupText>
                            </CInputGroupPrepend>
                            <CInput type="password" invalid={password !== rePassword} value={rePassword} onChange={(e) => {setRePassword(e.target.value)}} placeholder="Repeat password" autoComplete="new-password" />
                            <CInvalidFeedback>Mật khẩu không khớp</CInvalidFeedback>
                        </CInputGroup>
                        <CButton color="success" onClick={()=> signUpClicked()} block>Tạo tài khoản</CButton>
                        </CForm>
                    </CCardBody>
                    <CCardFooter className="p-4">
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CButton onClick={() => {history.push('/login')}} className="btn-facebook mb-1" block><span>Đăng nhập</span></CButton>
                            </CCol>
                        </CRow>
                    </CCardFooter>
                    </CCard>
                </CCol>
                </CRow>
            </CContainer>
            <Toaster ref={toastRef} title={notifyTitle} content={notifyContent} />
        </div>
    )
}

export default SignUp 