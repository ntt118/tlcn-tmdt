import { useEffect, useState } from 'react'
import { useHistory, useLocation, useParams } from 'react-router'
import ProductApi from 'src/api/product'
import {
  CTabs,
  CNav,
  CCol,
  CRow,
  CNavLink,
  CNavItem,
  CTabContent,
  CTabPane,
  CFormGroup,
  CInputRadio,
  CLabel
  } from '@coreui/react'
import './product-details.scss'
import ImageGallery from 'react-image-gallery';
import { useDispatch, useSelector } from 'react-redux'
import CartApi from 'src/api/cart'
import { getCart } from 'src/stores/actions/cart.action'
import { formatPrice } from 'src/page/utils'

const ProductDetail = () => {

    const { id } = useParams()
    const [product, setProduct] = useState(null)
    const [otherDetails, setOtherDetails] = useState(null)
    const [size, setSize] = useState(null)
    const [sizeList, setSizeList] = useState(null)
    const [color, setColor] = useState(null)
    const [detail, setDetail] = useState(null)
    const [colorList, setColorList] = useState(null)
    const [imgsSlideShow, setImgsSlideShow] = useState(null)
    const [quantity, setQuantity] = useState(1)
    const user = useSelector(state => state.user.user)
    const dispatch = useDispatch()
    const location = useLocation()
    const history = useHistory()

    useEffect(() => {
        if(id) {
          ProductApi.getById({id}).then(rs => {
            if(rs.Value) {
              setProduct(rs.Value)
              setOtherDetails(rs.Value.productotherdetails)
            }
          })
        }
    }, [id])

    useEffect(() => {      
      if (otherDetails?.length) {
        let listSizes = []
        let listColors = []

        otherDetails.forEach(detail => {
          if (!listSizes[detail.sizeId]) {
            listSizes[detail.sizeId]={
              id: detail.sizeId,
              name: detail.sizeName
            }
          }
          if(!listColors[detail.colorId]) {
            listColors[detail.colorId]={
              id: detail.colorId,
              name: detail.colorName
            }
          }
        })

        setSizeList(listSizes)
        setColorList(listColors)
        const selectedSize = listSizes.filter(s =>s.id)[0]
        const selectedColor = listColors.filter(c =>c.id)[0]
        const selectedDetail = otherDetails.filter(detail => detail.colorId === selectedColor.id && detail.sizeId === selectedSize.id)[0]
        setSize(selectedSize)
        setColor(selectedColor)
        setDetail(selectedDetail)
      }
    }, [otherDetails])

    useEffect(() => {
      if(product?.imageproducts.length > 0) {             
        var updateArr = []   
        product.imageproducts.forEach((i) => {
          updateArr.push({original: i.image, thumbnail: i.image, thumbnailWidth: '80px', originalAlt: i.name, thumbnailAlt: i.name})
        })
        setImgsSlideShow(updateArr)
      }
    }, [product])

    const AddToCart = () => {
      if (user) {
        const postData = {userId: user.id, productotherdetailId: detail.otherdetailId, quantity}
        CartApi.create(postData).then(rs => {
          dispatch(getCart(user.id))
        })
      }
      else {
        history.push(`/login?returnUrl=${location.pathname}`)
      }
    }

    const changeDetail = (e, type) => {
      if(type === 'size') {
        setSize(e)
      }
      else{ 
        setColor(e)
      }
    }

    useEffect(() => {
      if(color && size){
        const selectedDetail = otherDetails.filter(detail => detail.colorId === color.id && detail.sizeId === size.id)[0]
        setDetail(selectedDetail)
      }
    }, [color, size])

    return( 
    <CRow id="productDetails">
      <CCol xs="12" lg="6" md="12" >
        { imgsSlideShow && imgsSlideShow.length>0 && 
          <ImageGallery items={imgsSlideShow} showPlayButton={false} autoPlay={true} thumbnailPosition="left" />
        }
      </CCol>
      <CCol xs="12" lg="6" md="12" >
        { product && 
          <div>          
            <div className="product-title">
              <h2> {product.name} </h2>
            </div>
            <div className="product-otherDetails mt-3">
              <h3> Kích cỡ: </h3>
              {sizeList?.length && size && sizeList.map((s) => {
                return <CFormGroup variant="checkbox" className="mx-3" key={s.id}>
                  <CInputRadio onChange={() => changeDetail(s, 'size')} className="form-check-input" id={"size"+s.id} name="sizes" value={s.id} checked={s.id === size?.id}/>
                  <CLabel variant="checkbox" htmlFor={"size"+s.id}>{s.name}</CLabel>
                </CFormGroup>
              })}              
            </div>
            <div className="product-otherDetails mt-3">
              <h3> Màu: </h3>
              {colorList?.length && color && colorList.map((c) => {
                return <CFormGroup variant="checkbox" className="mx-3" key={c.id}>
                  <CInputRadio onChange={() => changeDetail(c, 'color')} className="form-check-input" id={"color"+c.id} name="colors" value={c.id} checked={c.id === color?.id}/>
                  <CLabel variant="checkbox" htmlFor={"color"+c.id}>{c.name}</CLabel>
                </CFormGroup>
              })}              
            </div>
            <div className="product-price">
              <div className="pro-price">
              <h3> Giá: &nbsp;</h3><span>{detail?.price ? formatPrice(detail.price): formatPrice(0)}&#x111;</span>
              </div>
            </div>
            <div className="actions-container">
              <div className="product-quantity">
                <input type="button" value="-" onClick={() => setQuantity(prev => prev === 1 ? 1 : prev - 1)} className="qty-btn btn-left-quantity" />
                <input type="text" id="quantity" name="quantity" value={quantity} onChange={(e) => setQuantity(+e.target.value % 1 !== 0 ? 1 : e.target.value <= 0 ? 1 : +e.target.value)} min="1" className="quantity-selector"/>
                <input type="button" value="+" onClick={() => setQuantity(prev => prev + 1)} className="qty-btn btn-right-quantity"/>
              </div>
              <div className="product-addToCart">
                <button onClick={() => AddToCart()}>Thêm vào giỏ hàng</button>
              </div>              
            </div>
            <div className="product-descriptions">
              <CTabs>
                <CNav variant="tabs">
                  <CNavItem>
                    <CNavLink>
                      Mô tả sản phẩm
                    </CNavLink>
                  </CNavItem>
                </CNav>
                <CTabContent>
                  <CTabPane>
                    {product.description.trim()}
                  </CTabPane>
                  <CTabPane>
                    {product.description.trim()}
                  </CTabPane>
                </CTabContent>
              </CTabs>
            </div>
          </div>          
        }
      </CCol>
    </CRow>
    )
}

export default ProductDetail