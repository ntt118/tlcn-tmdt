import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { useParams, useLocation } from "react-router";
import { useEffect, useState } from "react";
import { getListProduct } from "src/stores/actions/product.action";
import { CCard, CCardBody, CCol, CRow, CPagination, CSelect } from '@coreui/react';
import './products-list.scss'
import CIcon from "@coreui/icons-react";
import { formatPrice } from "src/page/utils";

const ProductsList = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const listProducts = useSelector(state => state.product.products)
    const [filteredList, setFilteredList] = useState([])
    const [showList, setShowList] = useState([])
    const itemsPerPage = 12;
    const [currentPage, setCurrentPage] = useState(1)
    const [totalPages, setTotalPages] = useState(0)
    const [itemSelected, setItemSelected] = useState(null)

    const { id } = useParams(); 
    const location = useLocation();

    useEffect(() => {
        dispatch(getListProduct())
    }, [])  

    useEffect(() => {
        setFilterList()
    }, [id])

    useEffect(() => {
        setFilterList()
    }, [listProducts])  

    const setFilterList = () => {
        if (location.pathname.includes('category')) {
            setFilteredList(listProducts.filter(p => p.categoryId === +id))
        }
        else if (location.pathname.includes('brand')) {
            setFilteredList(listProducts.filter(p => p.brandId === +id))
        }
        else {
            setFilteredList(listProducts)
        }
    }

    useEffect(() => {  
        setShowedList()
    }, [filteredList])

    const setShowedList = () => {
        setShowList(filteredList.slice(0, itemsPerPage))
        const tempTotalPages = Math.floor(filteredList.length/itemsPerPage) + (filteredList.length%itemsPerPage === 0 ? 0 : 1)
        setTotalPages(tempTotalPages)
        setCurrentPage(1)
    }

    useEffect(() => {
        setShowList(filteredList.slice(itemsPerPage*(currentPage-1), itemsPerPage*currentPage))
    }, [currentPage]) 

    const filterSelections = [
        {
            value: 0, name: 'Mặc định'
        },
        {
            value: 1, name: 'Theo tên (A-Z)'
        },
        {
            value: 2, name: 'Theo tên (Z-A)'
        },
        {
            value: 3, name: 'Theo giá (Giảm dần)'
        },
        {
            value: 4, name: 'Theo tên (Tăng dần)'
        }]

    const [selectedFilter, setSelectedFilter] = useState(filterSelections[0].value)

    const sortShowList = () => {
        if(filteredList && filteredList.length > 0){
        switch(+selectedFilter) {
            case 1: {                  
                setFilterList(filteredList.sort((a, b) => {
                    if (a.name > b.name) return -1;
                    if (a.name < b.name) return 1;
                    return 0;
                }));                    
                break;
            }
            case 2: {  
                setFilterList(filteredList.sort((a, b) => {
                    if (a.name < b.name) return -1;
                    if (a.name > b.name) return 1;
                    return 0;
                }));
                break;
            }
            case 3: {
                setFilterList(filteredList.sort((a, b) => {
                    if (a.productotherdetails[0]?.price > b.productotherdetails[0]?.price) return -1;
                    if (a.productotherdetails[0]?.price < b.productotherdetails[0]?.price) return 1;
                    return 0;
                }));
                break;
            }
            case 4: {
                setFilterList(filteredList.sort((a, b) => {
                                if (a.productotherdetails[0]?.price < b.productotherdetails[0]?.price) return -1;
                                if (a.productotherdetails[0]?.price > b.productotherdetails[0]?.price) return 1;
                                return 0;
                            }));
                break;
            }
            default: {
                setFilterList(filteredList.sort((a, b) => {
                                if (a.id < b.id) return -1;
                                if (a.id > b.id) return 1;
                                return 0;
                            }))
            }
        }
        setShowedList()
    }}

    useEffect(() => {
        sortShowList()
    }, [selectedFilter])
    

    const ItemDetail = (props) => {
        return <CCol onMouseEnter={() => {setItemSelected(props.product.id)}}
         onMouseLeave={() => {setItemSelected(null)}}
         key={props.index}
         className="mb-3 card-item" xs="9" sm="6" md="4" xl="3" >
                    <div className="item--container" onClick={() => history.push(`/product/${props.product.id}`)}>
                        <img className="d-inline-block item-image mt-3" src={props.product.imageproducts[0]?.image} alt="slide 1"/>
                        <div className="item--name">
                            {props.product.name}
                        </div>
                    </div>
                    <div className="item--price mb-3">
                        {formatPrice(props.product.productotherdetails[0].price)} &#x111;
                    </div>
                </CCol>
    }

    const NullProductList = () => {
        return <div className="item--null">
            <CIcon name="cil-x-circle" size="2xl"/>
            No Item 
        </div>
    }

    return (            
        <CCard className="border-0">
            <div style={{display:'flex', alignItems:'center'}} className="mt-2 ml-3">
                <CSelect className="ml-3" custom name="select" id="select" style={{width: '150px'}} onChange={(e) => {setSelectedFilter(e.target.value)}}>
                Sắp xếp theo: 
                    {filterSelections.map(selection => {
                        return <option key={selection.value} value={selection.value}>{selection.name}</option>
                    })}
                </CSelect>
            </div>
            <CCardBody style={{paddingTop: '0'}}>                    
            <CRow className="text-center">
                { showList && showList.length > 0 ? showList.map((product, index) => {                                  
                    return <ItemDetail key={index} product={product} index={index} />
                }) : <NullProductList />}
            </CRow>
            { showList.length > 0 && <CPagination
                align="center"
                addListClass="some-class"
                activePage={currentPage}
                pages={totalPages}
                onActivePageChange={setCurrentPage}
            />}
            </CCardBody>
        </CCard>
    )

}

export default ProductsList