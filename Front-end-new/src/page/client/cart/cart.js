import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
    CCol,
    CRow,
    CLink
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import './cart.scss'
import CartApi from 'src/api/cart'
import { getCart } from 'src/stores/actions/cart.action'
import { useHistory, useLocation } from 'react-router'
import { formatPrice } from 'src/page/utils'
import { getListProduct } from 'src/stores/actions/product.action'

const Cart = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const location = useLocation()
    const cart = useSelector(state => state.cart.cart)
    const productsList = useSelector(state => state.product.products)
    const user = useSelector(state => state.user.user)
    
    useEffect(() => {
        dispatch(getListProduct())
    }, [])

    useEffect(() => {
        if(!user?.id){
            history.push(`/login?returnUrl=${location.pathname}`)
        }
    }, [user])

    const changeItemQuantity = (item, action, value) => {
        let tempCart = cart
        tempCart = tempCart.map(p => {
            if (p.id === item.id && p.userId === item.userId && p.productotherdetail.productId === item.productotherdetail.productId) {
                if (action === 'increase') {
                    const maxQuantity = productsList.filter(p => p.id === item.productotherdetail.productId)[0]?.quantity
                    p.quantity = p.quantity + 1 > maxQuantity ? maxQuantity : p.quantity + 1
                }
                else if (action === 'input') {
                    const maxQuantity = productsList.filter(p => p.id === item.productotherdetail.productId)[0]?.quantity
                    p.quantity = +value % 1 !== 0 ? 1 : +value <= 0 ? 1 : +value > maxQuantity ? maxQuantity : +value   
                }
                else {
                    p.quantity = p.quantity === 1 ? 1 : p.quantity -1
                }
            }   
            return p 
        })
        CartApi.update({userId:user.id, Value:tempCart}).then(rs =>{
            dispatch(getCart(user.id))
        })
    }

    const GetTotal = () => {
        let total = 0;
        cart.forEach(item => {
            const product = productsList.filter(p => p.id === item.productotherdetail.productId)[0];
            const detail = product.productotherdetails.filter(d => d.otherdetailId === item.productotherdetailId)[0];
            total += item.quantity*detail?.price
        })
        return formatPrice(total)
    }
    
    const DeleteFromCart = (id) => {
        if (user) {
          CartApi.removeProductFromCart({userId: user.id, productotherdetailId: id}).then(r => {
            dispatch(getCart(user.id))
          })
        }
    }

    return (
        <div id="cart" className="container">
            <CRow>
                <CCol className="heading-page" xs="12" md="12">
                    <h2><CIcon name="cil-cart" alt="Cart" size={'3xl'}/> Giỏ hàng của bạn </h2>
                    <div>Bạn đang có {cart?.length} sản phẩm trong giỏ hàng của bạn.</div>
                </CCol>
                <CCol className="list-items" xs="12" lg="12" md="12">
                    { cart?.length > 0 && productsList.length > 0 && cart.map((item, index) => {
                        const product = productsList.filter(p => p.id === item.productotherdetail.productId)[0];
                        const detail = product?.productotherdetails?.filter(d => d.otherdetailId === item.productotherdetailId)[0];
                        return <div key={index} className="item-detail" style={index<cart?.length-1 ? {borderBottom:'solid 1px #c4c9d0', marginBottom:'10px', paddingBottom: '5px'}: {}}>
                            <img className="d-inline-block item-image" src={productsList.filter(p => p.id === item.productotherdetail.productId)[0]?.imageproducts[0]?.image} alt="slide 1"/>
                            <div style={{display:'block', width:'100%'}}>
                                <CLink to={`/product/${item.productotherdetail.productId}`} style={{fontSize: '16px', fontWeight: 'bold'}}>
                                    {product?.name}
                                </CLink>
                                <div className="item-price">
                                    Giá: {formatPrice(detail?.price)} &#x111;
                                </div>
                                <div className="item-size">
                                    Màu: {detail.colorName} -
                                    Size: {detail.sizeName}
                                </div>
                                <div className="item-total-quantity">
                                    <div className="product-quantity">
                                        <input type="button" value="-" onClick={(e) => changeItemQuantity(item, 'decrease')} className="qty-btn btn-left-quantity" />
                                        <input type="text" id="quantity" onChange={(e) => changeItemQuantity(item, 'input', e.target.value)} value={item.quantity} name="quantity" min="1" className="quantity-selector"/>
                                        <input type="button" value="+"  onClick={(e) => changeItemQuantity(item, 'increase')} className="qty-btn btn-right-quantity"/>
                                    </div>
                                    <div className="total-item-price">
                                        {formatPrice(item.quantity*detail?.price)} &#x111;
                                    </div>
                                </div>      
                            </div>     
                            <div className="remove-item" onClick={() => DeleteFromCart(item.productotherdetailId)}>
                                <CIcon name="cil-trash"></CIcon>    
                            </div>                          
                        </div>
                    }) } 
                </CCol>
            </CRow>
            { cart?.length > 0 && productsList.length > 0 && <CRow style={{display:'block'}}>
                    <div className="total-price text-right px-3" xs="12" md="12">
                        <span>Tổng tiền: </span>{GetTotal()}&#8363;
                    </div>
                    <div className="cart-btn">
                        <div className="btn--payout">
                            <button onClick={() => history.push('/order/checkout')}>Đặt hàng</button>
                        </div>
                    </div>
                </CRow>
            }
        </div>
        
    )
}

export default Cart