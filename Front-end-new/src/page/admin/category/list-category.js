import CIcon from "@coreui/icons-react"
import { CBadge, CCard, CCardBody, CCardHeader, CDataTable, CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react"
import React, { useEffect, useRef, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router"
import CategoryApi from "src/api/category"
import { icons } from "src/assets/icons"
import { getListCategory } from "src/stores/actions/category.action"
import DetailCategory from './detail-category'
import Toaster from "src/components/toaster"

const ListCategory = () => {

  const dispatch = useDispatch()
  const history = useHistory()
  const categories = useSelector(state => state.category.categories)
  const [modalEdit, setModalEdit] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [category, setCategory] = useState({});
  const [idDelete, setIdDelete] = useState(false);
  const editToastRef = useRef()
  const deleteToastRef = useRef()
  const fields = ['#', 'name', 'active', 'action']

  const editCategory = useRef();

  useEffect(() => {
    dispatch(getListCategory())
  }, [])

  const getBadge = active => {
    switch (active) {
      case 1: return 'success'
      default: return 'danger'
    }
  }

  const onOpenModalEdit = (item) => {
    setCategory(item)
    setModalEdit(true)
  }

  const afterSubmit = () => {
    dispatch(getListCategory())
    setModalEdit(false)
    editToastRef.current.showToaster()
    editCategory.current.resetData()
  }

  const deleteCategory = () => {
    CategoryApi.delete({id: idDelete}).then(rs => {
      if(rs.status === "Success"){
        dispatch(getListCategory())
        setModalDelete(false)
        deleteToastRef.current.showToaster()
      }
    })
  }

  return (
    <>
      <CCard className="category">
        <CCardHeader className="flex-between">
          <h3 className="m-0">Danh sách phân loại</h3>
          <CButton color="success" onClick={() => history.push('/category/create')}>Thêm phân loại</CButton>
        </CCardHeader>
        <CCardBody>
          <CDataTable
            items={categories}
            fields={fields}
            striped
            itemsPerPage={5}
            pagination
            scopedSlots={{
              '#':
                (item, index) => (
                  <td style={{ width: '50px' }}>
                    {index + 1}
                  </td>
                ),
              'active': (item, index) => (
                <td style={{ width: '100px' }}>
                  <CBadge color={getBadge(item.isActive)}>
                    {item.isActive === 1 ? 'Active' : 'No Active'}
                  </CBadge>
                </td>
              ),
              'action': (item, index) => (
                <td style={{ width: '100px', textAlign: 'center' }}>
                  {/* <CIcon content={icons.cilPlus} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => history.push('/category/create')} /> */}
                  <CIcon content={icons.cilEyedropper} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => onOpenModalEdit(item)} />
                  <CIcon content={icons.cilDelete} style={{ cursor: item.isActive ? 'pointer' : 'not-allowed', color: 'red' }} size="xl" onClick={() => { if(!item.isActive) return; setModalDelete(true); setIdDelete(item.id)}} />
                </td>
              )

            }}
          />
        </CCardBody>
      </CCard>
      <CModal
        show={modalEdit}
        onClose={() => setModalEdit(!modalEdit)}
      >
        <CModalHeader closeButton>
          <CModalTitle>Chỉnh sửa phân loại</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <DetailCategory ref={editCategory} isEdit={true} category={category} afterSubmit={afterSubmit} />
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" disabled={!category.isActive} onClick={() => editCategory.current.handleSubmit()} >Lưu</CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => {setModalEdit(false); editCategory.current.resetData();}}
          >Hủy</CButton>
        </CModalFooter>
      </CModal>
      <CModal
        show={modalDelete}
        onClose={() => setModalDelete(!modalDelete)}
      >
        <CModalHeader closeButton>
          <CModalTitle>Xóa phân loại</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <span>Bạn có muốn xóa phân loại này không?</span>
        </CModalBody>
        <CModalFooter>
          <CButton color="primary" onClick={() => deleteCategory()} >Xóa</CButton>{' '}
          <CButton
            color="secondary"
            onClick={() => setModalDelete(false)}
          >Hủy</CButton>
        </CModalFooter>
      </CModal>
      <Toaster ref={editToastRef} title="Chỉnh sửa phân loại" content="Thành công!!!" />
      <Toaster ref={deleteToastRef} title="Xóa phân loại" content="Thành công!!!" />
    </>
  )
}
export default ListCategory