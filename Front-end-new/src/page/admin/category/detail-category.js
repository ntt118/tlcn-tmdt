import { useEffect } from 'react';
import {  CCol, CFormGroup, CInput, CInvalidFeedback, CLabel, CRow } from '@coreui/react'
import { forwardRef, useImperativeHandle, useState } from 'react'
import { useHistory } from 'react-router'
import CategoryApi from 'src/api/category'

const DetailCategory = forwardRef((props, ref) => {
        useImperativeHandle(ref, () => ({
            resetData() {
                setValidated(false);
                setNameCategory(null);
            },
            handleSubmit() {
                if(!nameCategory){
                    setValidated(true)
                    return
                }
                if(props.isEdit){
                    var data = {
                        id: props.category.id,
                        Value: {
                            name: nameCategory
                        }
                    }
                    CategoryApi.update(data).then(rs => {
                        if(rs.status === 'Success'){
                            props.afterSubmit()
                        }
                    })
                } else {
                    CategoryApi.create({name: nameCategory}).then(rs => {
                        if(rs.status === 'Success'){
                            history.push('/category/list')
                            this.resetData()
                        }
                    })
                }
                
            }
        }))

    const history = useHistory()
    const [validated, setValidated] = useState(false)
    const [nameCategory, setNameCategory] = useState(null)

    useEffect(() => {
        if(props.isEdit && props.category){
            setNameCategory(props.category.name)
        }
    }, [props.category])

    const onChangeName = (e) => {
        if(e.target.value){
            setValidated(false)
        }
        else{
            setValidated(true)
        }
        setNameCategory(e.target.value)
    }

    return (
        <div className="category" >
            <CRow>
                <CCol xs="12">
                        <CLabel htmlFor="nameCategory">Tên phân loại</CLabel>
                        <CInput id="nameCategory" placeholder="Nhập tên phân loại" value={nameCategory} onChange={(e) => onChangeName(e)} invalid={validated} required />
                        <CInvalidFeedback className="help-block">
                            Xin nhập tên phân loại
                        </CInvalidFeedback>
                </CCol>
            </CRow>
        </div>
    )
})
export default DetailCategory