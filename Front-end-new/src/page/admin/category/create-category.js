import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from "@coreui/react"
import React, { useRef } from "react"
import DetailCategory from "./detail-category"

const CreateCategory = () => {

    const editCategory = useRef();

    return (
        <CCard className="category">
            <CCardHeader>
                <h3 className="m-0">Thêm phân loại</h3>
            </CCardHeader>
            <CCardBody>
                <DetailCategory ref={editCategory} isEdit={false} />
            </CCardBody>
            <CCardFooter>
                <CRow>
                    <CCol xs={12} className="flex-end">
                        <CButton color="primary" onClick={() => editCategory.current.handleSubmit()}>
                            Thêm
                        </CButton>
                    </CCol>
                </CRow>
            </CCardFooter>
        </CCard>
    )
}
export default CreateCategory