import CIcon from "@coreui/icons-react";
import { CBadge, CButton, CCard, CCardBody, CCardHeader, CDataTable, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import UserApi from "src/api/user.api";
import { icons } from "src/assets/icons";
import Toaster from "src/components/toaster";
import { getListUser } from "src/stores/actions/user.action";
import DetailCustomer from './detail-customer'

const ListCustomer = () => {

    const dispatch = useDispatch()
  const history = useHistory()
  const users = useSelector(state => state.user.users)
  const [modalEdit, setModalEdit] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [user, setUser] = useState({});
  const [usernameDelete, setUsernameDelete] = useState(false);
  const [showToastEdit, setShowToastEdit] = useState(false);
  const [showToastDelete, setShowToastDelete] = useState(false);
  const fields = ['#', 'name', 'username', 'email', 'phoneNumber', 'address', 'active', 'action']

  const editCustomer = useRef();

  useEffect(() => {
    dispatch(getListUser())
  }, [])

  const getBadge = active => {
    switch (active) {
      case 1: return 'success'
      default: return 'danger'
    }
  }

  const onOpenModalEdit = (item) => {
    setUser(item)
    setModalEdit(true)
  }

  const afterSubmit = () => {
    dispatch(getListUser())
    setModalEdit(false)
    setShowToastEdit(true)
    editCustomer.current.resetData()
  }

  const deleteCategory = () => {
    UserApi.delete({username: usernameDelete}).then(rs => {
      if(rs.status === "Success"){
        dispatch(getListUser())
        setModalDelete(false)
        setShowToastDelete(true)
      }
    })
  }

    return (
        <>
            <CCard className="user">
                <CCardHeader className="flex-between">
                    <h3 className="m-0">Danh sách khách hàng</h3>
                    {/* <CButton color="success" onClick={() => history.push('/user/create')}>Thêm khách hàng</CButton> */}
                </CCardHeader>
                <CCardBody>
                    <CDataTable
                        items={users}
                        fields={fields}
                        striped
                        itemsPerPage={5}
                        pagination
                        scopedSlots={{
                            '#':
                                (item, index) => (
                                    <td style={{ width: '50px' }}>
                                        {index + 1}
                                    </td>
                                ),
                            'active': (item, index) => (
                                <td style={{ width: '100px' }}>
                                    <CBadge color={getBadge(item.isActive)}>
                                        {item.isActive === 1 ? 'Active' : 'No Active'}
                                    </CBadge>
                                </td>
                            ),
                            'action': (item, index) => (
                                <td style={{ width: '100px', textAlign: 'center' }}>
                                    {/* <CIcon content={icons.cilPlus} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => history.push('/user/create')} /> */}
                                    <CIcon content={icons.cilEyedropper} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => onOpenModalEdit(item)} />
                                    <CIcon content={icons.cilDelete} style={{ cursor: item.isActive ? 'pointer' : 'not-allowed', color: 'red' }} size="xl" onClick={() => { if (!item.isActive) return; setModalDelete(true); setUsernameDelete(item.username) }} />
                                </td>
                            )

                        }}
                    />
                </CCardBody>
            </CCard>
            <CModal
                size="lg"
                show={modalEdit}
                onClose={() => setModalEdit(!modalEdit)}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Thông tin khách hàng</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <DetailCustomer ref={editCustomer} customer={user} />
                </CModalBody>
                <CModalFooter>
                    {/* <CButton color="primary" disabled={!user.isActive} onClick={() => editCustomer.current.handleSubmit()} >Lưu</CButton>{' '} */}
                    <CButton
                        color="secondary"
                        onClick={() => { setModalEdit(false); editCustomer.current.resetData(); }}
                    >Thoát</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalDelete}
                onClose={() => setModalDelete(!modalDelete)}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Xóa khách hàng</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <span>Bạn có muốn xóa khách hàng này không?</span>
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" onClick={() => deleteCategory()} >Xóa</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalDelete(false)}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <Toaster title="Chỉnh sửa khách hàng" content="Thành công!!!" isShowToast={showToastEdit} />
            <Toaster title="Xóa khách hàng" content="Thành công!!!" isShowToast={showToastDelete} />
        </>
    )
}
export default ListCustomer