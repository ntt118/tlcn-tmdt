import { CCol, CInput, CLabel, CRow } from "@coreui/react";
import { forwardRef, useEffect, useImperativeHandle, useState } from "react";

const DetailCustomer  = forwardRef((props, ref) => {
    useImperativeHandle(ref, () => ({
        resetData() {
            
        },
        handleSubmit() {

        }
    }))

    const [ customer , setCustomer] = useState({})

    useEffect(() => {
        if(props.customer){
            setCustomer(props.customer)
        }
    }, [props.customer])

    const getGender = (e) => {
        if(!e){
            return 'Chưa có'
        }else if(e && e === 0){
            return 'Nữ'
        }
        return 'Nam'
    }

    return (
        <div>
            <CRow>
                <CCol span={6}>
                    <CLabel htmlFor="name">Tên khách hàng</CLabel>
                    <CInput id="name" disabled={true} value={customer.name} />
                </CCol>
                <CCol span={6}>
                    <CLabel htmlFor="name">Username</CLabel>
                    <CInput id="name" disabled={true} value={customer.username} />
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol span={6}>
                    <CLabel htmlFor="name">Địa chỉ email</CLabel>
                    <CInput id="name" disabled={true} value={customer.email} />
                </CCol>
                <CCol span={6}>
                    <CLabel htmlFor="name">Số điện thoại</CLabel>
                    <CInput id="name" disabled={true} value={customer.phoneNumber} />
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol span={6}>
                    <CLabel htmlFor="name">Giới tính</CLabel>
                    <CInput id="name" disabled={true} value={getGender(customer.gender)} />
                </CCol>
                <CCol span={6}>
                    <CLabel htmlFor="name">Ngày sinh</CLabel>
                    <CInput id="name" disabled={true} value={customer.dayOfBirth ?? "Không có"} />
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol span={12}>
                    <CLabel htmlFor="name">Địa chỉ</CLabel>
                    <CInput rows={3} id="name" disabled={true} value={customer.address ?? "Không có"} />
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol span={6}>
                    <CLabel htmlFor="name">Trạng thái</CLabel>
                    <CInput id="name" disabled={true} value={customer.isActive ? "Còn hoạt động" : "Dừng hoạt động"} />
                </CCol>
                <CCol span={6}>
                    <CLabel htmlFor="name">Phân quyền</CLabel>
                    <CInput id="name" disabled={true} value={"Khách hàng"} />
                </CCol>
            </CRow>
        </div>
    )
})
export default DetailCustomer