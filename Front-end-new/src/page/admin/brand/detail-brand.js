import { useEffect } from 'react';
import {  CCol, CInput, CInvalidFeedback, CLabel, CRow } from '@coreui/react'
import { forwardRef, useImperativeHandle, useState } from 'react'
import { useHistory } from 'react-router'
import BrandApi from 'src/api/brand';

const DetailBrand = forwardRef((props, ref) => {
        useImperativeHandle(ref, () => ({
            resetData() {
                setValidated(false);
                setNameBrand(null);
            },
            handleSubmit() {
                if(!nameBrand){
                    setValidated(true)
                    return
                }
                if(props.isEdit){
                    var data = {
                        id: props.brand.id,
                        Value: {
                            name: nameBrand
                        }
                    }
                    BrandApi.update(data).then(rs => {
                        if(rs.status === 'Success'){
                            props.afterSubmit()
                        }
                    })
                } else {
                    BrandApi.create({name: nameBrand}).then(rs => {
                        if(rs.status === 'Success'){
                            history.push('/brand/list')
                            this.resetData()
                        }
                    })
                }
                
            }
    }))

    const history = useHistory()
    const [validated, setValidated] = useState(false)
    const [nameBrand, setNameBrand] = useState(null)

    useEffect(() => {
        if(props.isEdit && props.brand){
            setNameBrand(props.brand.name)
        }
    }, [props.brand])

    const onChangeName = (e) => {
        if(e.target.value){
            setValidated(false)
        }
        else{
            setValidated(true)
        }
        setNameBrand(e.target.value)
    }

    return (
        <div className="" >
            <CRow>
                <CCol xs="12">
                    <CLabel htmlFor="nameBrand">Tên thương hiệu</CLabel>
                    <CInput id="nameBrand" placeholder="Nhập tên thương hiệu" value={nameBrand} onChange={(e) => onChangeName(e)} invalid={validated} required />
                    <CInvalidFeedback className="help-block">
                        Xin nhập tên thương hiệu
                    </CInvalidFeedback>
                </CCol>
            </CRow>
        </div>
    )
})
export default DetailBrand