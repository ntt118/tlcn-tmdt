import CIcon from "@coreui/icons-react";
import { CBadge, CButton, CCard, CCardBody, CCardHeader, CDataTable, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import BrandApi from "src/api/brand";
import { icons } from "src/assets/icons";
import Toaster from "src/components/toaster";
import { getListBrand } from "src/stores/actions/brand.action";
import DetailBrand from "./detail-brand";

const ListBrand = () => {

    const dispatch = useDispatch()
    const history = useHistory()
    const brands = useSelector(state => state.brand.brands)
    const [modalEdit, setModalEdit] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [brand, setBrand] = useState(false);
    const [idDelete, setIdDelete] = useState(false);
    const editToastRef = useRef()
    const deleteToastRef = useRef()
    const fields = ['#', 'name', 'active', 'action']

    const editBrand = useRef();

    useEffect(() => {
        dispatch(getListBrand())
    }, [])

    const getBadge = active => {
        switch (active) {
            case 1: return 'success'
            default: return 'danger'
        }
    }

    const onOpenModalEdit = (item) => {
        setBrand(item)
        setModalEdit(true)
    }

    const afterSubmit = () => {
        dispatch(getListBrand())
        setModalEdit(false)
        editToastRef.current.showToaster()
        editBrand.current.resetData()
    }

    const deleteBrand = () => {
        BrandApi.delete({ id: idDelete }).then(rs => {
            if (rs.status === "Success") {
                dispatch(getListBrand())
                setModalDelete(false)
                deleteToastRef.current.showToaster()
            }
        })
    }

    return (
        <>
            <CCard className="category">
                <CCardHeader className="flex-between">
                    <h3 className="m-0">Danh sách thương hiệu</h3>
                    <CButton color="success" onClick={() => history.push('/brand/create')}>Thêm thương hiệu</CButton>
                </CCardHeader>
                <CCardBody>
                    <CDataTable
                        items={brands}
                        fields={fields}
                        striped
                        itemsPerPage={5}
                        pagination
                        scopedSlots={{
                            '#':
                                (item, index) => (
                                    <td style={{ width: '50px' }}>
                                        {index + 1}
                                    </td>
                                ),
                            'active': (item, index) => (
                                <td style={{ width: '100px' }}>
                                    <CBadge color={getBadge(item.isActive)}>
                                        {item.isActive === 1 ? 'Active' : 'No Active'}
                                    </CBadge>
                                </td>
                            ),
                            'action': (item, index) => (
                                <td style={{ width: '100px', textAlign: 'center' }}>
                                    {/* <CIcon content={icons.cilPlus} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => history.push('/category/create')} /> */}
                                    <CIcon content={icons.cilEyedropper} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => onOpenModalEdit(item)} />
                                    <CIcon content={icons.cilDelete} style={{ cursor: item.isActive ? 'pointer' : 'not-allowed', color: 'red' }} size="xl" onClick={() => { if(!item.isActive) return; setModalDelete(true); setIdDelete(item.id) }} />
                                </td>
                            )

                        }}
                    />
                </CCardBody>
            </CCard>
            <CModal
                show={modalEdit}
                onClose={() => setModalEdit(!modalEdit)}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Chỉnh sửa thương hiệu</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <DetailBrand ref={editBrand} isEdit={true} brand={brand} afterSubmit={afterSubmit} />
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" disabled={!brand.isActive} onClick={() => editBrand.current.handleSubmit()} >Lưu</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => { setModalEdit(false); editBrand.current.resetData(); }}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalDelete}
                onClose={() => setModalDelete(false)}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Xóa thương hiệu</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <span>Bạn có muốn xóa thương hiệu này không?</span>
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" onClick={() => deleteBrand()} >Xóa</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalDelete(false)}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <Toaster ref={editToastRef} title="Chỉnh sửa thương hiệu" content="Thành công!!!"/>
            <Toaster ref={deleteToastRef} title="Xóa thương hiệu" content="Thành công!!!" />
        </>
    )
}
export default ListBrand;