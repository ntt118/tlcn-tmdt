import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from "@coreui/react";
import { useRef } from "react";
import DetailBrand from "./detail-brand";

const CreateBrand = () => {

    const editBrand = useRef();

    return (
        <CCard className="category">
            <CCardHeader>
                <h3 className="m-0">Thêm thương hiệu</h3>
            </CCardHeader>
            <CCardBody>
                <DetailBrand ref={editBrand} isEdit={false} />
            </CCardBody>
            <CCardFooter>
                <CRow>
                    <CCol xs={12} className="flex-end">
                        <CButton color="primary" onClick={() => editBrand.current.handleSubmit()}>
                            Thêm
                        </CButton>
                    </CCol>
                </CRow>
            </CCardFooter>
        </CCard>
    )
}
export default CreateBrand;