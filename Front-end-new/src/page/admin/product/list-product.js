import CIcon from "@coreui/icons-react";
import { CBadge, CButton, CCard, CCardBody, CCardHeader, CDataTable, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import ProductApi from "src/api/product";
import { icons } from "src/assets/icons";
import Toaster from "src/components/toaster";
import { getListProduct } from "src/stores/actions/product.action";
import DetailProduct from "../product/detail-product";

const ListProduct = () => {

    const dispatch = useDispatch()
    const history = useHistory()
    const categories = useSelector(state => state.product.products)
    const [modalEdit, setModalEdit] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [product, setProduct] = useState({});
    const [idDelete, setIdDelete] = useState(false);
    const editToastRef = useRef()
    const deleteToastRef = useRef()
    const fields = ['#', 'image', 'name', 'description', 'quantity', 'rateAvg', 'active', 'action']

    const editProduct = useRef();

    useEffect(() => {
        dispatch(getListProduct())
    }, [])

    const getBadge = active => {
        switch (active) {
            case 1: return 'success'
            default: return 'danger'
        }
    }

    const onOpenModalEdit = (item) => {
        setProduct(item)
        setModalEdit(true)
    }

    const afterSubmit = () => {
        dispatch(getListProduct())
        setModalEdit(false)
        editToastRef.current.showToaster()
        editProduct.current.resetData()
    }

    const deleteProduct = () => {
        ProductApi.delete({ id: idDelete }).then(rs => {
            if (rs.status === "Success") {
                dispatch(getListProduct())
                setModalDelete(false)
                deleteToastRef.current.showToaster()
            }
        })
    }

    return (
        <>
            <CCard className="product">
                <CCardHeader className="flex-between">
                    <h3 className="m-0">Danh sách sản phẩm</h3>
                    <CButton color="success" onClick={() => history.push('/product/create')}>Thêm sản phẩm</CButton>
                </CCardHeader>
                <CCardBody>
                    <CDataTable
                        items={categories}
                        fields={fields}
                        striped
                        itemsPerPage={5}
                        pagination
                        scopedSlots={{
                            '#':
                                (item, index) => (
                                    <td style={{ width: '50px' }}>
                                        {index + 1}
                                    </td>
                                ),
                            'image': (item) => (
                                <td>
                                    <img width="50" src={item.imageproducts[0]?.image} alt={item.name} />
                                </td>
                            ),
                            'rateAvg': (item, index) => (
                                <td className="flex-ali-center">
                                    {item.rateAvg ?? "Chưa có"}
                                    {item.rateAvg && <CIcon content={icons.cilStar} style={{ color: '#ccc', marginLeft: '5px' }} size="xl" />}
                                </td>
                            ),
                            'active': (item, index) => (
                                <td style={{ width: '100px' }}>
                                    <CBadge color={getBadge(item.isActive)}>
                                        {item.isActive === 1 ? 'Active' : 'No Active'}
                                    </CBadge>
                                </td>
                            ),
                            'action': (item, index) => (
                                <td style={{ width: '100px', textAlign: 'center' }}>
                                    {/* <CIcon content={icons.cilPlus} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => history.push('/product/create')} /> */}
                                    <CIcon content={icons.cilEyedropper} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => onOpenModalEdit(item)} />
                                    <CIcon content={icons.cilDelete} style={{ cursor: item.isActive ? 'pointer' : 'not-allowed', color: 'red' }} size="xl" onClick={() => { if(!item.isActive) return; setModalDelete(true); setIdDelete(item.id) }} />
                                </td>
                            )

                        }}
                    />
                </CCardBody>
            </CCard>
            <CModal
                size='lg'
                show={modalEdit}
                onClose={() => {setModalEdit(!modalEdit); editProduct.current.resetData();}}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Chỉnh sửa sản phẩm</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <DetailProduct ref={editProduct} isEdit={true} product={product} afterSubmit={afterSubmit} />
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" disabled={!product.isActive} onClick={() => editProduct.current.handleSubmit()} >Lưu</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => { setModalEdit(false); editProduct.current.resetData(); }}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalDelete}
                onClose={() => setModalDelete(!modalDelete)}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Xóa sản phẩm</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <span>Bạn có muốn xóa sản phẩm này không?</span>
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" onClick={() => deleteProduct()} >Xóa</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalDelete(false)}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <Toaster ref={editToastRef} title="Chỉnh sửa sản phẩm" content="Thành công!!!" />
            <Toaster ref={deleteToastRef} title="Xóa sản phẩm" content="Thành công!!!" />
        </>
    )
}
export default ListProduct;