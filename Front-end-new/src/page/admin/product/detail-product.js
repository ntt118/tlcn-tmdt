import { CButton, CCol, CFormGroup, CInput, CInputCheckbox, CInvalidFeedback, CLabel, CRow, CSelect } from "@coreui/react";
import { forwardRef, useEffect, useImperativeHandle, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import ProductApi from "src/api/product";
import UploadImage from "src/components/upload-image";
import { getListBrand } from "src/stores/actions/brand.action";
import { getListCategory } from "src/stores/actions/category.action";
import { getListColor, getListSize } from "src/stores/actions/otherdetail.action";
import './styles.scss'

const DetailProduct = forwardRef((props, ref) => {
    useImperativeHandle(ref, () => ({
        resetData() {
            setValidatedData({});
            setData(orgData);
            setImageList([])
            setSelectColors([])
            setSelectSizes([])
            setListOtherDetail([])
            createValue()
        },
        handleSubmit() {
            if (checkValidateOtherDetail() || checkValidateForm()) return
            const otherdetails = listOtherDetail.map(rs => ({
                sizeId: rs.sizeId,
                colorId: rs.colorId,
                quantity: rs.quantity,
                price: rs.price,
            }))
            if (props.isEdit) {
                var _data = data
                var image = uploadImageRef.current.getData()
                _data.imageproducts = image.map(r => ({ image: r }))
                _data.otherdetails = otherdetails
                var newdata = {
                    id: data.id,
                    Value: _data
                }
                delete newdata.Value.id
                ProductApi.update(newdata).then(rs => {
                    if (rs.status === 'Success') {
                        props.afterSubmit()
                    }
                })
            } else {
                var _data = data
                var image = uploadImageRef.current.getData()
                _data.imageproducts = image.map(r => ({ image: r }))
                _data.otherdetails = otherdetails
                ProductApi.create(_data).then(rs => {
                    if (rs.status === 'Success') {
                        history.push('/product/list')
                        this.resetData()
                    }
                })
            }

        }
    }))

    const orgData = {
        name: null,
        description: null,
        price: null,
        quantity: null,
        status: 1,
        isActive: 1,
        rateAvg: null,
        categoryId: null,
        brandId: null
    }

    const dispatch = useDispatch()
    const history = useHistory()
    const brands = useSelector(state => state.brand.brands)
    const categories = useSelector(state => state.category.categories)
    const sizes = useSelector(state => state.otherDetail.sizes)
    const colors = useSelector(state => state.otherDetail.colors)
    const [selectSizeAll, setSelectSizeAll] = useState(sizes)
    const [selectColorAll, setSelectColorAll] = useState(colors)
    const [selectSizes, setSelectSizes] = useState([])
    const [selectColors, setSelectColors] = useState([])
    const [listOtherDetail, setListOtherDetail] = useState([])
    const [data, setData] = useState(orgData)
    const [validatedData, setValidatedData] = useState({})
    const [imageList, setImageList] = useState([])
    const [flag, setFlag] = useState(true)

    const uploadImageRef = useRef()

    useEffect(() => {
        createValue()
    }, [sizes, colors])

    const createValue = () => {
        const sizeAll = sizes.map(rs => {
            return {
                ...rs,
                checked: false
            }
        })
        const colorAll = colors.map(rs => {
            return {
                ...rs,
                checked: false
            }
        })
        setSelectSizeAll(sizeAll);
        setSelectColorAll(colorAll);
    }

    useEffect(() => {
        if (props.isEdit && props.product) {
            setData(props.product)
            if (props.product.imageproducts && props.product.imageproducts.length > 0) {
                setImageList(props.product.imageproducts.map(r => r.image))
            }
            if(props.product.productotherdetails && props.product.productotherdetails.length > 0){
                props.product.productotherdetails.forEach(otherdetail => {
                    const size = sizes.filter(rs => rs.id === otherdetail.sizeId)
                    const color = colors.filter(rs => rs.id === otherdetail.colorId)
                    if(size && size.length){
                        setSelectSizeAll(selectSizeAll.map(rs => {
                            if(rs.id === size[0].id){
                                rs.checked = true
                            }
                            return rs;
                        }))
                    }
                    if(color && color.length) {
                        setSelectColorAll(selectColorAll.map(rs => {
                            if(rs.id === color[0].id){
                                rs.checked = true
                            }
                            return rs;
                        }))
                    }
                })
                const selectSizeId = props.product.productotherdetails.map(rs => rs.sizeId)
                const selectColorId = props.product.productotherdetails.map(rs => rs.colorId)
                setSelectSizes(sizes.filter(rs => selectSizeId.includes(rs.id)))
                setSelectColors(colors.filter(rs => selectColorId.includes(rs.id)))
            }
        }
    }, [props.product])

    useEffect(() => {
        if (props.isEdit && props.product && flag) {
            if(props.product.productotherdetails && props.product.productotherdetails.length > 0) {
                const newList = listOtherDetail.map(otherdetail => {
                    const item = props.product.productotherdetails.filter(rs => rs.sizeId === otherdetail.sizeId && rs.colorId === otherdetail.colorId)
                    if (item && item.length) {
                        return {
                            ...otherdetail,
                            quantity: item[0].quantity,
                            price: item[0].price,
                            validQuantity: false,
                            validPrice: false
                        }
                    }
                })
                setListOtherDetail(newList);
                setFlag(false)
            }
        }
    }, [listOtherDetail])

    useEffect(() => {
        dispatch(getListCategory())
        dispatch(getListBrand())
        dispatch(getListSize())
        dispatch(getListColor())
    }, [])

    useEffect(() => {
        var ob = {
            categoryId: null,
            brandId: null
        }
        if (categories && categories.length > 0) {
            ob.categoryId = categories[0].id
        }
        if (brands && brands.length > 0) {
            ob.brandId = brands[0].id
        }
        setData({ ...data, ...ob })
    }, [categories, brands])

    const onChangeField = (e, field) => {

        if (e.target.value) {
            setValidatedData({ ...validatedData, [field]: false })
        }
        else {
            setValidatedData({ ...validatedData, [field]: true })
        }
        setData({ ...data, [field]: e.target.value })
    }

    const checkValidateForm = () => {
        var flag = false;
        var valid = {}
        for (const [key, value] of Object.entries(data)) {
            if (!value && key !== "rateAvg" && key !== "quantity" && key !== "price") {
                valid[key] = true
                flag = true
            }
        }
        setValidatedData(valid)
        return flag
    }

    const checkValidateOtherDetail = (id = null) => {
        var flag = false;
        var data = []
        listOtherDetail.forEach(rs => {
            const result = rs;
            if (id === rs.id) {
                result.validQuantity = false;
                result.validPrice = false;
            } else if (!rs.quantity || !rs.price) {
                if (!rs.quantity) result.validQuantity = true;
                if (!rs.price) result.validPrice = true;
                flag = true;
            } else {
                result.validQuantity = false;
                result.validPrice = false;
            }
            data.push(result);
        })
        setListOtherDetail(data);
        return flag;
    }

    useEffect(() => {
        submitOtherDetail()
    }, [selectSizes, selectColors])

    const changeCheckBoxSize = (e, id) => {
        const size = sizes.filter(r => r.id === +e.target.value)[0]
        if (e.target.checked) {
            setSelectSizes([...selectSizes, size])
            setSelectSizeAll(selectSizeAll.map(rs => {
                if(rs.id === id){
                    rs.checked = true
                }
                return rs
            }))
        } else {
            setSelectSizes(selectSizes.filter(rs => size.id !== rs.id))
            setSelectSizeAll(selectSizeAll.map(rs => {
                if(rs.id === id){
                    rs.checked = false
                }
                return rs
            }))
        }
        submitOtherDetail()
    }

    const changeCheckColorSize = (e, id) => {
        const color = colors.filter(r => r.id === +e.target.value)[0]
        if (e.target.checked) {
            setSelectColors([...selectColors, color])
            setSelectColorAll(selectColorAll.map(rs => {
                if(rs.id === id){
                    rs.checked = true
                }
                return rs
            }))
        } else {
            setSelectColors(selectColors.filter(rs => color.id !== rs.id))
            setSelectColorAll(selectColorAll.map(rs => {
                if(rs.id === id){
                    rs.checked = false
                }
                return rs
            }))
        }
        submitOtherDetail()
    }

    const onChangeOtherdetail = (e, id, key) => {
        checkValidateOtherDetail(id)
        const data = listOtherDetail.map(rs => {
            if (rs.id === id) {
                rs[key] = +e.target.value
            }
            return rs
        })
        setListOtherDetail(data);
    }

    const submitOtherDetail = () => {
        const list = [];
        let index = 0;
        selectSizes.forEach(size => {
            selectColors.forEach(color => {
                const isExist = listOtherDetail.filter(rs => rs.sizeId === size.id && rs.colorId === color.id)
                if (isExist && isExist.length) {
                    isExist[0].id = index;
                    list.push(isExist[0]);
                    index++;
                } else {
                    const item = {
                        id: index,
                        sizeId: size.id,
                        colorId: color.id,
                        sizeName: size.name,
                        colorName: color.name,
                        quantity: null,
                        validQuantity: false,
                        validPrice: false
                    }
                    list.push(item);
                    index++;
                }
            })
        })
        setListOtherDetail(list);
    }

    return (
        <div className="product" >
            <CRow className="">
                <CCol xs={props.isEdit ? '3' : '2'}>
                    <UploadImage ref={uploadImageRef} imageList={imageList} />
                </CCol>
                <CCol xs={props.isEdit ? '9' : '10'}>
                    <CCol xs="12" className="p-0">
                        <CLabel htmlFor="name">Tên sản phẩm</CLabel>
                        <CInput id="name" placeholder="Nhập tên sản phẩm" value={data.name} onChange={(e) => onChangeField(e, 'name')} invalid={validatedData.name} required />
                        <CInvalidFeedback className="help-block">
                            Xin nhập tên sản phẩm
                        </CInvalidFeedback>
                    </CCol>
                    <CCol xs="12" className="mt-16 p-0">
                        <CLabel htmlFor="description">Mô tả</CLabel>
                        <CInput id="description" placeholder="Nhập mô tả sản phẩm" value={data.description} onChange={(e) => onChangeField(e, 'description')} invalid={validatedData.description} required />
                        <CInvalidFeedback className="help-block">
                            Xin nhập mô tả sản phẩm
                        </CInvalidFeedback>
                    </CCol>
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol xs="6">
                    <CLabel htmlFor="category">Phân loại</CLabel>
                    <CSelect custom value={data.categoryId} onChange={(e) => onChangeField(e, 'categoryId')} name="category" id="category">
                        {categories && categories.map((rs, index) => {
                            return <option key={index} value={rs.id}>{rs.name}</option>
                        })}
                    </CSelect>
                </CCol>
                <CCol xs="6">
                    <CLabel htmlFor="brand">Thương hiệu</CLabel>
                    <CSelect custom value={data.brandId} onChange={(e) => onChangeField(e, 'brandId')} name="brand" id="brand">
                        {brands && brands.map((rs, index) => {
                            return <option key={index} value={rs.id}>{rs.name}</option>
                        })}
                    </CSelect>
                </CCol>
            </CRow>
            {/* <CRow className="mt-16">
                <CCol xs="12">
                    <CLabel htmlFor="price">Giá sản phẩm</CLabel>
                    <CInput id="price" type="number" placeholder="Nhập giá sản phẩm" value={data.price} onChange={(e) => onChangeField(e, 'price')} invalid={validatedData.price} required />
                    <CInvalidFeedback className="help-block">
                        Xin nhập giá sản phẩm hợp lệ
                    </CInvalidFeedback>
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol xs="12">
                    <CLabel htmlFor="quantity">Số lượng sản phẩm</CLabel>
                    <CInput id="quantity" type="number" placeholder="Nhập số lượng sản phẩm" value={data.quantity} onChange={(e) => onChangeField(e, 'quantity')} invalid={validatedData.quantity} required />
                    <CInvalidFeedback className="help-block">
                        Xin nhập số lượng sản phẩm hợp lệ
                    </CInvalidFeedback>
                </CCol>
            </CRow> */}
            <div className="box">
                <CRow>
                    <CCol xs="12">
                        <CLabel htmlFor="quantity">Chi tiết khác</CLabel>
                    </CCol>
                </CRow>
                <CRow className="mt-16">
                    <CCol xs="12">
                        <CLabel htmlFor="quantity">Kích cỡ</CLabel>
                    </CCol>
                </CRow>
                <CRow>
                    {selectSizeAll && selectSizeAll.filter(r => r.status).map((item) => {
                        return <CCol xs={3} key={item.id}>
                            <CFormGroup variant="custom-checkbox" inline>
                                <CInputCheckbox
                                    custom
                                    checked={item.checked}
                                    id={item.id + item.name}
                                    name={item.name}
                                    value={item.id}
                                    onClick={(e) => changeCheckBoxSize(e, item.id)}
                                />
                                <CLabel variant="custom-checkbox" htmlFor={item.id + item.name}>{item.name}</CLabel>
                            </CFormGroup>
                        </CCol>
                    })}
                </CRow>
                <CRow className="mt-16">
                    <CCol xs="12">
                        <CLabel htmlFor="quantity">Màu sắc</CLabel>
                    </CCol>
                </CRow>
                <CRow>
                    {selectColorAll && selectColorAll.filter(r => r.status).map((item) => {
                        return <CCol xs={3} key={item.id}>
                            <CFormGroup variant="custom-checkbox" inline>
                                <CInputCheckbox
                                    custom
                                    checked={item.checked}
                                    id={item.id + item.name}
                                    name={item.name}
                                    value={item.id}
                                    onClick={(e) => changeCheckColorSize(e, item.id)}
                                />
                                <CLabel variant="custom-checkbox" htmlFor={item.id + item.name}>{item.name}</CLabel>
                            </CFormGroup>
                        </CCol>
                    })}
                </CRow>
            </div>
            <CRow className="mt-16">
                {listOtherDetail && listOtherDetail.map((rs, index) => {
                    return <CCol xs={6} key={index}>
                        <CLabel>{rs.sizeName + '-' + rs.colorName} - Số lượng</CLabel>
                        <CRow>
                            <CCol xs={6}>
                                <CInput type="number" value={rs.quantity} placeholder="Số lượng chi tiết" onChange={(e) => onChangeOtherdetail(e, rs.id, 'quantity')} invalid={rs.validQuantity} required></CInput>
                                <CInvalidFeedback className="help-block">
                                    Xin nhập số lượng chi tiết {rs.sizeName + '-' + rs.colorName}
                                </CInvalidFeedback>
                            </CCol>
                            <CCol xs={6}>
                                <CInput type="number" value={rs.price} placeholder="Giá chi tiết" onChange={(e) => onChangeOtherdetail(e, rs.id, 'price')} invalid={rs.validPrice} required></CInput>
                                <CInvalidFeedback className="help-block">
                                    Xin nhập giá chi tiết {rs.sizeName + '-' + rs.colorName}
                                </CInvalidFeedback>
                            </CCol>
                        </CRow>
                    </CCol>
                })}
            </CRow>
            {/* <CRow className="mt-16">
                <CCol xs={12} className="flex-center">
                    <CButton color="info" onClick={() => showQuantity ? backToStart() : submitOtherDetail()}>{showQuantity ? 'Trở lại' : 'Xác nhận'}</CButton>
                </CCol>
            </CRow> */}
        </div>
    )
})
export default DetailProduct;