import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from "@coreui/react"
import React, { useRef } from "react"
import DetailProduct from "./detail-product"

const CreateProduct = () => {

    const createProduct = useRef();

    return (
        <CCard>
            <CCardHeader>
                <h3 className="m-0">Thêm sản phẩm</h3>
            </CCardHeader>
            <CCardBody>
                <DetailProduct ref={createProduct} isEdit={false} />
            </CCardBody>
            <CCardFooter>
                <CRow>
                    <CCol xs={12} className="flex-end">
                        <CButton color="primary" onClick={() => createProduct.current.handleSubmit()}>
                            Thêm
                        </CButton>
                    </CCol>
                </CRow>
            </CCardFooter>
        </CCard>
    )
}
export default CreateProduct