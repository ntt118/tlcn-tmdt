import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from "@coreui/react"
import { useRef } from "react";
import DetailPromotion from "./detail-promotion";

const CreatePromotion = () => {

    const createPromotion = useRef();

    return (
        <CCard>
            <CCardHeader>
                <h3 className="m-0">Thêm khuyến mãi</h3>
            </CCardHeader>
            <CCardBody>
                <DetailPromotion ref={createPromotion} isEdit={false} />
            </CCardBody>
            <CCardFooter>
                <CRow>
                    <CCol xs={12} className="flex-end">
                        <CButton color="primary" onClick={() => createPromotion.current.handleSubmit()}>
                            Thêm
                        </CButton>
                    </CCol>
                </CRow>
            </CCardFooter>
        </CCard>
    )
}
export default CreatePromotion