import CIcon from "@coreui/icons-react";
import { CBadge, CButton, CCard, CCardBody, CCardHeader, CDataTable, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import moment from "moment";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import PromotionApi from "src/api/promotion";
import { icons } from "src/assets/icons";
import Toaster from "src/components/toaster";
import { getListPromotion } from "src/stores/actions/promotion.action";
import DetailPromotion from "./detail-promotion";

const ListPromotion = () => {

    const dispatch = useDispatch()
    const history = useHistory()
    const promotions = useSelector(state => state.promotion.promotions)
    const [modalEdit, setModalEdit] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [promotion, setPromotion] = useState({});
    const [idDelete, setIdDelete] = useState(false);
    const editToastRef = useRef()
    const deleteToastRef = useRef()
    const fields = ['#', 'name', 'description', 'code', 'dateFrom', 'dateTo', 'status', 'action']

    const editPromotion = useRef();

    useEffect(() => {
        dispatch(getListPromotion())
    }, [])

    const getBadge = active => {
        switch (active) {
            case 1: return 'success'
            default: return 'danger'
        }
    }

    const onOpenModalEdit = (item) => {
        setPromotion(item)
        setModalEdit(true)
    }

    const afterSubmit = () => {
        dispatch(getListPromotion())
        setModalEdit(false)
        editToastRef.current.showToaster()
        editPromotion.current.resetData()
    }

    const deleteCategory = () => {
        PromotionApi.delete({ id: idDelete }).then(rs => {
            if (rs.status === "Success") {
                dispatch(getListPromotion())
                setModalDelete(false)
                deleteToastRef.current.showToaster()
            }
        })
    }

    return (
        <>
            <CCard className="promotion">
                <CCardHeader className="flex-between">
                    <h3 className="m-0">Danh sách khuyến mãi</h3>
                    <CButton color="success" onClick={() => history.push('/promotion/create')}>Thêm khuyến mãi</CButton>
                </CCardHeader>
                <CCardBody>
                    <CDataTable
                        items={promotions}
                        fields={fields}
                        striped
                        itemsPerPage={5}
                        pagination
                        scopedSlots={{
                            '#':
                                (item, index) => (
                                    <td style={{ width: '50px' }}>
                                        {index + 1}
                                    </td>
                                ),
                            'status': (item, index) => (
                                <td style={{ width: '100px' }}>
                                    <CBadge color={getBadge(item.status)}>
                                        {item.status === 1 ? 'Active' : 'No Active'}
                                    </CBadge>
                                </td>
                            ),
                            'action': (item, index) => (
                                <td style={{ width: '100px', textAlign: 'center' }}>
                                    <CIcon content={icons.cilEyedropper} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => onOpenModalEdit(item)} />
                                    <CIcon content={icons.cilDelete} style={{ cursor: item.status ? 'pointer' : 'not-allowed', color: 'red' }} size="xl" onClick={() => { if(!item.status) return; setModalDelete(true); setIdDelete(item.id) }} />
                                </td>
                            ),
                            'dateFrom': (item, index) => (
                                <td>{moment(item.dateFrom).format("DD/MM/YYYY hh:mm A")}</td>
                            ),
                            'dateTo': (item, index) => (
                                <td>{moment(item.dateTo).format("DD/MM/YYYY hh:mm A")}</td>
                            )

                        }}
                    />
                </CCardBody>
            </CCard>
            <CModal
                size="lg"
                show={modalEdit}
                onClose={() => setModalEdit(!modalEdit)}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Chỉnh sửa khuyến mãi</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <DetailPromotion ref={editPromotion} isEdit={true} promotion={promotion} afterSubmit={afterSubmit} />
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" disabled={!promotion.isActive} onClick={() => editPromotion.current.handleSubmit()} >Lưu</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => { setModalEdit(false); editPromotion.current.resetData(); }}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalDelete}
                onClose={() => setModalDelete(!modalDelete)}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Xóa khuyến mãi</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <span>Bạn có muốn xóa khuyến mãi này không?</span>
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" onClick={() => deleteCategory()} >Xóa</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalDelete(false)}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <Toaster ref={editToastRef} title="Chỉnh sửa khuyến mãi" content="Thành công!!!" />
            <Toaster ref={deleteToastRef} title="Xóa khuyến mãi" content="Thành công!!!" />
        </>
    )
}
export default ListPromotion