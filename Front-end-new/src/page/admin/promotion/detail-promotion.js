import { CCol, CInput, CInvalidFeedback, CLabel, CRow } from "@coreui/react"
import { forwardRef, useEffect, useImperativeHandle, useState } from "react"
import { useHistory } from "react-router"
import moment from "moment";
import PromotionApi from "src/api/promotion";

const DetailPromotion = forwardRef((props, ref) => {
    useImperativeHandle(ref, () => ({
        resetData() {
            setValidatedData({});
            setData(orgData);
        },
        handleSubmit() {
            if(checkValidateForm()) return
            var _data = data
            _data.dateFrom = _data.dateFrom.date + ' ' + _data.dateFrom.time
            _data.dateTo = _data.dateTo.date + ' ' + _data.dateTo.time
            if (props.isEdit) {
                var newData = {
                    id: data.id,
                    Value: _data
                }
                delete newData.Value.id
                PromotionApi.update(newData).then(rs => {
                    if (rs.status === 'Success') {
                        props.afterSubmit()
                    }
                })
            } else {
                PromotionApi.create(_data).then(rs => {
                    if (rs.status === 'Success') {
                        history.push('/promotion/list')
                        this.resetData()
                    }
                })
            }

        }
    }))


    const orgData = {
        name: null,
        description: null,
        code: null,
        dateFrom: {
            date: moment(Date.now()).format("YYYY-MM-DD"),
            time: "00:00"
        },
        dateTo: {
            date: moment(Date.now()).format("YYYY-MM-DD"),
            time: "00:00"
        },
        status: 1
    }

    const history = useHistory()
    const [data, setData] = useState(orgData)
    const [validatedData, setValidatedData] = useState({})

    useEffect(() => {
        if(props.isEdit && props.promotion){
            var data = props.promotion
            data.dateFrom = {
                date: moment(data.dateFrom).format("YYYY-MM-DD"),
                time: moment(data.dateFrom).format("hh:mm")
            }
            data.dateTo = {
                date: moment(data.dateTo).format("YYYY-MM-DD"),
                time: moment(data.dateTo).format("hh:mm")
            }
            setData(data)
        }
    }, [props.promotion])

    const onChangeField = (e, field) => {

        if(e.target.value){
            setValidatedData({...validatedData, [field]: false})
        }
        else {
            setValidatedData({...validatedData, [field]: true})
        }
        setData({...data, [field]: e.target.value})
    }

    const checkValidateForm = () => {
        var flag = false
        var valid = {}
        for (const [key, value] of Object.entries(data)) {
            if(!value && key !== "description"){
                valid[key] = true
                flag = true
            }
        }
        setValidatedData(valid)
        return flag
    }

    const onChangeDateTime = (e, type, key) => {
        var _key = 'date' + key
        var _data = data
        _data[_key][type] = e.target.value
        setData({..._data})

    }

    return (
        <div>
            <CRow>
                <CCol xs="12">
                    <CLabel>Tên khuyến mãi</CLabel>
                    <CInput placeholder="Nhập tên khuyến mãi" value={data.name} onChange={(e) => onChangeField(e, 'name')} invalid={validatedData.name} required />
                    <CInvalidFeedback className="help-block">
                        Xin nhập tên khuyến mãi
                    </CInvalidFeedback>
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol xs="12">
                    <CLabel>Mô tả khuyến mãi</CLabel>
                    <CInput placeholder="Nhập mô tả khuyến mãi" value={data.description} onChange={(e) => onChangeField(e, 'description')} invalid={validatedData.description} required />
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol xs="12">
                    <CLabel>Mã khuyến mãi</CLabel>
                    <CInput placeholder="Nhập mã khuyến mãi" value={data.code} onChange={(e) => onChangeField(e, 'code')} invalid={validatedData.code} required />
                    <CInvalidFeedback className="help-block">
                        Xin nhập mã khuyến mãi
                    </CInvalidFeedback>
                </CCol>
            </CRow>
            <CRow className="mt-16">
                <CCol xs="6">
                    <CLabel>Ngày bắt đầu</CLabel>
                    <CRow>
                        <CCol sx="6" style={{ paddingRight: '2px' }}>
                            <CInput value={data.dateFrom.date} placeholder="Nhập ngày bắt đầu" onChange={(e) => onChangeDateTime(e, 'date', 'From')} type="date" />
                        </CCol>
                        <CCol sx="6" style={{ paddingLeft: '2px' }}>
                            <CInput value={data.dateFrom.time} placeholder="Nhập thời gian bắt đầu" onChange={(e) => onChangeDateTime(e, 'time', 'From')} type="time" />
                        </CCol>
                    </CRow>
                    <CInvalidFeedback className="help-block">
                        Xin nhập ngày bắt đầu
                    </CInvalidFeedback>
                </CCol>
                <CCol xs="6">
                    <CLabel>Ngày kết thúc</CLabel>
                    <CRow>
                        <CCol sx="6" style={{ paddingRight: '2px' }}>
                            <CInput value={data.dateTo.date} placeholder="Nhập ngày kết thúc" onChange={(e) => onChangeDateTime(e, 'date', 'To')} type="date" />
                        </CCol>
                        <CCol sx="6" style={{ paddingLeft: '2px' }}>
                            <CInput value={data.dateTo.time} placeholder="Nhập thời gian kết thúc" onChange={(e) => onChangeDateTime(e, 'time', 'To')} type="time" />
                        </CCol>
                    </CRow>
                    <CInvalidFeedback className="help-block">
                        Xin nhập ngày kết thúc
                    </CInvalidFeedback>
                </CCol>
            </CRow>
        </div>
    )
})
export default DetailPromotion