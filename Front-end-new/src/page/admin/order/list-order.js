import CIcon from "@coreui/icons-react";
import { CCard, CCardBody, CCardHeader, CDataTable, CSelect } from "@coreui/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { icons } from "src/assets/icons";
import { useHistory } from 'react-router'
import { getListOrder } from "src/stores/actions/order.action";
import moment from "moment";
import OrderApi from "src/api/order";

const ListOrder = () => {

    const history = useHistory()
    const dispatch = useDispatch()
    const orders = useSelector(state => state.order.ordersAll)
    const [orderList, setOrderList] = useState([]);
    const fields = ['#', 'name', 'dateCreate', 'paymentType', 'isPayment', 'total', 'status', 'action']
    const [typeSelect, setTypeSelect] = useState(1);
    const typeStatus = [
        { value: 0, name: 'Đang chờ' },
        { value: 1, name: 'Đã xác nhận' },
        { value: 2, name: 'Đang giao' },
        { value: 3, name: 'Đã nhận' },
        { value: 4, name: 'Đã hủy' },
    ]

    useEffect(() => {
        dispatch(getListOrder())
    }, [])

    useEffect(() => {
        if(orders?.length) {
            setOrderList(orders.map(rs => {
                return {
                    ...rs,
                    isEdit: false,
                }
            }))
        }
    }, [orders])

    const getStatusOrder = (status) => {
        switch(status){
            case 0:
                return 'Đang chờ';
            case 1:
                return 'Đã xác nhận';
            case 2:
                return 'Đang giao';
            case 3:
                return 'Đã nhận';
            case 4:
                return 'Đã hủy';
            default:
                return 'Đang chờ';
        }
    }

    const goToOrderDetail = (item) => {
        history.push({
            pathname: '/order/detail',
            search: item.id.toString()
        })
    }

    const handleEditStatus = (item, index) => {
        setTypeSelect(item.status)
        setOrderList(orderList.map((rs, i) => {
            return {
                ...rs,
                isEdit: item.id === rs.id ? !rs.isEdit : false,
            }
        }))
    }

    const onChangeField = (e, item) => {
        setTypeSelect(e.target.value)
        setOrderList(orderList.map((rs, i) => {
            return {
                ...rs,
                isEdit: false,
            }
        }))
        const dataUpdate = {
            id: item.id,
            Value: {
                dateCreate: item.dateCreate,
                total: item.total,
                status: e.target.value,
                userId: item.userId,
                isPayment: item.isPayment,
                paymentType: item.paymentType,
                address: item.address,
            }
        }
        OrderApi.update(dataUpdate).then(rs => {
            if(rs.status === 'Success'){
                dispatch(getListOrder())
            }
        })
    }


    return (
        <>
            <CCard className="order">
                <CCardHeader className="flex-between">
                    <h3 className="m-0">Danh sách đơn đặt hàng</h3>
                </CCardHeader>
                <CCardBody>
                    <CDataTable
                        items={orderList}
                        fields={fields}
                        striped
                        itemsPerPage={5}
                        pagination
                        scopedSlots={{
                            '#':
                                (item, index) => (
                                    <td style={{ width: '50px' }}>
                                        {index + 1}
                                    </td>
                                ),
                            'name': 
                                    (item, index) => (
                                        <td>{item.user.name}</td>
                                    ),
                            'dateCreate':
                                    (item, index) => (
                                        <td>{moment(item.dateCreate).format('L hh:mm')}</td>
                                    ),
                            'paymentType': 
                                    (item, index) => (
                                        <td>{item.paymentType === 1 ? 'Trực tiếp' : 'Trực tuyến'}</td>
                                    ),
                            'isPayment':
                                    (item, index) => (
                                        <td>{item.isPayment === 1 ? 'Đã thanh toán' : 'Chưa thanh toán'}</td>
                                    ),
                            'status':
                                    (item, index) => (
                                        <div style={{ width: '175px', display: 'flex', 'alignItems': 'center', 'justifyContent': 'center'}}>
                                            {
                                                item.isEdit 
                                                ? <CSelect custom value={typeSelect} onChange={(e) => onChangeField(e, item)} name="type" id="type" style={{ margin: '7px' }}>
                                                        {typeStatus && typeStatus.map((rs, index) => {
                                                            return <option key={rs.value} value={rs.value}>{rs.name}</option>
                                                        })}
                                                    </CSelect>
                                                : <td>{getStatusOrder(item.status)}</td>
                                            }
                                        </div>
                                        // <td>{getStatusOrder(item.status)}</td>
                                    ),
                            'action': (item, index) => (
                                <td style={{ width: '100px', textAlign: 'center' }}>
                                    <CIcon content={icons.cilEyedropper} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => handleEditStatus(item, index)} />
                                    <CIcon content={icons.cilFace} style={{ cursor: 'pointer', color: '#321fdb' }} size="xl" onClick={() => goToOrderDetail(item)} />
                                </td>
                            )
                        }}
                    />
                </CCardBody>
            </CCard>
        </>
    )
}
export default ListOrder;