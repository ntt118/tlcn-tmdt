export const OrderStatus = {
    Waiting: 0,
    Confirmed: 1,
    Shipping: 2,
    Received: 3,
    Canceled: 4, 
}