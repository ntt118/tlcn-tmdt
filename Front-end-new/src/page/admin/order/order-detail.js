import { CCard, CCardBody, CCardHeader } from "@coreui/react";
import { useHistory } from 'react-router'
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getOrderById } from "src/stores/actions/order.action";
import './styles.scss'
import { formatPrice } from "src/page/utils";

const OrderDetail = () => {

    const history = useHistory();
    const dispatch = useDispatch();
    const order = useSelector(state => state.order.order)

    useEffect(() => {
        if (history.location.search) {
            const id = +history.location.search.slice(1,);
            dispatch(getOrderById({ id }))
        }
    }, [history.location.search])

    return (
        <CCard className="order">
            <CCardHeader className="flex-between">
                <h3 className="m-0">Chi tiết đơn đặt hàng</h3>
            </CCardHeader>
            <CCardBody>
                {order ? <>
                    <div className="_298G_L"></div>
                <div className="order-detail-page__delivery__container">
                    <div className="title">
                        <h1>Địa chỉ nhận hàng</h1>
                    </div>
                    <div className="name">
                        <h6>{order.user.name.toUpperCase()}</h6>
                        <h6>{order.user.phoneNumber}</h6>
                    </div>
                    <div className="address">
                        <span>{order.user.address}</span>
                    </div>
                </div>
                <div className="_298G_L"></div>
                <div className="info-detail-order">
                    <div>
                        {order.orderdetails.map(item => {
                            return (
                                <div className="product">
                                    <img src={item.product.imageproducts[0].image} alt="" style={{width: '100px', alignItems: 'center'}} />
                                        <div style={{display: 'grid'}}>
                                            <span style={{marginLeft: '20px'}}>{ item.product.name ? item.product.name : '' }</span>
                                            <span style={{marginLeft: '20px'}}>x{ item.quantity ? item.quantity : '' }</span>
                                        </div>
                                        <div className="price">
                                            <span>{ item.quantity * item.product.productotherdetails[0].price } VND</span>
                                        </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
                <div className="total">
                    <div className="payment-detail__container _3VUm_y">
                        <div className="payment-detail__item">
                            <div className="payment-detail__item__description">Tổng tiền hàng</div>
                            <div className="payment-detail__item__value">
                                <div className="payment-detail__item__value-text">{formatPrice(order.total)} ₫</div>
                            </div>
                        </div>
                    </div>
                </div>
                </> : <></>}
            </CCardBody>
        </CCard >
    )
}
export default OrderDetail;