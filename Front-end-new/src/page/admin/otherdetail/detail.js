import { CCol, CInput, CInvalidFeedback, CLabel, CRow, CSelect } from "@coreui/react"
import { forwardRef, useEffect, useImperativeHandle, useState } from "react"
import { useHistory } from "react-router"
import OtherDetailApi from "src/api/otherDetail"

const DetailOtherDetail = forwardRef((props, ref) => {
    useImperativeHandle(ref, () => ({
        resetData() {
            setValidatedData({});
            setData(orgData);
        },
        handleSubmit() {
            if (checkValidateForm()) return
            if (props.isEdit) {
                var newData = {
                    id: props.data.id,
                    Value: data
                }
                OtherDetailApi.update(newData).then(rs => {
                    if (rs.status === 'Success') {
                        props.afterSubmit()
                    }
                })
            } else {
                OtherDetailApi.create(data).then(rs => {
                    if (rs.status === 'Success') {
                        history.push('/otherdetail/list')
                        this.resetData()
                    }
                })
            }

        }
    }))

    const orgData = {
        name: null,
        type: "SIZE"
    }

    const history = useHistory()
    const [validatedData, setValidatedData] = useState({})
    const [data, setData] = useState(orgData)

    const typeList = [
        { id: 1, name: "SIZE" },
        { id: 2, name: "COLOR" },
    ]

    useEffect(() => {
        if (props.isEdit && props.data) {
            setData(props.data)
        }
    }, [props.data])

    const onChangeField = (e, field) => {

        if (e.target.value) {
            setValidatedData({ ...validatedData, [field]: false })
        }
        else {
            setValidatedData({ ...validatedData, [field]: true })
        }
        setData({ ...data, [field]: e.target.value })
    }

    const checkValidateForm = () => {
        var flag = false
        var valid = {}
        for (const [key, value] of Object.entries(data)) {
            if (!value) {
                valid[key] = true
                flag = true
            }
        }
        setValidatedData(valid)
        return flag
    }

    const getCheckRole = () => {
        const user = data.user;
        if (user) {
            if (!user.confirmed) {
                return false;
            } else {
                if (user.role === 'user') {
                    return true
                } else {
                    return false
                }
            }
        }
        return false
    }

    return (
        <div>
            <CRow>
                <CCol xs="12">
                    <CLabel htmlFor="type">Loại chi tiết</CLabel>
                    <CSelect custom value={data.type} onChange={(e) => onChangeField(e, 'type')} name="type" id="type">
                        {typeList && typeList.map((rs, index) => {
                            return <option key={index} value={rs.name}>{rs.name}</option>
                        })}
                    </CSelect>
                </CCol>
            </CRow>
            <CRow className='mt-16'>
                <CCol xs="12">
                    <CLabel htmlFor="nameOtherDetail">Tên chi tiết</CLabel>
                    <CInput id="nameOtherDetail" placeholder="Nhập tên chi tiết" value={data.name} onChange={(e) => onChangeField(e, 'name')} invalid={validatedData.name} required />
                    <CInvalidFeedback className="help-block">
                        Xin nhập tên chi tiết
                    </CInvalidFeedback>
                </CCol>
            </CRow>
        </div>
    )
})
export default DetailOtherDetail