import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCol, CRow } from "@coreui/react";
import { useRef } from "react";
import DetailOtherDetail from "./detail";

const CreateOtherDetail = () => {

    const editOtherDetail = useRef();

    return (
        <CCard className="category">
            <CCardHeader>
                <h3 className="m-0">Thêm chi tiết khác</h3>
            </CCardHeader>
            <CCardBody>
                <DetailOtherDetail ref={editOtherDetail} isEdit={false} />
            </CCardBody>
            <CCardFooter>
                <CRow>
                    <CCol xs={12} className="flex-end">
                        <CButton color="primary" onClick={() => editOtherDetail.current.handleSubmit()}>
                            Thêm
                        </CButton>
                    </CCol>
                </CRow>
            </CCardFooter>
        </CCard>
    )
}
export default CreateOtherDetail