import CIcon from "@coreui/icons-react";
import { CBadge, CButton, CCard, CCardBody, CCardHeader, CDataTable, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CSelect } from "@coreui/react";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import OtherDetailApi from "src/api/otherDetail";
import { icons } from "src/assets/icons";
import Toaster from "src/components/toaster";
import { getListOtherDetail } from "src/stores/actions/otherdetail.action";
import DetailOtherDetail from "./detail";

const ListOtherDetail = () => {

    const dispatch = useDispatch()
    const history = useHistory()
    const otherDetails = useSelector(state => state.otherDetail.otherDetails)
    const [modalEdit, setModalEdit] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [data, setData] = useState({});
    const [idDelete, setIdDelete] = useState({});
    const [type, setType] = useState('SIZE');
    const editToastRef = useRef()
    const deleteToastRef = useRef()
    const fields = ['#', 'name', 'status', 'action']
    const typeList = [
        { id: 1, name: "SIZE" },
        { id: 2, name: "COLOR" },
    ]

    const editOtherDatail = useRef();

    useEffect(() => {
        dispatch(getListOtherDetail(type))
    }, [type])

    const getBadge = active => {
        switch (active) {
            case 1: return 'success'
            default: return 'danger'
        }
    }

    const onOpenModalEdit = (item) => {
        const data = item
        data.type = type
        setData(data)
        setModalEdit(true)
    }

    const afterSubmit = () => {
        dispatch(getListOtherDetail(type))
        setModalEdit(false)
        editToastRef.current.showToaster()
        editOtherDatail.current.resetData()
    }

    const deleteProduct = () => {
        OtherDetailApi.delete(idDelete).then(rs => {
            if (rs.status === "Success") {
                dispatch(getListOtherDetail(type))
                setModalDelete(false)
                deleteToastRef.current.showToaster()
            }
        })
    }

    return (
        <>
            <CCard className="product">
                <CCardHeader className="flex-between">
                    <div className="flex-ali-center">
                        <h3 className="m-0">Danh sách chi tiết</h3>
                        <CSelect custom ={type} name="type" id="type" onChange={(e) => setType(e.target.value)} style={{ width: '90px', marginLeft: '7px' }}>
                            {typeList && typeList.map((rs, index) => {
                                return <option key={index} value={rs.name}>{rs.name}</option>
                            })}
                        </CSelect>
                    </div>
                    <CButton color="success" onClick={() => history.push('/otherdetail/create')}>Thêm chi tiết</CButton>
                </CCardHeader>
                <CCardBody>
                    <CDataTable
                        items={otherDetails}
                        fields={fields}
                        striped
                        itemsPerPage={5}
                        pagination
                        sorter
                        scopedSlots={{
                            '#':
                                (item, index) => (
                                    <td style={{ width: '50px' }}>
                                        {index + 1}
                                    </td>
                                ),
                            'status': (item, index) => (
                                <td style={{ width: '100px' }}>
                                    <CBadge color={getBadge(item.status)}>
                                        {item.status === 1 ? 'Active' : 'No Active'}
                                    </CBadge>
                                </td>
                            ),
                            'action': (item, index) => (
                                <td style={{ width: '100px', textAlign: 'center' }}>
                                    {/* <CIcon content={icons.cilPlus} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => history.push('/product/create')} /> */}
                                    <CIcon content={icons.cilEyedropper} style={{ cursor: 'pointer', color: 'green', marginRight: '15px' }} size="xl" onClick={() => onOpenModalEdit(item)} />
                                    <CIcon content={icons.cilDelete} style={{ cursor: 'pointer', color: 'red' }} size="xl" onClick={() => { setModalDelete(true); setIdDelete({id: item.id, type: type, status: item.status}) }} />
                                </td>
                            )

                        }}
                    />
                </CCardBody>
            </CCard>
            <CModal
                size='lg'
                show={modalEdit}
                onClose={() => {setModalEdit(!modalEdit); editOtherDatail.current.resetData();}}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Chỉnh sửa chi tiết</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <DetailOtherDetail ref={editOtherDatail} isEdit={true} data={data} afterSubmit={afterSubmit} />
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" disabled={!data.status} onClick={() => editOtherDatail.current.handleSubmit()} >Lưu</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => { setModalEdit(false); editOtherDatail.current.resetData(); }}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalDelete}
                onClose={() => setModalDelete(!modalDelete)}
            >
                <CModalHeader closeButton>
                    <CModalTitle>Xóa chi tiết</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <span>Bạn có muốn xóa chi tiết này không?</span>
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary" onClick={() => deleteProduct()} >Xóa</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalDelete(false)}
                    >Hủy</CButton>
                </CModalFooter>
            </CModal>
            <Toaster ref={editToastRef} title="Chỉnh sửa chi tiết" content="Thành công!!!" />
            <Toaster ref={deleteToastRef} title="Xóa chi tiết" content="Thành công!!!" />
        </>
    )
}
export default ListOtherDetail