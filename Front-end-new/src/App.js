/* eslint-disable no-unused-vars */
import React, { Component, useEffect, useState } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { Redirect } from 'react-router'
import './scss/style.scss';
import 'react-image-gallery/styles/scss/image-gallery.scss';
import { useSelector } from 'react-redux';
import UserProfile from './page/client/profile/profile';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
const Login = React.lazy(() => import('src/page/client/login/login'));
const ForgetPassword = React.lazy(() => import('src/page/client/signUp/forgetPassword'));
const SignUp = React.lazy(() => import('src/page/client/signUp/signUp'));
const Home = React.lazy(() => import('src/page/client/home/home'));
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));
const CoreUIIcons = React.lazy(() => import('./views/icons/coreui-icons/CoreUIIcons'));

const App = () => {

  const role = JSON.parse(localStorage.getItem('role'))?.toUpperCase()
  const user = useSelector(state => state.user.user) 
  const [isLogin, setIsLogin] = useState(true)//user ? true : false
  const [isAdmin, setIsAdmin] = useState(() => {
    if(role){
      if (role === "ADMIN") {
        return true
      } else {
        return false
      }
    }
    return false
  })

  useEffect(() => {
    if (user) {
      setIsLogin(true)
    }
  }, [user])

  useEffect(() => {
    if (user) {
      if (user.role === "ADMIN") {
        setIsAdmin(true)
      } else {
        setIsAdmin(false)
      }
    }
    else setIsLogin(false)
  }, [role, user])

  return (
    <HashRouter>
      <React.Suspense fallback={loading}>
        <Switch>
          <Route exact path="/login" name="Trang đăng nhập" render={() => {
            return isLogin
            ? <Redirect to="/home" />
            : <Login />
          }} />
          <Route exact path="/profile" name="Trang thông tin người dùng" component={UserProfile} />
          <Route exact path="/forget-password" name="Trang quên mật khẩu"  render={() => { return <ForgetPassword /> }}/>
          <Route exact path="/signUp" name="Trang đăng ký" render={() => {
            return isLogin
            ? <Redirect to="/home" />
            : <SignUp />
          }} />
          <Route exact path="/icons" name="Icon" render={props =>  <CoreUIIcons {...props} />}></Route>
          <Route exact path="/404" name="Page 404" component={Page404} />
          <Route exact path="/500" name="Page 500" component={Page500} />
          <Route path="/home" exact name="Client Home Page" render={props => <Home {...props} />} />
          <Route path="/" name="Admin Page" render={props => {
            return isLogin && isAdmin 
            ? <TheLayout {...props} />
            : <Home {...props} />
          }
          } />
        </Switch>
      </React.Suspense>
    </HashRouter>
  );
}
export default App